<?php
include('index.php');
?>

<div class="indicadores" style="width: 75%; margin: 0 auto; padding-top: 40px;">
    <h3 class="h3" style="text-align: center;">Árbol de alarmas año 2020</h3><br>
    <div class="generals">
        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Métricas generales
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <ul>
                            <li><a href="./graph/Clareo de alarmas durante el 2020 por línea.html" class="text-decoration-none">Clareo de alarmas durante el 2020 por línea</a></li>
                            <li><a href="./graph/Clareo de alarmas por rango de tiempo durante enero y febrero.html" class="text-decoration-none">Clareo de alarmas por rango de tiempo durante enero y febrero</a></li>
                            <li><a href="./graph/Proporción de alarmas por línea.html" class="text-decoration-none">Proporción de alarmas por línea</a></li>
                            <li><a href="./graph/Regiones afectadas 2020.html" class="text-decoration-none">Regiones afectadas 2020</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Acceso
                    </button>
                </h2>
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <ul>
                            <li><a href="./graph/acceso/Gestión de alarmas semanalmente en la linea ACCESO MOVIL.html" class="text-decoration-none">Gestión de alarmas semanalmente en la linea acceso móvil</a></li>
                            <li><a href="./graph/acceso/Porcentaje de alarmas mensuales discriminadas por gestión ACCESO MOVIL.html" class="text-decoration-none">Porcentaje de alarmas mensuales discriminadas por gestión</a></li>
                            <li><a href="./graph/acceso/Porcentaje de alarmas semanalmente discriminadas por gestión ACCESO MOVIL.html" class="text-decoration-none">Porcentaje de alarmas semanalmente discriminadas por gestión ACCESO MOVIL</a></li>
                            <li><a href="./graph/acceso/Porcentajes de regiones afectadas entre enero y febrero para acceso movil.html" class="text-decoration-none">Porcentajes de regiones afectadas entre enero y febrero para acceso movil</a></li>
                            <li><a href="./graph/acceso/Proporción de alarmas de acceso movil por gestión.html" class="text-decoration-none">Proporción de alarmas de acceso movil por gestión</a></li>
                            <li><a href="./graph/acceso/Regiones y municipios afectadas entre enero y febrero para acceso movil.html" class="text-decoration-none">Regiones y municipios afectadas entre enero y febrero para acceso movil</a></li>
                            <li><a href="./graph/acceso/Regiones y municipios afectados entre enero y febrero para acceso movil.html" class="text-decoration-none">Regiones y municipios afectados entre enero y febrero para acceso movil</a></li>
                            <li><a href="./graph/acceso/Clareo de top 5 alarmas no gestionadas de la linea Acceso Movil.html" class="text-decoration-none">Clareo de top 5 alarmas no gestionadas de la linea Acceso Movil</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Energía
                    </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <ul>
                            <li><a class="text-decoration-none" href="./graph/energia/Gestión de alarmas semanalmente en la linea ENERGÍA.html" class="text-decoration-none">Gestión de alarmas semanalmente en la linea</a></li>
                            <li><a class="text-decoration-none" href="./graph/energia/Clareo de top 5 alarmas de la linea Energía.html" class="text-decoration-none">Clareo de top 5 alarmas de la linea Energía</a></li>
                            <li><a class="text-decoration-none" href="./graph/energia/Porcentaje de alarmas mensuales discriminadas por gestión ENERGÍA.html" class="text-decoration-none">Porcentaje de alarmas mensuales discriminadas por gestión</a></li>
                            <li><a class="text-decoration-none" href="./graph/energia/Porcentaje de alarmas semanalmente discriminadas por gestión ENERGÍA.html" class="text-decoration-none">Porcentaje de alarmas semanalmente discriminadas por gestión</a></li>
                            <li><a class="text-decoration-none" href="./graph/energia/Porcentajes de regiones afectadas entre enero y febrero para energia.html" class="text-decoration-none">Porcentajes de regiones afectadas entre enero y febrero para energia</a></li>
                            <li><a class="text-decoration-none" href="./graph/energia/Proporción de alarmas de energía por gestión.html" class="text-decoration-none">Proporción de alarmas de energía por gestión</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingFour">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        FO Core
                    </button>
                </h2>
                <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <ul>
                            <li><a class="text-decoration-none" href="./graph/core/Clareo de top 5 alarmas de la linea Core.html" class="text-decoration-none">Clareo de top 5 alarmas de la linea Core</a></li>
                            <li><a class="text-decoration-none" href="./graph/core/Gestión de alarmas semanalmente en la linea CORE.html" class="text-decoration-none">Gestión de alarmas semanalmente en la linea CORE</a></li>
                            <li><a class="text-decoration-none" href="./graph/core/Porcentaje de alarmas mensuales discriminadas por gestión CORE.html" class="text-decoration-none">Porcentaje de alarmas mensuales discriminadas por gestión CORE</a></li>
                            <li><a class="text-decoration-none" href="./graph/core/Proporción de alarmas de Core por gestión.html" class="text-decoration-none">Proporción de alarmas de Core por gestión</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingFive">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        FO TX/IP
                    </button>
                </h2>
                <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <ul>
                            <li><a class="text-decoration-none" href="./graph/tx_ip/Clareo de top 5 alarmas de la linea Transmisión e IP.html" class="text-decoration-none">Clareo de top 5 alarmas de la linea Transmisión e IP</a></li>
                            <li><a class="text-decoration-none" href="./graph/tx_ip/Gestión de alarmas semanalmente en la linea TRANSMISION E IP.html" class="text-decoration-none">Gestión de alarmas semanalmente en la linea TRANSMISION E IP</a></li>
                            <li><a class="text-decoration-none" href="./graph/tx_ip/Porcentaje de alarmas semanalmente discriminadas por gestión Transmision e IP.html" class="text-decoration-none">Porcentaje de alarmas semanalmente discriminadas por gestión Transmision e IP</a></li>
                            <li><a class="text-decoration-none" href="./graph/tx_ip/Proporción de alarmas de transmisión e IP por gestión.html" class="text-decoration-none">Proporción de alarmas de transmisión e IP por gestión</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">