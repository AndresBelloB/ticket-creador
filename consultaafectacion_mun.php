<?php
include('index.php');
include('con_db.php');
?>
<?php
$resultgsm = $con->query("SELECT DISTINCT MUNICIPIO FROM dbtrafico_gsm") or die(mysqli_error($con));
$mun_gsm = $resultgsm->fetch_all();

$resultumts = $con->query("SELECT DISTINCT MUNICIPIO FROM dbtrafico_umts") or die(mysqli_error($con));
$mun_umts = $resultumts->fetch_all();

$resultlte = $con->query("SELECT DISTINCT MUNICIPIO FROM dbtrafico_lte") or die(mysqli_error($con));
$mun_lte = $resultlte->fetch_all();




if (!empty($mun_gsm) || !empty($mun_umts) || !empty($mun_lte)) {
    $municipios = array_merge($mun_gsm, $mun_umts, $mun_lte);
    for ($o = 0; $o < sizeof($municipios); $o++) {
        $municipios_lista[$o] = $municipios[$o][0];
    }

    $municipios_lista = array_unique($municipios_lista);
} else {
}
?>

<?php
if (isset($_POST['searchsites'])) {

    $tabla_gsm = [];
    $tabla_umts = [];
    $tabla_lte = [];

    if (isset($_POST['municipio'])) {
        $selmun = $_POST['municipio'];
    } else {
        $_SESSION['message'] = "Campos vacios";
        $_SESSION['msg_type'] = "danger";
        exit();
    }

    $mungsm = $con->query("SELECT MUNICIPIO, COUNT(DISTINCT NOMBRE_EB_GSM) FROM dbtrafico_gsm GROUP BY MUNICIPIO") or die(mysqli_error($con));
    $munumts = $con->query("SELECT MUNICIPIO, COUNT(DISTINCT NOMBRE_EB_UMTS) FROM dbtrafico_umts GROUP BY MUNICIPIO") or die(mysqli_error($con));
    $munlte = $con->query("SELECT MUNICIPIO, COUNT(DISTINCT NOMBRE_EB_LTE) FROM dbtrafico_lte GROUP BY MUNICIPIO") or die(mysqli_error($con));

    $cuentagsm = $mungsm->fetch_all();
    $cuentaumts = $munumts->fetch_all();
    $cuentalte = $munlte->fetch_all();

    $resultgsm = $con->query("SELECT DISTINCT NOMBRE_EB_GSM, DEPARTAMENTO, MUNICIPIO FROM dbtrafico_gsm WHERE MUNICIPIO='$selmun'") or die(mysqli_error($con));
    $eb_gsm = $resultgsm->fetch_all();

    $resultumts = $con->query("SELECT DISTINCT NOMBRE_EB_UMTS, DEPARTAMENTO, MUNICIPIO FROM dbtrafico_umts WHERE MUNICIPIO='$selmun'") or die(mysqli_error($con));
    $eb_umts = $resultumts->fetch_all();

    $resultlte = $con->query("SELECT DISTINCT NOMBRE_EB_LTE, DEPARTAMENTO, MUNICIPIO FROM dbtrafico_lte WHERE MUNICIPIO='$selmun'") or die(mysqli_error($con));
    $eb_lte = $resultlte->fetch_all();


    //estaciones base distintas

    $ebgsm = array_column($eb_gsm, 0);
    $ebumts = array_column($eb_umts, 0);
    $eblte = array_column($eb_lte, 0);
    $eb = array_merge($ebgsm, $ebumts, $eblte);

    $eb_union = array_values(array_unique($eb));



    for ($i = 0; $i < sizeof($eb_union); $i++) {
        $tabla[$i][1] = $eb_union[$i];
    }

    for ($i = 0; $i < sizeof($eb_gsm); $i++) {
        $tabla_gsm[$i][0] = $eb_gsm[$i][1];
        $tabla_gsm[$i][1] = $eb_gsm[$i][0];
        $tabla_gsm[$i][2] = $eb_gsm[$i][2];
        $tabla_gsm[$i][3] = floor(((1 / sizeof($eb_gsm)) * 10000)) / 100;
    }

    for ($i = 0; $i < sizeof($eb_umts); $i++) {
        $tabla_umts[$i][0] = $eb_umts[$i][1];
        $tabla_umts[$i][1] = $eb_umts[$i][0];
        $tabla_umts[$i][2] = $eb_umts[$i][2];
        $tabla_umts[$i][3] = floor(((1 / sizeof($eb_umts)) * 10000)) / 100;
    }

    for ($i = 0; $i < sizeof($eb_lte); $i++) {
        $tabla_lte[$i][0] = $eb_lte[$i][1];
        $tabla_lte[$i][1] = $eb_lte[$i][0];
        $tabla_lte[$i][2] = $eb_lte[$i][2];
        $tabla_lte[$i][3] = floor(((1 / sizeof($eb_lte)) * 10000)) / 100;
    }


    for ($o = 0; $o < sizeof($tabla); $o++) {
        for ($i = 0; $i < sizeof($tabla_gsm); $i++) {
            if (strtoupper($tabla_gsm[$i][1]) == $tabla[$o][1]) {
                if (empty($tabla[$o][0])) {
                    $tabla[$o][0] = $tabla_gsm[$i][0];
                }
                if (empty($tabla[$o][2])) {
                    $tabla[$o][2] = $tabla_gsm[$i][2];
                }
                $tabla[$o][3] = $tabla_gsm[$i][3];
            }
        }
        for ($i = 0; $i < sizeof($tabla_umts); $i++) {
            if (strtoupper($tabla_umts[$i][1]) == $tabla[$o][1]) {
                if (empty($tabla[$o][0])) {
                    $tabla[$o][0] = $tabla_umts[$i][0];
                }
                if (empty($tabla[$o][2])) {
                    $tabla[$o][2] = $tabla_umts[$i][2];
                }
                $tabla[$o][4] = $tabla_umts[$i][3];
            }
        }
        for ($i = 0; $i < sizeof($tabla_lte); $i++) {
            if (strtoupper($tabla_lte[$i][1]) == $tabla[$o][1]) {
                if (empty($tabla[$o][0])) {
                    $tabla[$o][0] = $tabla_lte[$i][0];
                }
                if (empty($tabla[$o][2])) {
                    $tabla[$o][2] = $tabla_lte[$i][2];
                }
                $tabla[$o][5] = $tabla_lte[$i][3];
            }
        }
    }
}

?>

<br>
<div class="container">
    <form action="consultaafectacion_mun" method="post" id="busquedasitios">
        <div class="row">
            <div class="col-4 my-auto text-center">
                <label style="font-weight:bold">CONSULTA POR MUNICIPIO:</label>
                <select class="selectpicker mt-2 " id="municipio" name="municipio" data-live-search="true" data-size="10">
                    <option data-tokens="" selected disabled hidden>Seleccione municipio</option>
                    <?php
                    foreach ($municipios_lista as $municipio) { ?>
                        <option value="<?php echo $municipio; ?>"> <?php echo $municipio; ?> </option>
                    <?php } ?>
                </select>
                <button type="submit" id="searchsites" name="searchsites" class="btn btn-dark mt-2">Buscar</button>
            </div>
            <div class="col-8 text-center">
                <table id="porestacion" class="display compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>DEPARTAMENTO</th>
                            <th>EB</th>
                            <th>MUNICIPIO</th>
                            <th>GSM</th>
                            <th>UMTS</th>
                            <th>LTE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_POST['searchsites'])) {
                            for ($i = 0; $i < sizeof($tabla); $i++) {
                                if (empty($tabla)) {
                                    session_start();
                                    $_SESSION['message'] = "No se encuentran registros asociados";
                                    $_SESSION['msg_type'] = "danger";
                                    header("location: consultadesitios ");
                                    exit();
                                } else {
                        ?> <tr>
                                        <td> <?php if (isset($tabla[$i][0])) {
                                                    echo $tabla[$i][0];
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][1])) {
                                                    echo $tabla[$i][1];
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][2])) {
                                                    echo $tabla[$i][2];
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][3])) {
                                                    echo $tabla[$i][3] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][4])) {
                                                    echo $tabla[$i][4] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][5])) {
                                                    echo $tabla[$i][5] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                    </tr>
                        <?php
                                }
                            }
                        }

                        ?>
                    </tbody>
                </table>
            </div>
    </form>
    <script>
        $(document).ready(function() {
            $('#porestacion').DataTable();
            $('#afectacion100').DataTable();
        });
    </script>
    <script>
        $('#porestacion').DataTable({
            language: {
                emptyTable: "Por favor realice su búsqueda"
            },
            "searching": false,
            "ordering": false,
            "info": false,
            "paging": false,
            scrollCollapse: true,
            scrollY: "280px",

        });
    </script>
    <script>
        $('#afectacion100').DataTable({
            language: {
                emptyTable: "Por favor realice su búsqueda"
            },
            "searching": false,
            "ordering": false,
            "info": false,
            "paging": false,
            scrollCollapse: true,
            scrollY: "280px",
        });
    </script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
</div>
<?php include_once('footer.php'); ?>