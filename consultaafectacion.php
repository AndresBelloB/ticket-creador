<?php
include('index.php');
include('con_db.php');
?>
<?php
$textoinput = "";
$listamunicipios = [];
$tabla = [];
$tabla100 = [];
if (isset($_POST['searchsites'])) {

    $listamunicipios = [];
    $valmun = [];
    $tabla_gsm = [];
    $tabla_umts = [];
    $tabla_lte = [];
    $tabla = [];
    $tabla100 = [];

    if (isset($_POST['consultasites'])) {
        $textoinput = $_POST['consultasites'];
    } else {
        $textoinput = "";
    }
    $text = $_POST['consultasites'];
    if (empty($text)) {
        session_start();
        $_SESSION['message'] = "Campos vacios";
        $_SESSION['msg_type'] = "danger";
        header("location: consultadesitios ");
        exit();
    } else {
        $text = preg_split('/(\r\n|,)/', $text);
        $text = array_map('trim', $text);
        $text = array_unique(array_filter($text));
        $textoclear = $text;
        foreach ($text as &$format) {
            $format = "'" . $format . "'";
        }

        unset($format);

        $textsql = implode(',', $text);
    }


    $mungsm = $con->query("SELECT MUNICIPIO, COUNT(DISTINCT NOMBRE_EB_GSM) FROM dbtrafico_gsm GROUP BY MUNICIPIO") or die(mysqli_error($con));
    $munumts = $con->query("SELECT MUNICIPIO, COUNT(DISTINCT NOMBRE_EB_UMTS) FROM dbtrafico_umts GROUP BY MUNICIPIO") or die(mysqli_error($con));
    $munlte = $con->query("SELECT MUNICIPIO, COUNT(DISTINCT NOMBRE_EB_LTE) FROM dbtrafico_lte GROUP BY MUNICIPIO") or die(mysqli_error($con));

    $cuentagsm = $mungsm->fetch_all();
    $cuentaumts = $munumts->fetch_all();
    $cuentalte = $munlte->fetch_all();

    $resultgsm = $con->query("SELECT DISTINCT NOMBRE_EB_GSM, DEPARTAMENTO, MUNICIPIO FROM dbtrafico_gsm WHERE NOMBRE_EB_GSM IN ($textsql)") or die(mysqli_error($con));
    $eb_gsm = $resultgsm->fetch_all();

    $resultumts = $con->query("SELECT DISTINCT NOMBRE_EB_UMTS, DEPARTAMENTO, MUNICIPIO FROM dbtrafico_umts WHERE NOMBRE_EB_UMTS IN ($textsql)") or die(mysqli_error($con));
    $eb_umts = $resultumts->fetch_all();

    $resultlte = $con->query("SELECT DISTINCT NOMBRE_EB_LTE, DEPARTAMENTO, MUNICIPIO FROM dbtrafico_lte WHERE NOMBRE_EB_LTE IN ($textsql)") or die(mysqli_error($con));
    $eb_lte = $resultlte->fetch_all();



    for ($i = 0; $i < sizeof($eb_gsm); $i++) {
        for ($k = 0; $k < sizeof($cuentagsm); $k++) {
            if ($eb_gsm[$i][2] == $cuentagsm[$k][0]) {
                $tabla_gsm[$i][0] = $eb_gsm[$i][1];
                $tabla_gsm[$i][1] = $eb_gsm[$i][0];
                $tabla_gsm[$i][2] = $eb_gsm[$i][2];
                // $cuentagsm[$k][1]--;
                $tabla_gsm[$i][3] = floor(((1 / $cuentagsm[$k][1]) * 10000)) / 100;
            }
        }
    }

    for ($i = 0; $i < sizeof($eb_umts); $i++) {
        for ($k = 0; $k < sizeof($cuentaumts); $k++) {
            if ($eb_umts[$i][2] == $cuentaumts[$k][0]) {

                $tabla_umts[$i][0] = $eb_umts[$i][1];
                $tabla_umts[$i][1] = $eb_umts[$i][0];
                $tabla_umts[$i][2] = $eb_umts[$i][2];

                // $cuentaumts[$k][1]--;
                $tabla_umts[$i][3] = floor(((1 / $cuentaumts[$k][1]) * 10000)) / 100;
            }
        }
    }


    for ($i = 0; $i < sizeof($eb_lte); $i++) {
        for ($k = 0; $k < sizeof($cuentalte); $k++) {
            if ($eb_lte[$i][2] == $cuentalte[$k][0]) {

                $tabla_lte[$i][0] = $eb_lte[$i][1];
                $tabla_lte[$i][1] = $eb_lte[$i][0];
                $tabla_lte[$i][2] = $eb_umts[$i][2];

                // $cuentalte[$k][1]--;
                $tabla_lte[$i][3] = floor(((1 / $cuentalte[$k][1]) * 10000)) / 100;
            }
        }
    }

    $o = 0;


    if (!empty($eb_gsm) || !empty($eb_umts) || !empty($eb_lte)) {
        foreach ($textoclear as $eb) {
            $tabla[$o][1] = strtoupper($eb);
            $o++;
        }
    }

    for ($o = 0; $o < sizeof($tabla); $o++) {
        for ($i = 0; $i < sizeof($tabla_gsm); $i++) {
            if (strtoupper($tabla_gsm[$i][1]) == $tabla[$o][1]) {
                if (empty($tabla[$o][0])) {
                    $tabla[$o][0] = $tabla_gsm[$i][0];
                }
                if (empty($tabla[$o][2])) {
                    $tabla[$o][2] = $tabla_gsm[$i][2];
                }
                $tabla[$o][3] = $tabla_gsm[$i][3];
            }
        }
        for ($i = 0; $i < sizeof($tabla_umts); $i++) {
            if (strtoupper($tabla_umts[$i][1]) == $tabla[$o][1]) {
                if (empty($tabla[$o][0])) {
                    $tabla[$o][0] = $tabla_umts[$i][0];
                }
                if (empty($tabla[$o][2])) {
                    $tabla[$o][2] = $tabla_umts[$i][2];
                }
                $tabla[$o][4] = $tabla_umts[$i][3];
            }
        }
        for ($i = 0; $i < sizeof($tabla_lte); $i++) {
            if (strtoupper($tabla_lte[$i][1]) == $tabla[$o][1]) {
                if (empty($tabla[$o][0])) {
                    $tabla[$o][0] = $tabla_lte[$i][0];
                }
                if (empty($tabla[$o][2])) {
                    $tabla[$o][2] = $tabla_lte[$i][2];
                }
                $tabla[$o][5] = $tabla_lte[$i][3];
            }
        }
    }


    // MUNICIPIOS 100%


    $resultgsm = $con->query("SELECT DISTINCT MUNICIPIO FROM dbtrafico_gsm WHERE NOMBRE_EB_GSM IN ($textsql)") or die(mysqli_error($con));
    $mun_gsm = $resultgsm->fetch_all();

    $resultumts = $con->query("SELECT DISTINCT MUNICIPIO FROM dbtrafico_umts WHERE NOMBRE_EB_UMTS IN ($textsql)") or die(mysqli_error($con));
    $mun_umts = $resultumts->fetch_all();

    $resultlte = $con->query("SELECT DISTINCT MUNICIPIO FROM dbtrafico_lte WHERE NOMBRE_EB_LTE IN ($textsql)") or die(mysqli_error($con));
    $mun_lte = $resultlte->fetch_all();




    if (!empty($mun_gsm) || !empty($mun_umts) || !empty($mun_lte)) {
        $municipios = array_merge($mun_gsm, $mun_umts, $mun_lte);

        for ($o = 0; $o < sizeof($municipios); $o++) {
            $municipios_lista[$o] = $municipios[$o][0];
        }

        $municipios_lista = array_unique($municipios_lista);

        $j = 0;
        foreach ($municipios_lista as $mun) {
            $tabla100[$j][0] = $mun;
            $tabla100[$j][2] = 0;
            $tabla100[$j][3] = 0;
            $tabla100[$j][4] = 0;
            $j++;
        }
    } else {
    }


    for ($o = 0; $o < sizeof($tabla100); $o++) {
        for ($i = 0; $i < sizeof($tabla); $i++) {
            if (isset($tabla[$i][2])) {
                if (strtoupper($tabla100[$o][0]) == strtoupper($tabla[$i][2])) {
                    if (isset($tabla[$i][3])) {
                        $tabla100[$o][1] = $tabla[$i][0];
                        $tabla100[$o][2] = $tabla100[$o][2] + $tabla[$i][3];
                    }
                    if (isset($tabla[$i][4])) {
                        $tabla100[$o][1] = $tabla[$i][0];
                        $tabla100[$o][3] = $tabla100[$o][3] + $tabla[$i][4];
                    }
                    if (isset($tabla[$i][5])) {
                        $tabla100[$o][1] = $tabla[$i][0];
                        $tabla100[$o][4] = $tabla100[$o][4] + $tabla[$i][5];
                    }
                }
            }
        }
    }
}



?>

<br>

<div class="container">
    <form action="consultaafectacion" method="post" id="busquedasitios">
        <div class="row">
            <div class="col-4 text-center">
                <label style="font-weight:bold">CONSULTA DE SITIOS:</label>
                <textarea class="form-control" id="consultasites" name="consultasites" rows="26" required="required"><?php echo $textoinput; ?></textarea>
                <button type="submit" id="searchsites" name="searchsites" class="btn btn-dark mt-2">Buscar</button>
            </div>

            <div class="col-8 text-center">
                <table id="porestacion" class="display compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>DEPARTAMENTO</th>
                            <th>EB</th>
                            <th>MUNICIPIO</th>
                            <th>GSM</th>
                            <th>UMTS</th>
                            <th>LTE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_POST['searchsites'])) {
                            for ($i = 0; $i < sizeof($tabla); $i++) {
                                if (empty($tabla)) {
                                    session_start();
                                    $_SESSION['message'] = "No se encuentran registros asociados";
                                    $_SESSION['msg_type'] = "danger";
                                    header("location: consultadesitios ");
                                    exit();
                                } else {
                        ?> <tr>
                                        <td> <?php if (isset($tabla[$i][0])) {
                                                    echo $tabla[$i][0];
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][1])) {
                                                    echo $tabla[$i][1];
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][2])) {
                                                    echo $tabla[$i][2];
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][3])) {
                                                    echo $tabla[$i][3] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][4])) {
                                                    echo $tabla[$i][4] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla[$i][5])) {
                                                    echo $tabla[$i][5] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                    </tr>
                        <?php
                                }
                            }
                        }

                        ?>

                    </tbody>
                </table>
                <table id="afectacion100" class="display compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>DEPARTAMENTO</th>
                            <th>MUNICIPIO</th>
                            <th>GSM</th>
                            <th>UMTS</th>
                            <th>LTE</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if (isset($_POST['searchsites'])) {
                            for ($i = 0; $i < sizeof($tabla100); $i++) {
                                if (empty($tabla100)) {
                                    session_start();
                                    $_SESSION['message'] = "No se encuentran registros asociados";
                                    $_SESSION['msg_type'] = "danger";
                                    header("location: consultadesitios ");
                                    exit();
                                } else {
                        ?> <tr>
                                        <td> <?php if (isset($tabla100[$i][1])) {
                                                    echo $tabla100[$i][1];
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if (isset($tabla100[$i][0])) {
                                                    echo ucwords(strtolower($tabla100[$i][0]));
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if ($tabla100[$i][2] != 0) {
                                                    echo $tabla100[$i][2] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if ($tabla100[$i][3] != 0) {
                                                    echo $tabla100[$i][3] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                        <td> <?php if ($tabla100[$i][4] != 0) {
                                                    echo $tabla100[$i][4] . "%";
                                                } else {
                                                    echo "-";
                                                } ?> </td>
                                    </tr>
                        <?php
                                }
                            }
                        }

                        ?>

                    </tbody>
                </table>
                <button type="button" id="generarexcel" name="generarexcel" class="btn btn-dark mt-2">Excel</button>
            </div>

            <script>
                $(document).ready(function() {
                    $('#porestacion').DataTable();
                    $('#afectacion100').DataTable();
                });
            </script>
            <script>
                $('#porestacion').DataTable({
                    language: {
                        emptyTable: "Por favor realice su búsqueda"
                    },
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "paging": false,
                    scrollCollapse: true,
                    scrollY: "280px",

                });
            </script>
            <script>
                $('#afectacion100').DataTable({
                    language: {
                        emptyTable: "Por favor realice su búsqueda"
                    },
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "paging": false,
                    scrollCollapse: true,
                    scrollY: "280px",
                });
            </script>

        </div>
        <script>
            var wb = XLSX.utils.book_new();
            wb.SheetNames.push("CON PLANTA");

            var ws = XLSX.utils.table_to_sheet(document.getElementById('porestacion'));
            wb.Sheets['POR EB'] = ws;

            var ws2 = XLSX.utils.table_to_sheet(document.getElementById('afectacion100'));
            wb.SheetNames.push("MUN100")
            wb.Sheets['MUN100'] = ws2;


            var wbout = XLSX.write(wb, {
                bookType: 'xlsx',
                bookSST: true,
                type: 'binary'
            });

            function s2ab(s) {
                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            }
            $("#generarexcel").click(function() {
                saveAs(new Blob([s2ab(wbout)], {
                    type: "application/octet-stream"
                }), 'consultaafectacion.xlsx');
            });
        </script>
    </form>
    <?php include_once('footer.php'); ?>
</div>