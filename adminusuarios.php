<?php
include('index.php');
$inc = include("con_db.php");


//Carga permisos generales en tabla
$permisotablas_sql = $con->query("SELECT estado FROM permisos_generales WHERE id='1'") or die(mysqli_error($con));
$permisotablas = $permisotablas_sql->fetch_assoc();

if ($_SESSION['perfil'] == "admin") {
?>

    <div class="contain-table">
        <script>
            $(function() {
                $('#tabla_proveedores').DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por pagina",
                        "zeroRecords": "No se encuentra el registro",
                        "info": "Mostrando pagina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "infoFiltered": "(filtrando de un total de _MAX_ total registros)"
                    },
                    ordering:  false

                });
            });
        </script>

    </div>
    <?php
    if ($inc) {
        $consulta = "SELECT * FROM usuarios ";
        $res = mysqli_query($con, $consulta);
        if ($res) {
    ?>

            <div class="container">
                <input type="hidden" id="areaselect" name="areaselect" value="FEE">
                <table id="tabla_proveedores" class="table table-hover order-column">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Email</th>
                            <th>Usuario</th>
                            <th>Perfil</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($row = $res->fetch_assoc()) : ?>
                            <div class="modal fade" id="staticBackdropEdit<?php echo $row['id']; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form action="includes/adminusers.php" method="post" id="form-modal">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title w-100 text-center" id="staticBackdropLabel">Editar usuario</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <input type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>">
                                                <div class="row mr-3 ml-2 mb-2">
                                                    <label>Nombres:</label>
                                                    <input class="form-control" type="text" id="nombres" name="nombres" value="<?php echo $row['nombres']; ?>" required>
                                                </div>
                                                <div class="row mr-3 ml-2 mb-2">
                                                    <label>Apellidos:</label>
                                                    <input class="form-control" type="text" id="apellidos" name="apellidos" value="<?php echo $row['apellidos']; ?>" required>
                                                </div>
                                                <div class="row mr-3 ml-2 mb-2">
                                                    <label>Email</label>
                                                    <input class="form-control" type="email" id="email" name="email" value="<?php echo $row['email']; ?>" required>
                                                </div>
                                                <div class="row mr-3 ml-2 mb-2">
                                                    <label>Usuario:</label>
                                                    <input class="form-control" type="text" id="usuario" name="usuario" value="<?php echo $row['usuario']; ?>" required>
                                                </div>
                                                <div class="row mr-3 ml-2 pb-0 justify-content-center">
                                                    <label>Perfil:</label>
                                                </div>
                                                <div class="row justify-content-center pt-0">
                                                    <select class="form-control" id="perfilvalue" name="perfilvalue" style="width:32%;">
                                                        <option value="admin" <?php if ($row['perfil'] == "admin") {
                                                                                echo ("selected");
                                                                            } ?>>Admin</option>
                                                        <option value="normaldb" <?php if ($row['perfil'] == "normaldb") {
                                                                                echo ("selected");
                                                                            } ?>>NormalDB</option>
                                                        <option value="normal" <?php if ($row['perfil'] == "normal") {
                                                                                echo ("selected");
                                                                            } ?>>Normal</option>
                                                        <option value="sin activar" <?php if ($row['perfil'] == "sin activar") {
                                                                                        echo ("selected");
                                                                                    } ?>>Sin activar</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" id="updateuser" name="updateuser" class="btn btn-primary">Actualizar</button>
                                            </div>
                                        </div>


                                    </form>
                                </div>
                            </div>
                            <div class="modal fade" id="staticBackdropDel<?php echo $row['id']; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form action="includes/adminusers.php" method="post" id="form-modal">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title w-100 text-center" id="staticBackdropLabel">Eliminar usuario</h5>
                                                <span type="button" class="close material-icons" data-dismiss="modal">highlight_off</span>

                                            </div>
                                            <div class="modal-body">
                                                <input type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>">
                                                <input type="hidden" id="user" name="user" value="<?php echo $row['usuario']; ?>">
                                                <h5>Está seguro de eliminar el siguiente usuario:</h5>
                                                <h4 class="text-center"><?php echo $row['usuario']; ?></h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" id="eliminarus" name="eliminarus" class="btn btn-danger">Eliminar</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                            <tr>
                                <td><?php echo $row['id']; ?></td>
                                <td><?php echo $row['nombres']; ?></td>
                                <td><?php echo $row['apellidos']; ?></td>
                                <td><?php echo $row['email']; ?></td>
                                <td><?php echo $row['usuario']; ?></td>
                                <td><?php echo $row['perfil']; ?></td>
                                <td>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#staticBackdropEdit<?php echo $row['id'] ?>">Editar</button>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#staticBackdropDel<?php echo $row['id'] ?>">Eliminar</button>
                                </td>
                            </tr>
                        <?php endwhile; ?>
                        </form>
                    </tbody>
                </table>
                <form id="formtablas" name="formatablas" action="includes/adminusers.php" method="post">
                    <div class="row justify-content-center mt-5" style="background: none;">
                        <label class="pr-3 pt-1">Mostrar/Ocultar búsqueda: </label>
                        <input type="checkbox" id="btnshowhidetablas" name="btnshowhidetablas" data-toggle="toggle" data-onstyle="dark" data-on="Si" data-off="No" data-style="fast" onchange="document.getElementById('showhidetablas').click()">
                        <input type="submit" id="showhidetablas" name="showhidetablas" style="visibility: hidden;">
                    </div>
                </form>
            </div>
            <?php include_once('footer.php'); ?>

<?php
        }
    }
}
?>

<script>
$(function() {
    $('#btnshowhidetablas').bootstrapToggle('<?php echo $permisotablas['estado']; ?>', true);
  })
</script>