<?php
include('index.php');
include('includes/proveedoresproceso.php');
?>

<?php
$proveedores_sql = $con->query("SELECT * FROM proveedores") or die(mysqli_error($con));
$proveedores_nom = $proveedores_sql->fetch_all();
?>

<div class="card text-center border-light mb-3">
    <div class="card-body">
        <form action="" method="POST">
            <div class="container">
                <div class="row justify-content-center mb-4" style="background: #FFFFFF;">
                    <div class="col-md-4">
                        <div>
                            <label>Nombre proveedor:</label>
                            <br>
                            <select name="busprov" id="busprov" class="selectpicker" data-live-search="true">
                                <option value="" selected disabled hidden>Seleccione</option>
                                <?php
                                foreach ($proveedores_nom as $nom) { ?>
                                    <?php if ($nom[1] == "") {
                                        continue;
                                    } ?>
                                    <option value="<?php if ($nom[1] != "") {
                                                        echo $nom[0];
                                                    } ?>"><?php if ($nom[1] != "") {
                                                                echo $nom[1];
                                                            } ?></option>
                                <?php } ?>
                            </select>
                            <button type="submit" id="bproveedor" name="bproveedor" class="btn btn btn-info btn-sm">Buscar</button>
                        </div>
                    </div>
                </div>
                <div class="row-12">
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <label>Nombre proveedor:</label>
                            <input type="text" class="form-control" id="electrificadora" name="electrificadora" value="<?php if (isset($proveedor)) {
                                                                                                                            echo $proveedor['electrificadora'];
                                                                                                                        } ?>">
                        </div>
                        <div class="col-md-7">
                            <label>Teléfono:</label>
                            <input type="text" class="form-control" id="telefono" name="telefono" value="<?php if (isset($proveedor)) {
                                                                                                                echo $proveedor['telefono'];
                                                                                                            } ?>">

                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-5">
                            <label>App:</label>
                            <input type="text" class="form-control" id="app" name="app" value="<?php if (isset($proveedor)) {
                                                                                                    echo $proveedor['app'];
                                                                                                } ?>">
                        </div>
                        <div class="col-md-7">
                            <label>Correo Electrónico:</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?php if (isset($proveedor)) {
                                                                                                        echo $proveedor['email'];
                                                                                                    } ?>">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-7">
                            <label>Página web:</label>
                            <input type="text" class="form-control" id="paginaweb" name="paginaweb" value="<?php if (isset($proveedor)) {
                                                                                                                echo $proveedor['pagina web'];
                                                                                                            } ?>">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <label>Formato de escalamiento:</label>
                            <textarea class="form-control" id="formatoesc" name="formatoesc" rows="8"><?php if (isset($proveedor)) {
                                                                                                            echo $proveedor['formato escalamiento'];
                                                                                                        } ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>