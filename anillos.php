<?php
include('index.php');
include('con_db.php');
?>
<?php
$textoinputanillos = "";
$textoinputnodos = "";
$ciudadporanillo = "";
$ciudadpornodo = "";
$textoinputlineales = "";
$ciudadlineales = "";

if (isset($_POST['searchrings'])) {
    $textoinputanillos = "";
    $ciudadporanillo = "";

    if (isset($_POST['consultaporanillos'])) {
        $textoinputanillos = $_POST['consultaporanillos'];
    } else {
        $textoinputanillos = "";
    }
    $text = $_POST['consultaporanillos'];
    if (empty($text)) {
        $_SESSION['message'] = "Campos vacios";
        $_SESSION['msg_type'] = "danger";
        echo '<meta http-equiv="refresh" content="0; URL=anillos">';
        exit();
    } else {
        $text = preg_split('/(\r\n|,)/', $text);
        $text = array_map('trim', $text);
        foreach ($text as &$format) {
            $format = "'" . $format . "'";
        }

        unset($format);

        $textsql = implode(',', $text);

        if (isset($_POST['ciudadporanillo'])) {
            $ciudadporanillo = $_POST['ciudadporanillo'];
        } else {
            $_SESSION['message'] = "Seleccione la ciudad";
            $_SESSION['msg_type'] = "danger";
            echo '<meta http-equiv="refresh" content="0; URL=anillos">';
        }

        $resultanillos = $con->query("SELECT id, CIUDAD, ANILLO, NOMBRE_NODO, INTERFAZ, PUERTO FROM anillos_nodos WHERE ANILLO IN ($textsql) AND CIUDAD = '$ciudadporanillo'") or die(mysqli_error($con));
        $consultaanillos = $resultanillos->fetch_all();
    }
}

if (isset($_POST['searchnodes'])) {
    $textoinputnodos = "";
    $ciudadpornodo = "";

    if (isset($_POST['consultapornodos'])) {
        $textoinputnodos = $_POST['consultapornodos'];
    } else {
        $textoinputnodos = "";
    }
    $text = $_POST['consultapornodos'];
    if (empty($text)) {
        $_SESSION['message'] = "Campos vacios";
        $_SESSION['msg_type'] = "danger";
        echo '<meta http-equiv="refresh" content="0; URL=anillos">';
        exit();
    } else {
        $text = preg_split('/(\r\n|,)/', $text);
        $text = array_map('trim', $text);
        $text = array_unique(array_filter($text));
        foreach ($text as &$format) {
            $format = "'" . $format . "'";
        }

        unset($format);

        $textsql = implode(',', $text);

        if (isset($_POST['ciudadpornodo'])) {
            $ciudadpornodo = $_POST['ciudadpornodo'];
        } else {
            $_SESSION['message'] = "Seleccione la ciudad";
            $_SESSION['msg_type'] = "danger";
            echo '<meta http-equiv="refresh" content="0; URL=anillos">';
        }

        $resultnodos = $con->query("SELECT id, CIUDAD, ANILLO, NOMBRE_NODO, INTERFAZ, PUERTO FROM anillos_nodos WHERE NOMBRE_NODO IN ($textsql) AND CIUDAD = '$ciudadpornodo'") or die(mysqli_error($con));
        $consultanodos = $resultnodos->fetch_all();
    }
}


if (isset($_POST['searchlineales'])) {
    $textoinputlineales = "";

    if (isset($_POST['consultalineales'])) {
        $textoinputlineales = $_POST['consultalineales'];
    } else {
        $textoinputlineales = "";
    }
    $text = $_POST['consultalineales'];
    if (empty($text)) {
        $_SESSION['message'] = "Campos vacios";
        $_SESSION['msg_type'] = "danger";
        echo '<meta http-equiv="refresh" content="0; URL=anillos">';
        exit();
    } else {
        $text = preg_split('/(\r\n|,)/', $text);
        $text = array_map('trim', $text);
        $text = array_unique(array_filter($text));
        foreach ($text as &$format) {
            $format = "'" . $format . "'";
        }

        unset($format);

        $textsql = implode(',', $text);


        $resultlineales = $con->query("SELECT id, CIUDAD, ANILLO, NOMBRE_NODO, INTERFAZ, PUERTO FROM lineales WHERE NOMBRE_NODO IN ($textsql)") or die(mysqli_error($con));
        $consultalineales = $resultlineales->fetch_all();
    }
}



?>

<div class="container mt-3">

    <form action="anillos.php" method="post" id="busquedasitios">
        <div class="row">
            <div class="col-4 text-center">
                <label style="font-weight:bold">CONSULTA POR ANILLOS:</label>
                <textarea class="form-control" id="consultaporanillos" name="consultaporanillos" rows="11"><?php echo $textoinputanillos; ?></textarea>
                <div class="row justify-content-center mt-2">
                    <div class="col-auto">
                        <select class="form-control" id="ciudadporanillo" name="ciudadporanillo">
                            <option value="" selected disabled hidden>Seleccione ciudad</option>
                            <?php
                            $ciudadessql = $con->query("SELECT DISTINCT CIUDAD FROM anillos_nodos ") or die(mysqli_error($con));
                            while ($row = $ciudadessql->fetch_assoc()) : ?>
                                <option value="<?php echo $row['CIUDAD']; ?>" <?php if ($ciudadporanillo == $row['CIUDAD']) {
                                                                                    echo ("selected");
                                                                                } ?>><?php echo $row['CIUDAD']; ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="col-auto">
                        <button type="submit" id="searchrings" name="searchrings" class="btn btn-dark">Buscar</button>
                    </div>
                </div>
            </div>
            <div class="col-8 text-center">
                <table id="poranillo" class="display compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>CIUDAD</th>
                            <th>ANILLO</th>
                            <th>NODO</th>
                            <th>INTERFAZ</th>
                            <th>PUERTO</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_POST['searchrings'])) {
                            for ($i = 0; $i < sizeof($consultaanillos); $i++) {
                        ?>

                                <tr>
                                    <td> <?php echo $consultaanillos[$i][1] ?> </td>
                                    <td> <?php echo $consultaanillos[$i][2] ?> </td>
                                    <td> <?php echo $consultaanillos[$i][3] ?> </td>
                                    <td> <?php echo $consultaanillos[$i][4] ?> </td>
                                    <td> <?php echo $consultaanillos[$i][5] ?> </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="row">
            <div class="col-4 text-center">
                <label style="font-weight:bold">CONSULTA POR NODOS:</label>
                <textarea class="form-control" id="consultapornodos" name="consultapornodos" rows="11"><?php echo $textoinputnodos; ?></textarea>
                <div class="row justify-content-center mt-2">
                    <div class="col-auto">
                        <select class="form-control" id="ciudadpornodo" name="ciudadpornodo">
                            <option value="" selected disabled hidden>Seleccione ciudad</option>
                            <?php
                            $ciudadessql = $con->query("SELECT DISTINCT CIUDAD FROM anillos_nodos ") or die(mysqli_error($con));
                            while ($row = $ciudadessql->fetch_assoc()) : ?>
                                <option value="<?php echo $row['CIUDAD']; ?>" <?php if ($ciudadpornodo == $row['CIUDAD']) {
                                                                                    echo ("selected");
                                                                                } ?>><?php echo $row['CIUDAD']; ?></option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <div class="col-auto">
                        <button type="submit" id="searchnodes" name="searchnodes" class="btn btn-dark">Buscar</button>
                    </div>
                </div>
            </div>
            <div class="col-8 text-center">
                <table id="pornodos" class="display compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>CIUDAD</th>
                            <th>ANILLO</th>
                            <th>NODO</th>
                            <th>INTERFAZ</th>
                            <th>PUERTO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_POST['searchnodes'])) {
                            for ($i = 0; $i < sizeof($consultanodos); $i++) {
                        ?>
                                <tr>
                                    <td> <?php echo $consultanodos[$i][1] ?> </td>
                                    <td> <?php echo $consultanodos[$i][2] ?> </td>
                                    <td> <?php echo $consultanodos[$i][3] ?> </td>
                                    <td> <?php echo $consultanodos[$i][4] ?> </td>
                                    <td> <?php echo $consultanodos[$i][5] ?> </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-4 text-center">
                <label style="font-weight:bold">ENLACES LINEALES:</label>
                <textarea class="form-control" id="consultalineales" name="consultalineales" rows="11"><?php echo $textoinputlineales; ?></textarea>
                <div class="row justify-content-center mt-2">
                    <div class="col-auto">
                        <button type="submit" id="searchlineales" name="searchlineales" class="btn btn-dark">Buscar</button>
                    </div>
                </div>
            </div>
            <div class="col-8 text-center">
                <table id="lineales" class="display compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>CIUDAD</th>
                            <th>ANILLO</th>
                            <th>NODO</th>
                            <th>INTERFAZ</th>
                            <th>PUERTO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_POST['searchlineales'])) {
                            for ($i = 0; $i < sizeof($consultalineales); $i++) {
                        ?>
                                <tr>
                                    <td> <?php echo $consultalineales[$i][1] ?> </td>
                                    <td> <?php echo $consultalineales[$i][2] ?> </td>
                                    <td> <?php echo $consultalineales[$i][3] ?> </td>
                                    <td> <?php echo $consultalineales[$i][4] ?> </td>
                                    <td> <?php echo $consultalineales[$i][5] ?> </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <script>
            $(document).ready(function() {
                $('#poranillo').DataTable();
                $('#pornodos').DataTable();
                $('#lineales').DataTable();
            });
        </script>
        <script>
            $('#poranillo').DataTable({
                language: {
                    emptyTable: "Por favor realice su búsqueda"
                },
                "searching": false,
                "ordering": false,
                "info": false,
                "paging": false,
                scrollCollapse: true,
                scrollY: "280px",

            });
        </script>
        <script>
            $('#pornodos').DataTable({
                language: {
                    emptyTable: "Por favor realice su búsqueda"
                },
                "searching": false,
                "ordering": false,
                "info": false,
                "paging": false,
                scrollCollapse: true,
                scrollY: "280px",
            });
        </script>
        <script>
            $('#lineales').DataTable({
                language: {
                    emptyTable: "Por favor realice su búsqueda"
                },
                "searching": false,
                "ordering": false,
                "info": false,
                "paging": false,
                scrollCollapse: true,
                scrollY: "280px",
            });
        </script>
    </form>
</div>
<?php include_once('footer.php'); ?>