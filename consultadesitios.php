<?php
include('index.php');
include('con_db.php');
?>
<?php
$textoinput = "";
if (isset($_POST['searchsites'])) {

    if (isset($_POST['consultasites'])) {
        $textoinput = $_POST['consultasites'];
    } else {
        $textoinput = "";
    }
    $text = $_POST['consultasites'];
    if (empty($text)) {
        session_start();
        $_SESSION['message'] = "Campos vacios";
        $_SESSION['msg_type'] = "danger";
        header("location: consultadesitios ");
        exit();
    } else {
        $text = preg_split('/(\r\n|,)/', $text);
        $text = array_map('trim', $text);
        foreach ($text as &$format) {
            $format = "'" . $format . "'";
        }

        unset($format);

        $textsql = implode(',', $text);

        $result = $con->query("SELECT id, T_EB, T_PLANTA, T_CRITICIDAD FROM datos WHERE T_EB IN ($textsql)") or die(mysqli_error($con));
        $consulta = $result->fetch_all();
    }
}

?>

<br>

<div class="container">

    <form action="consultadesitios" method="post" id="busquedasitios">
        <div class="row">
            <div class="col-4 text-center">
                <label style="font-weight:bold">CONSULTA DE SITIOS:</label>
                <textarea class="form-control" id="consultasites" name="consultasites" rows="26" required="required"><?php echo $textoinput; ?></textarea>
                <button type="submit" id="searchsites" name="searchsites" class="btn btn-dark mt-2">Buscar</button>
            </div>

            <div class="col-8 text-center">

                <table id="conplanta" class="display compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>SITIOS CON PLANTA</th>
                            <th>CRITICIDAD</th>
                            <th>PLANTA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_POST['searchsites'])) {
                            for ($i = 0; $i < sizeof($consulta); $i++) {
                                if ($consulta[$i][2] == "SI" || $consulta[$i][2] == "7X24" || $consulta[$i][2] == "7X12") {
                        ?> <tr>
                                        <td> <?php echo $consulta[$i][1]; ?> </td>
                                        <td> <?php echo $consulta[$i][3]; ?> </td>
                                        <td> <?php echo $consulta[$i][2]; ?> </td>
                                    </tr>
                        <?php
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <input type="hidden" name="tabla_conplanta" id="tabla_conplanta">
                <table id="sinplanta" class="display compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>SITIOS SIN PLANTA</th>
                            <th>CRITICIDAD</th>
                            <th>PLANTA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_POST['searchsites'])) {
                            for ($i = 0; $i < sizeof($consulta); $i++) {
                                if ($consulta[$i][2] == "SIN VERIFICAR" || $consulta[$i][2] == "SIN PLANTA" || $consulta[$i][2] == "SIN RESPALDO") {
                        ?> <tr>
                                        <td> <?php echo $consulta[$i][1]; ?> </td>
                                        <td> <?php echo $consulta[$i][3]; ?> </td>
                                        <td> <?php echo $consulta[$i][2]; ?> </td>
                                    </tr>
                        <?php
                                }
                            }
                        }

                        ?>
                    </tbody>
                </table>
                <button type="button" id="generarexcel" name="generarexcel" class="btn btn-dark mt-2">Excel</button>
            </div>
            <script>
                $(document).ready(function() {
                    $('#conplanta').DataTable();
                    $('#sinplanta').DataTable();
                });
            </script>
            <script>
                $('#conplanta').DataTable({
                    language: {
                        emptyTable: "Por favor realice su búsqueda"
                    },
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "paging": false,
                    scrollCollapse: true,
                    scrollY: "280px",

                });
            </script>
            <script>
                $('#sinplanta').DataTable({
                    language: {
                        emptyTable: "Por favor realice su búsqueda"
                    },
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "paging": false,
                    scrollCollapse: true,
                    scrollY: "280px",
                });
            </script>
            <script>
                var wb = XLSX.utils.book_new();
                wb.SheetNames.push("CON PLANTA");

                var ws = XLSX.utils.table_to_sheet(document.getElementById('conplanta'));
                wb.Sheets['CON PLANTA'] = ws;

                var ws2 = XLSX.utils.table_to_sheet(document.getElementById('sinplanta'));
                wb.SheetNames.push("SIN PLANTA")
                wb.Sheets['SIN PLANTA'] = ws2;


                var wbout = XLSX.write(wb, {
                    bookType: 'xlsx',
                    bookSST: true,
                    type: 'binary'
                });

                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }
                $("#generarexcel").click(function() {
                    saveAs(new Blob([s2ab(wbout)], {
                        type: "application/octet-stream"
                    }), 'consultadesitios.xlsx');
                });
            </script>
        </div>
    </form>
    <?php include_once('footer.php');?>
</div>