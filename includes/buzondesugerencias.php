<?php
require '../con_db.php';

if (isset($_POST['g-recaptcha-response'])) {
    $secretkey = "6LcPAzIaAAAAAK2V4yOK86vx8ReDqW3m5bt7zxCu";
    $ip = $_SERVER['REMOTE_ADDR'];
    $response = $_POST['g-recaptcha-response'];
    $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretkey&response=$response&remoteip=$ip";
    $fire = file_get_contents($url);
    $data = json_decode($fire);
    if ($data->success == true) {
        if (isset($_POST['envsugerencia'])) {
            if (isset($_POST['sugerencia'])) {
                $sugerencia = $_POST['sugerencia'];
                // Enviar email
                //require_once('../PHPMailer/PHPMailerAutoload.php');
                require_once('../Mailer/src/Exception.php');
                require_once('../Mailer/src/PHPMailer.php');
                require_once('../Mailer/src/SMTP.php');
                // $mail = new PHPMailer();
                $mail = new PHPMailer\PHPMailer\PHPMailer(true);
                $mail->CharSet = 'UTF-8';
                $mail->Encoding = 'base64';
                $mail­->SMTPDebug = 2;
                $mail->isSMTP();
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = 'ssl';
                $mail->Host = 'ns18.hostgator.co';
                $mail->Port = '465';
                $mail->Username = 'ticketcreador@incoelectronica.com';
                $mail->Password = '*T@URm&uQ9V0';

                $emails_sql = $con->query("SELECT email FROM usuarios where perfil='admin'") or die($con->error);

                $mail->setFrom('no-reply@ticketcreador.com');

                // $to = $useremail;
                session_start();

                $mail->Subject = 'Sugerencia en Ticketcreador';
                $mail->Body = '<!doctype html>
                <html lang="en-US">
                
                <head>
                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                    <title>Email Templates</title>
                    <meta name="description" content="Email Templates.">
                    <style type="text/css">
                        a:hover {
                            text-decoration: underline !important;
                        }
                    </style>
                </head>
                
                <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                    <!--100% body table-->
                    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: " Open Sans ", sans-serif;">
                        <tr>
                            <td>
                                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="height:30px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                <tr>
                                                    <td style="height:5px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 35px;">
                                                        <h1 style="color:#1e1e2d; font-weight:400; margin:0;font-size:28px;font-family:"Rubik",sans-serif;font-weight: bold;">TICKETCREADOR</h1>
                                                        <span style="display:inline-block; vertical-align:middle; margin:13px 0 16px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin-top: 0px;">
                                                            Se le informa que el usuario ' . $_SESSION['logged'] . ' ha enviado la siguiente sugerencia:
                                                        </p>
                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:15px 0px 15px 0px;">
                                                            ' . $sugerencia . '
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height:40px;">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <tr>
                                            <td style="height:20px;">&nbsp;</td>
                                        </tr>
                
                                        <tr>
                                            <td style="height:30px;">&nbsp;</td>
                                        </tr>
                                </table>
                            </td>
                            </tr>
                    </table>
                    <!--/100% body table-->
                </body>
                
                </html>';
                //$mail->addAddress('andresbello98@gmail.com');
                while ($email = $emails_sql->fetch_assoc()) {
                    $mail->addAddress(strtolower(trim($email['email'])));
                }

                $mail->isHTML(true);
                $mail->Send();

                $_SESSION['message'] = "Sugerencia enviada";
                $_SESSION['msg_type'] = "success";
                header("location: ../sugerencias");
                exit();
            }
        }
    }else{
        session_start();
        $_SESSION['message'] = "Valide el recaptcha";
        $_SESSION['msg_type'] = "danger";
        header("location: ../sugerencias");
        exit();
    }
}
