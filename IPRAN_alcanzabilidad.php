<?php
include('index.php');
?>

<?php



?>

<div class="card text-center border-light mb-3">
    <div class="card-body">
        <div class="container">
            <form action="" method="POST" id="ipran_alcanzabilidad_form">
                <div class="row">
                    <div class="col-md-5">
                        <label class="text-center">Información de equipos</label>
                        <div class="row justify-content-center">
                            <label>Equipo:</label>
                            <div class="input-group">
                                <input type="text" id ="equipo" name="equipo" onfocusout="mostrarequipos()" class="form-control">
                                <div class="input-group-append">
                                    <button type="button" onclick="addsplitter()" class="btn btn-primary">add</button>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <label>Fecha hora de inicio: </label>
                            <input type="text" id="date" name="date" class="form-control">
                        </div>
                        <div class="row justify-content-center">
                            <label>Descripción de la falla:</label>
                            <textarea class="form-control" name="descfalla" id="descfalla" rows="3"></textarea>
                        </div>
                        
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <label>Intermitente</label>
                                <br>
                                <input type="checkbox" id="intermitencia_ipran" name="intermitencia_ipran" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No"data-style="fast">
                            </div>
                            <div class="col-auto">
                                <label>Troncal lineal</label>
                                <br>
                                <input type="checkbox" id="troncal-lineal" name="troncal-lineal" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No"data-style="fast">
                            </div>
                            <div class="col-auto">
                                <label>Troncal respaldo</label>
                                <br>
                                <input type="checkbox" id="troncal-respaldo" name="troncal-respaldo" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No"data-style="fast">
                            </div>
                            <div class="col-auto mt-2">
                                <label>Comparten hardware</label>
                                <br>
                                <input type="checkbox" id="compartenhrdw" name="compartenhrdw" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No"data-style="fast">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <label>Falla de energía</label>
                                <select class="form-control" name="fallaenergia" id="fallaenergia">
                                    <option value="" selected disabled hidden>Seleccione</option>
                                    <option value="SI">SI</option>
                                    <option value="NO">NO</option>
                                    <option value="ENVALIDACION">EN VALIDACIÓN</option>
                                </select>
                            </div>
                            <div class="col-auto">
                                <label>Proveedor</label>
                                <select class="form-control" name="proveedor" id="proveedor">
                                    <option value="" selected disabled hidden>Seleccione</option>
                                    <option value="SDH">SDH</option>
                                    <option value="LAZUS">LAZUS</option>
                                    <option value="PLANTA EXTERNA">PLANTA EXTERNA</option>
                                    <option value="AZTECA">AZTECA</option>
                                    <option value="MICROONDAS">MICROONDAS</option>
                                    <option value="UFINET">UFINET</option>
                                    <option value="ALCATEL">ALCATEL</option>
                                </select>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="row">
                                <div class="col-auto">
                                    <label>Ventana de mantenimiento en curso:</label>
                                    <br>
                                    <input type="checkbox" id="ventanademantenimiento" name="ventanademantenimiento" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No"data-style="fast">
                                </div>
                            </div>
                            <div class="row" id="div-ventana" name="div-ventana">
                                <div class="col-auto">
                                    <input type="text" class="form-control" id="textventana1">
                                    <br>
                                    <textarea class="form-control" name="textventana2" id="textventana2" cols="50" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <label>Escalamiento con Planta Externa o Proveedor de Fibra:</label>
                                <br>
                                <input type="checkbox" id="escalamientopop" name="escalamientopop" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No"data-style="fast">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="row mr-1">
                                <div class="col-auto">
                                    <label>Ticket Carrier:</label>
                                    <br>
                                    <input type="checkbox" id="ticketcarrier" name="ticketcarrier" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No"data-style="fast">
                                </div>
                            </div>
                            <div class="row" id="div-tcarrier" name="div-tcarrier">
                                <div class="col-auto">
                                    <input type="text" class="form-control" id="texttcarrier1">
                                    <br>
                                    <textarea class="form-control" name="texttcarrier2" id="texttcarrier2" cols="50" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row ml-1">
                                <div class="col-auto">
                                    <label>Ticket Remedy:</label>
                                    <br>
                                    <input type="checkbox" id="ticketremedy" name="ticketremedy" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No"data-style="fast">
                                </div>
                            </div>
                            <div class="row" id="div-tremedy" name="div-tremedy">
                                <div class="col-auto">
                                    <input type="text" class="form-control" id="texttremedy1">
                                    <br>
                                    <textarea class="form-control" name="texttremedy2" id="texttremedy2" cols="50" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="modal fade" id="ModalTxt" name="ModalTxt" tabindex="-1" role="dialog" aria-labelledby="ModalTxtLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                            <div class="modal-body">
                                <textarea class="form-control" id="textoplano" name="textoplano" rows="25" style="white-space: nowrap;"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="row justify-content-center">
                            <label>Equipos efectados:</label>
                            <textarea class="form-control" name="equiposafectados" id="equiposafectados" rows="6"></textarea>
                        </div>
                        <div class="row justify-content-center">
                            <label>Troncales afectadas:</label>
                            <textarea class="form-control" name="troncalesafectadas" id="troncalesafectadas" rows="6"></textarea>
                        </div>
                        <div class="row justify-content-center">
                            <label>Log de Alarmas:</label>
                            <textarea class="form-control" name="logdealarmas" id="logdealarmas" rows="6"></textarea>
                        </div>
                        <div class="row justify-content-center">
                            <button type="button" id="nuevo_ipran" name="nuevo_ipran" class="btn btn btn-info">Nuevo</button>
                            <button type="button" onclick="showmodal()" id="generar_ipran" name="generar_ipran" class="btn btn-outline-secondary">Generar</button>
                        </div>
                    </div>
                </div>
            </form>

            <script type="text/javascript" src="js/scripts_ipran_alcan.js"></script>
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>