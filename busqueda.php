<?php
include("index.php");
include("con_db.php");

//Carga permisos generales en tabla
$permisotablas_sql = $con->query("SELECT estado FROM permisos_generales WHERE id='1'") or die(mysqli_error($con));
$permisotablas = $permisotablas_sql->fetch_assoc();

$id = null;
$nombre = null;
$CRC = null;
$regional = null;
$informaciondelsitio = null;
$planta = null;
$lugar = null;
$cnocvip = null;
$ecp = null;
$bloqueo = null;
$electrificadora = null;
$nie = null;
$numero = null;
$criticidad = null;
$impacto = null;
$satpost = null;
$usms = null;
$netnumen = null;
$plancorp = null;
$municipio = null;
$direccion = null;
$fecha = null;
$observacion = null;
$vip = null;


if (isset($_POST['buscar'])) {
    $data = $_POST['buscartxt'];
    $res = $con->query("SELECT * FROM datos WHERE T_EB='$data'") or die(mysqli_error($con));
    $mensaje = $con->query("SELECT MENSAJE FROM mensajes_eb WHERE EB='$data'") or die(mysqli_error($con));
    $mensaje_res = $mensaje->fetch_assoc();
    $row = $res->fetch_assoc();
    $id = $row['id'];
    $nombre = $row['T_EB'];
    $cnocvip = $row['T_CNOCVIP'];
    $ecp = $row['T_ECP'];
    $CRC = $row['T_CRC'];
    $regional = $row['T_REGIONAL'];
    $informaciondelsitio = $row['T_INFSITIO'];
    $lugar = $row['T_LUGAR'];
    $planta = $row['T_PLANTA'];
    $bloqueo = $row['T_BLOQUEO'];
    $electrificadora = $row['T_ELECTRIFICADORA'];
    $nie = $row['T_NIE'];
    $numero = $row['T_NUMERO'];
    $criticidad = $row['T_CRITICIDAD'];
    $impacto = $row['T_IMPACTO'];
    $satpost = $row['T_SATELITAL'];
    $usms = $row['T_USMS'];
    $netnumen = $row['T_NETNUMEN'];
    $plancorp = $row['T_PLANCORP'];
    $municipio = $row['T_MUNICIPIO'];
    $direccion = $row['T_DIRECCION'];
    $fecha = $row['T_FECHA'];
    $observacion = $row['T_OBSERVACION'];
    $vip = $row['VIP'];


    //TRAFICO

    $sqlgsm = "SELECT DISTINCT MUNICIPIO FROM dbtrafico_gsm WHERE NOMBRE_EB_GSM=?";
    $sqlumts = "SELECT DISTINCT MUNICIPIO FROM dbtrafico_umts WHERE NOMBRE_EB_UMTS=?";
    $sqllte = "SELECT DISTINCT MUNICIPIO FROM dbtrafico_lte WHERE NOMBRE_EB_LTE=?";

    $stmtgsm = mysqli_stmt_init($con);
    $stmtumts = mysqli_stmt_init($con);
    $stmtlte = mysqli_stmt_init($con);


    if (!mysqli_stmt_prepare($stmtgsm, $sqlgsm) || !mysqli_stmt_prepare($stmtumts, $sqlumts) || !mysqli_stmt_prepare($stmtlte, $sqllte)) {
        $_SESSION['message'] = "Error base de datos";
        $_SESSION['msg_type'] = "danger";
    } else {
        mysqli_stmt_bind_param($stmtgsm, "s", $data);
        mysqli_stmt_execute($stmtgsm);
        $resultgsm = mysqli_stmt_get_result($stmtgsm);

        mysqli_stmt_bind_param($stmtumts, "s", $data);
        mysqli_stmt_execute($stmtumts);
        $resultumts = mysqli_stmt_get_result($stmtumts);

        mysqli_stmt_bind_param($stmtlte, "s", $data);
        mysqli_stmt_execute($stmtlte);
        $resultlte = mysqli_stmt_get_result($stmtlte);

        if ($rowgsm = mysqli_fetch_assoc($resultgsm)) {
            $municipiogsm = $con->query("SELECT DISTINCT NOMBRE_EB_GSM FROM dbtrafico_gsm WHERE MUNICIPIO='$rowgsm[MUNICIPIO]'") or die(mysqli_error($con));
            $ebmun_gsm = floor(((1 / sizeof($municipiogsm->fetch_all())) * 100000)) / 1000;
            if ($rowumts = mysqli_fetch_assoc($resultumts)) {
                $municipioumts = $con->query("SELECT DISTINCT NOMBRE_EB_UMTS FROM dbtrafico_umts WHERE MUNICIPIO='$rowumts[MUNICIPIO]'") or die(mysqli_error($con));
                $ebmun_umts = floor(((1 / sizeof($municipioumts->fetch_all())) * 100000)) / 1000;
            }
            if ($rowlte = mysqli_fetch_assoc($resultlte)) {
                $municipiolte = $con->query("SELECT DISTINCT NOMBRE_EB_LTE FROM dbtrafico_lte WHERE MUNICIPIO='$rowlte[MUNICIPIO]'") or die(mysqli_error($con));
                $ebmun_lte = floor(((1 / sizeof($municipiolte->fetch_all())) * 100000)) / 1000;
            }
        } elseif ($rowumts = mysqli_fetch_assoc($resultumts)) {
            $municipioumts = $con->query("SELECT DISTINCT NOMBRE_EB_UMTS FROM dbtrafico_umts WHERE MUNICIPIO='$rowumts[MUNICIPIO]'") or die(mysqli_error($con));
            $ebmun_umts = floor(((1 / sizeof($municipioumts->fetch_all())) * 100000)) / 1000;
            if ($rowlte = mysqli_fetch_assoc($resultlte)) {
                $municipiolte = $con->query("SELECT DISTINCT NOMBRE_EB_LTE FROM dbtrafico_lte WHERE MUNICIPIO='$rowlte[MUNICIPIO]'") or die(mysqli_error($con));
                $ebmun_lte = floor(((1 / sizeof($municipiolte->fetch_all())) * 100000)) / 1000;
            }
        } elseif ($rowlte = mysqli_fetch_assoc($resultlte)) {
            $municipiolte = $con->query("SELECT DISTINCT NOMBRE_EB_LTE FROM dbtrafico_lte WHERE MUNICIPIO='$rowlte[MUNICIPIO]'") or die(mysqli_error($con));
            $ebmun_lte = floor(((1 / sizeof($municipiolte->fetch_all())) * 100000)) / 1000;
            if ($rowumts = mysqli_fetch_assoc($resultumts)) {
                $municipioumts = $con->query("SELECT DISTINCT NOMBRE_EB_UMTS FROM dbtrafico_umts WHERE MUNICIPIO='$rowumts[MUNICIPIO]'") or die(mysqli_error($con));
                $ebmun_umts = floor(((1 / sizeof($municipioumts->fetch_all())) * 100000)) / 1000;
            }
        }
    }
}
?>

<?php
$tablagsm = null;
if (isset($_POST['fill_gsm'])) {
    $nombre = $_POST['nombrehidden2'];
    $tablagsm_sql = $con->query("SELECT * FROM consulta_sectores_gsm WHERE BCF_NAME='$nombre'") or die(mysqli_error($con));
    $tablagsm = $tablagsm_sql->fetch_all();
}

if (isset($_POST['fill_umts'])) {
    $nombre = $_POST['nombrehidden2'];
    $tablaumts_sql = $con->query("SELECT * FROM consulta_sectores_umts WHERE WBTS_NAME='$nombre'") or die(mysqli_error($con));
    $tablaumts = $tablaumts_sql->fetch_all();
}

if (isset($_POST['fill_lte'])) {
    $nombre = $_POST['nombrehidden2'];
    $tablalte_sql = $con->query("SELECT * FROM consulta_sectores_lte WHERE LNBTS_NAME='$nombre'") or die(mysqli_error($con));
    $tablalte = $tablalte_sql->fetch_all();
}
?>
<div class="card text-center border-light mb-3">
    <div class="card-body">
        <div class="container">
            <form action="busqueda" id="form" name="form" method="post">
                <div class="form-row justify-content-center">
                    <input type="text" id="buscartxt" name="buscartxt" class="form-control" method="post" placeholder="Buscar..." style="width:75%;" autocomplete="off" onfocus="this.value=''" required>
                    <button type="submit" id="buscar" name="buscar" value="buscar" class="btn btn-primary" style="width:auto;">Buscar</button>
                    <div style="width:60%;">
                        <div class="list-group" id="show-list" name="show-list">
                            <!-- <a href="#" class="list-group-item list-group-item-action border-1">Busqueda</a> -->
                        </div>
                    </div>
                </div>
            </form>
            <br>
            <form action="proceso" id="formproceso" name="formproceso" method="post">
                <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
                <input type="hidden" id="nombrehidden" name="nombrehidden" value="<?php echo $nombre; ?>">
                <div class="row">
                    <div class="col-md-5">
                        <div class="row justify-content-center">
                            <label for="inputnombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre ?>" disabled>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-7 pb-2">
                                <label for="siglasingeniero">Siglas Ingeniero</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="user" name="user" placeholder="Who" required>
                                    <div class="input-group-append">
                                        <select class="form-control" id="typeu" name="typeu" required>
                                            <option value="A" selected>A</option>
                                            <option value="S">S</option>
                                            <option value="C">C</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 pt-2">
                                <br>
                                <label for="Turno T11">Turno T11:</label>
                                <input type="checkbox" id="turnot11" name="turnot11" data-toggle="toggle" data-onstyle="dark" data-on="Si" data-off="No" data-style="fast">
                            </div>
                        </div>
                        <div id="divcasosdeuso" name="divcasosdeuso" class="row justify-content-center pb-4 pt-2">
                            <div class="col-md-6">
                                <label for="inputcasosdeuso">Casos de uso:</label>
                                <select class="form-control" id="casosdeuso" name="casosdeuso">
                                    <option value="NO ESPECIFICADO" selected hidden>Sin especificar</option>
                                    <option value="ALTATEMP">Alta temperatura </option>
                                    <option value="PLANTAEN">Planta encendida</option>
                                    <option value="FUERADES">Fuera de servicio</option>
                                    <option value="FALLAALCATEL">Falla Alcatel</option>
                                    <option value="FALLASDH">Falla SDH</option>
                                    <option value="POWERBATERIA">Power en batería</option>
                                    <option value="BAJOVOLTAJEB">Bajo voltaje en baterías</option>
                                </select>
                            </div>
                        </div>
                        <div class="row justify-content-center pb-4 pt-2" id="divsintrafico" style="display:none;">
                            <div class="col-md-6">
                                <label for="inputsintrafico">Casos de uso:</label>
                                <select class="form-control" id="sintrafico" name="sintrafico">
                                    <option value="FUERADES" selected>Fuera de servicio</option>
                                    <option value="DATOS">Sin tráfico datos</option>
                                    <option value="VOZ">Sin tráfico voz</option>
                                    <option value="VOZ-DATOS">Sin tráfico voz/datos</option>
                                    <option value="PERDGESTION">Pérdida de gestión</option>
                                </select>
                            </div>
                        </div>
                        <div id="casoalcatelsdh" name="casoalcatelsdh" class="row justify-content-center" style="display:none;">
                            <div class="col-auto">
                                <label for="inputequipos">Equipo:</label>
                                <input type="text" class="form-control" name="equipocaso" id="equipocaso" onfocusout="diagnosticocaso()">
                            </div>
                            <div class="col-auto mt-2">
                                <label for="inputalarmacaso">Alarma:</label>
                                <input type="text" class="form-control" name="alarmacaso" id="alarmacaso" onfocusout="diagnosticocaso()">
                            </div>
                        </div>
                        <div id="divsectores" name="divsectores" class="row justify-content-center" style="display:none;">
                            <div class="col-auto">
                                <label for="inputsectores">Sectores:</label>
                                <input type="text" class="form-control" name="sectores" id="sectores">
                            </div>
                        </div>
                        <div id="casogm" name="casogm" class="row justify-content-center" style="display:none;">
                            <div class="col-auto">
                                <label for="inputalarmacasogm">Alarma:</label><br>
                                <select class="selectpicker" id="selectalarmagm" name="selectalarmagm" data-live-search="true" data-size="5" onchange="alarmagm()">
                                    <option data-tokens="" selected disabled hidden>Seleccione alarma</option>
                                    <?php
                                    $alarmas_sql = $con->query("SELECT * FROM alarmasgm") or die(mysqli_error($con));
                                    $alarmas = $alarmas_sql->fetch_all();

                                    foreach ($alarmas as $alarma) { ?>
                                        <option value="<?php echo $alarma[2][0] . "&" . $alarma[1] . "&" . $alarma[3]; ?>" style="<?php if ($alarma[2][0] == "2") {
                                                                                                                                        echo "background: #ff4747; color: #fff;";
                                                                                                                                    } elseif ($alarma[2][0] == "3") {
                                                                                                                                        echo "background: #ff6400; color: #fff;";
                                                                                                                                    } elseif ($alarma[2][0] == "4") {
                                                                                                                                        echo "background: #ffc300; color: #000;";
                                                                                                                                    } elseif ($alarma[2][0] == "5") {
                                                                                                                                        echo "background: #ffc300; color: #000;";
                                                                                                                                    } ?>"> <?php echo $alarma[1]; ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-auto">
                                <label for="inputalarmacasogm">Criticidad</label><br>
                                <div class="boxcrit" id="critalarmagm" name="critalarmagm"></div>
                            </div>
                        </div>
                        <div class="row justify-content-center" id="blporenergia" name="blporenergia" style="display: none;">
                            <div class="col-auto">
                                <select class="form-control" id="automanual" name="automanual">
                                    <option value="auto" selected>Automática</option>
                                    <option value="manual">Manual</option>
                                </select>
                            </div>
                        </div>
                        <div class="row justify-content-center" id="divcheckbv" name="divcheckbv" style="display: none;">
                            <div class="col-auto">
                                <label>Bajo voltaje:</label>
                                <br>
                                <input type="checkbox" id="checkbv" name="checkbv" data-toggle="toggle" data-onstyle="success" data-on="Si" data-off="No" data-style="fast" data-size="sm">
                            </div>
                        </div>
                        <label for="tecnologias" style="font-weight: bold;">Si tiene afectación marque según la tecnología</label>
                        <div class="row justify-content-center pb-4">
                            <div class="col-auto">
                                <label for="inputnombre">GSM</label>
                                <br>
                                <input type="checkbox" id="GSM" name="GSM" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                            </div>
                            <div class="col-auto">
                                <label for="inputnombre">UMTS</label>
                                <br>
                                <input type="checkbox" id="UMTS" name="UMTS" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                            </div>
                            <div class="col-auto">
                                <label for="inputnombre">LTE</label>
                                <br>
                                <input type="checkbox" id="LTE" name="LTE" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <label for="inputintermitencia">Intermitencia</label><br>
                                <input type="checkbox" id="intermitencia" name="intermitencia" data-toggle="toggle" data-onstyle="warning" data-on="Si" data-off="No" data-style="fast">
                            </div>
                            <div class="col-auto">
                                <label for="inputreinicio">Reinicio</label><br>
                                <input type="checkbox" id="reinicio" name="reinicio" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast" data-width="60">
                            </div>
                            <div class="col-auto">
                                <label for="input100municipio">Municipio 100%</label><br>
                                <input type="checkbox" id="100municipio" name="100municipio" data-toggle="toggle" data-onstyle="success" data-on="Si" data-off="No" data-style="fast" data-width="60">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-auto">
                                <input type="checkbox" id="vng" name="vng" data-toggle="toggle" data-onstyle="success" data-on="VNG" data-off="VNG" data-style="fast">
                            </div>
                            <div class="col-md-auto">
                                <input type="checkbox" id="tx" name="tx" data-toggle="toggle" data-onstyle="success" data-on="TX" data-off="TX" data-style="fast">
                            </div>
                            <div class="col-md-auto">
                                <input type="checkbox" id="pnmsj" name="pnmsj" data-toggle="toggle" data-onstyle="success" data-on="PNMSJ" data-off="PNMSJ" data-style="fast">
                            </div>
                            <div class="col-md-auto">
                                <input type="checkbox" id="cod" name="cod" data-toggle="toggle" data-onstyle="success" data-on="(COD)" data-off="(COD)" data-style="fast">
                            </div>
                            <div class="col-md-auto mt-3">
                                <label data-toggle="tooltip" data-placement="top" title="Reinicio recurrente">RST_RECU</label>
                                <br>
                                <input type="checkbox" id="rst_recu" name="rst_recu" data-toggle="toggle" data-onstyle="success" data-on="Si" data-off="No" data-style="fast">
                            </div>
                            <div class="col-md-auto mt-3">
                                <label for="Validacion de radios">Validacion de radios:</label>
                                <br>
                                <input type="checkbox" id="validacionderadios" name="validacionderadios" data-toggle="toggle" data-onstyle="success" data-on="Si" data-off="No" data-style="fast">
                            </div>
                        </div>
                        <div class="row justify-content-center pt-4">
                            <div class="col-md-10">
                                <label for="idtrabajo">ID Trabajo</label>
                                <input type="text" class="form-control" id="idtrabajo" name="idtrabajo">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <label for="inputTicketFSP">Ticket FSP</label>
                                <input type="text" class="form-control" id="ticketFSP" name="ticketFSP">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <label for="Diagnostico">Diagnóstico</label>
                                <textarea class="form-control" id="diagnostico" name="diagnostico" rows="5"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-7">
                        <div class="row justify-content-center">
                            <div class="col-md-4">
                                <label for="inputlugar">Lugar</label>
                                <input type="text" class="form-control" id="lugar" name="lugar" value="<?php echo $lugar ?>">
                            </div>
                            <div class="col-md-4">
                                <label for="inputplanta">Planta</label>
                                <select class="form-control" id="planta" name="planta">
                                    <option value="" selected disabled hidden>Seleccione</option>
                                    <option value="SIN PLANTA" <?php if ($planta == "SIN PLANTA") {
                                                                    echo ("selected");
                                                                } ?>>SIN PLANTA</option>
                                    <option value="SI" <?php if ($planta == "SI") {
                                                            echo ("selected");
                                                        } ?>>SI</option>
                                    <option value="7X24" <?php if ($planta == "7X24") {
                                                                echo ("selected");
                                                            } ?>>7X24</option>
                                    <option value="7X12" <?php if ($planta == "7X12") {
                                                                echo ("selected");
                                                            } ?>>7X12</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="inputplanta">Criticidad</label>
                                <select class="form-control" id="criticidad" name="criticidad">
                                    <option value="" selected disabled hidden>Seleccione</option>
                                    <option value="N/A" <?php if ($criticidad == "N/A") {
                                                            echo ("selected");
                                                        } ?>>N/A</option>
                                    <option value="CCM" <?php if ($criticidad == "CCM") {
                                                            echo ("selected");
                                                        } ?>>CCM</option>
                                    <option value="CRITICA" <?php if ($criticidad == "CRITICA") {
                                                                echo ("selected");
                                                            } ?>>CRITICA</option>
                                    <option value="ALTA" <?php if ($criticidad == "ALTA") {
                                                                echo ("selected");
                                                            } ?>>ALTA</option>
                                    <option value="MEDIA" <?php if ($criticidad == "MEDIA") {
                                                                echo ("selected");
                                                            } ?>>MEDIA</option>
                                    <option value="BAJA" <?php if ($criticidad == "BAJA") {
                                                                echo ("selected");
                                                            } ?>>BAJA</option>
                                </select>
                            </div>
                        </div>
                        <div class="row justify-content-center pb-0">

                            <div class="row">
                                <div class="col-auto">
                                    <label for="inputbloqueo">BL ENERGÍA</label>
                                    <br>
                                    <input type="checkbox" id="bloqueo" name="bloqueo" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                                </div>
                                <div class="col-auto">
                                    <label for="inputcnocvip">CNOCVIP</label>
                                    <br>
                                    <input type="checkbox" id="cnocvip" name="cnocvip" data-toggle="toggle" data-onstyle="info" data-on="Si" data-off="No" data-style="fast">
                                </div>
                                <div class="col-auto">
                                    <label for="inputecp">ECP</label>
                                    <br>
                                    <input type="checkbox" id="ecp" name="ecp" data-toggle="toggle" data-onstyle="info" data-on="Si" data-off="No" data-style="fast">
                                </div>
                                <div class="col-auto">
                                    <label for="inputnombre">CRC</label>
                                    <br>
                                    <input type="checkbox" id="CRC" name="CRC" data-toggle="toggle" data-onstyle="warning" data-on="Si" data-off="No" data-style="fast">
                                </div>
                                <div class="col-auto">
                                    <label for="inputvip">VIP</label>
                                    <br>
                                    <input type="checkbox" id="vip" name="vip" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-auto">
                                    <label id="labelplancorp" name="labelplancorp" for="inputplancorp" data-toggle="tooltip" data-placement="top" title="Exclusión por plan corporativo">PLAN CORP</label>
                                    <br>
                                    <input type="checkbox" id="plancorp" name="plancorp" data-toggle="toggle" data-onstyle="danger-2" data-on="Si" data-off="No" data-style="fast">
                                </div>
                                <div class="col-auto">
                                    <label for="inputsatelital">SAT/POSTE/TMX</label>
                                    <select class="form-control" id="sat-poste" name="sat-poste">
                                        <option value="NO" <?php if ($satpost == "NO") {
                                                                echo ("selected");
                                                            } ?> selected>NO</option>
                                        <option value="SATELITAL" <?php if ($satpost == "SATELITAL") {
                                                                        echo ("selected");
                                                                    } ?>>SATELITAL</option>
                                        <option value="POSTE" <?php if ($satpost == "POSTE") {
                                                                    echo ("selected");
                                                                } ?>>POSTE</option>
                                        <option value="TMX" <?php if ($satpost == "TMX") {
                                                                echo ("selected");
                                                            } ?>>TMX</option>
                                        <option value="RPT" <?php if ($satpost == "RPT") {
                                                                echo ("selected");
                                                            } ?>>RPT</option>
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <label for="impacto">Impacto</label>
                                    <select class="form-control" id="impacto" name="impacto">
                                        <option value="" selected disabled hidden>Seleccione</option>
                                        <option value="NO" <?php if ($impacto == "NO") {
                                                                echo ("selected");
                                                            } ?>>NO</option>
                                        <option value="MID" <?php if ($impacto == "MID") {
                                                                echo ("selected");
                                                            } ?>>MID</option>
                                        <option value="AID" <?php if ($impacto == "AID") {
                                                                echo ("selected");
                                                            } ?>>AID</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row justify-content-center" id="divelecnum">
                            <div class="col-md-5">
                                <label for="inputelectri" id="inpelectrificadora">Electrificadora</label>
                                <input type="text" class="form-control" id="electrificadora" name="electrificadora" value="<?php echo $electrificadora ?>">
                            </div>
                            <div class="col-md-3">
                                <label for="inputNIE">NIE</label>
                                <input type="text" class="form-control" id="nie" name="nie" value="<?php echo $nie ?>">
                            </div>
                            <div class="col-md-4">
                                <label for="inputnumero">Contacto electrificadora</label>
                                <input type="text" class="form-control" id="numero" name="numero" value="<?php echo $numero ?>">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-7">
                                <div class="form-row justify-content-md-center rounded pt-1" style="border:1px solid #ced4da">
                                    <div class="col-md-auto pr-3">
                                        <label id="labelgsm1">GSM: </label>
                                        <br>
                                        <label id="labelgsm"><?php if (isset($ebmun_gsm)) {
                                                                    echo $ebmun_gsm . "%";
                                                                } else {
                                                                    echo "-";
                                                                } ?></label>
                                    </div>
                                    <div class="col-md-auto pr-3">
                                        <label id="labelumts1">UMTS: </label>
                                        <br>
                                        <label id="labelumts" class="pr-2"><?php if (isset($ebmun_umts)) {
                                                                                echo $ebmun_umts . "%";
                                                                            } else {
                                                                                echo "-";
                                                                            } ?></label>
                                    </div>
                                    <div class="col-md-auto pr-3">
                                        <label id="labellte1">LTE: </label>
                                        <br>
                                        <label id="labellte"><?php if (isset($ebmun_lte)) {
                                                                    echo $ebmun_lte . "%";
                                                                } else {
                                                                    echo "-";
                                                                } ?></label>
                                    </div>
                                    <div class="col-md-4">
                                        <label id="labelmuncp1">MUNICIPIO: </label>
                                        <br>
                                        <label id="labelmuncp"><?php if (isset($rowgsm['MUNICIPIO'])) {
                                                                    echo $rowgsm['MUNICIPIO'];
                                                                } elseif (isset($rowumts['MUNICIPIO'])) {
                                                                    echo $rowumts['MUNICIPIO'];
                                                                } elseif (isset($rowlte['MUNICIPIO'])) {
                                                                    echo $rowlte['MUNICIPIO'];
                                                                } else {
                                                                    echo "-";
                                                                } ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 my-auto">
                                <div class="row">
                                    <div class="col-md-5">
                                        <input type="checkbox" id="usms" name="usms" data-toggle="toggle" data-onstyle="danger" data-on="USMS" data-off="USMS" data-style="fast">
                                    </div>
                                    <div class="col-md-5">
                                        <input type="checkbox" id="netnumen" name="netnumen" data-toggle="toggle" data-onstyle="danger" data-on="NETNUMEN" data-off="NETNUMEN" data-style="fast">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <label for="inputregional">Regional</label>
                                <input type="text" class="form-control" id="regional" name="regional" value="<?php echo $regional ?>">
                            </div>
                            <div class="col-md-6">
                                <label for="inputfecha">Fecha</label>
                                <input type="text" class="form-control" id="fecha" name="fecha" value="<?php echo $fecha ?>">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-5">
                                <label for="inputmunicipio">Municipio</label>
                                <input type="text" class="form-control" id="municipio" name="municipio" value="<?php echo $municipio ?>">
                            </div>
                            <div class="col-md-7">
                                <label for="inputdireccion">Direccion</label>
                                <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $direccion ?>">
                            </div>
                        </div>
                        <div class="row justify-content-center pb-4">
                            <div class="col-md-12 pb-1">
                                <label for="Información del sitio">Información del sitio</label>
                                <textarea class="form-control" id="informaciondelsitio" name="informaciondelsitio" rows="5"><?php echo $informaciondelsitio ?></textarea>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-12 mt-1">
                                <label>Observación</label>
                                <textarea class="form-control" id="observacion" name="observacion" rows="6"><?php echo $observacion ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <label for="Alarmas">Alarmas</label>
                        <textarea class="form-control" id="alarmas" name="alarmas" rows="10"></textarea>
                    </div>
                </div>
                <div class="modal fade" id="ModalTxt" name="ModalTxt" tabindex="-1" role="dialog" aria-labelledby="ModalTxtLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <label for="textoplano">Summary/detalles</label>
                                <textarea class="form-control" id="textoplano" name="textoplano" rows="2" style="white-space: nowrap;"></textarea>
                                <label class="mt-2" for="textoplano1">Nota</label>
                                <textarea class="form-control" id="textoplano1" name="textoplano1" rows="20" style="white-space: nowrap;"></textarea>
                                <br>
                                <label for="ruta">Ruta de clasificación sugerida:</label>
                                <br>
                                <label id="rutaclasificacion" name="rutaclasificacion"></label>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info" id="download" value="Download">Descargar</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <textarea type="hidden" id="textoplanohidden" name="textoplanohidden" style="display:none;"></textarea>
                <textarea type="hidden" id="textoplano2hidden" name="textoplano2hidden" style="display:none;"></textarea>

                <div class="modal fade" id="aviso" name="aviso" tabindex="-1" role="dialog" aria-labelledby="avisotitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header" style="display:block; padding: 0.4rem 0.4rem ">
                                <h5 class="modal-title" id="aviso-titulo">Aviso</h5>
                            </div>
                            <div class="modal-body text-left" id="aviso-body">

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" id="update" name="update" class="btn btn-dark">Actualizar</button>
                <button type="button" onclick="descargartexto()" id="download2" name="download2" class="btn btn btn-info">Descargar</button>
                <button type="button" onclick="showmodal()" id="resumenmodal" name="resumenmodal" class="btn btn-outline-secondary">Ver en pantalla</button>
                <button type="reset" id="limpiar_busqueda" name="limpiar_busqueda" class="btn btn-outline-secondary">Limpiar</button>

                <hr class="my-5">
            </form>
            <?php if($permisotablas['estado'] == 'on'){ ?>
                <form id="form2" name="form2" action="" method="post">
                    <input type="hidden" id="nombrehidden2" name="nombrehidden2" value="<?php echo $nombre; ?>">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">GSM</a>
                            <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">UMTS</a>
                            <a class="nav-link" id="nav-contact-tab;javascript:adjustltetable();" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false" >LTE</a>
                        </div>
                    </nav>
                    
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <button type="submit" id="fill_gsm" name="fill_gsm" class="btn btn-dark btn-sm mt-3">Buscar</button>
                            <div>
                                <table id="tablagestoresgsm" class="display compact" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>MSC NAME</th>
                                            <th>BSC NAME</th>
                                            <th>BCF NAME</th>
                                            <th>BTS NAME</th>
                                            <th>BTS NO</th>
                                            <th>BCF NO</th>
                                            <th>CELL ID</th>
                                            <th>CL</th>
                                            <th>ESTADO</th>
                                            <th>FECHA ESTADO</th>
                                            <th>DEPARTAMENTO</th>
                                            <th>COD MUNICIPIO</th>
                                            <th>MUNICIPIO</th>
                                            <th>LOCALIDAD</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (isset($tablagsm)) {
                                            for ($i = 0; $i < sizeof($tablagsm); $i++) {
                                        ?>
                                                <tr>
                                                    <td><?php if (isset($tablagsm[$i][1])) {
                                                            echo $tablagsm[$i][1];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][2])) {
                                                            echo $tablagsm[$i][2];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][3])) {
                                                            echo $tablagsm[$i][3];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][4])) {
                                                            echo $tablagsm[$i][4];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][5])) {
                                                            echo $tablagsm[$i][5];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][6])) {
                                                            echo $tablagsm[$i][6];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][7])) {
                                                            echo $tablagsm[$i][7];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][8])) {
                                                            echo $tablagsm[$i][8];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][9])) {
                                                            echo $tablagsm[$i][9];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][10])) {
                                                            echo $tablagsm[$i][10];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][11])) {
                                                            echo $tablagsm[$i][11];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][12])) {
                                                            echo $tablagsm[$i][12];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][13])) {
                                                            echo $tablagsm[$i][13];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablagsm[$i][14])) {
                                                            echo $tablagsm[$i][14];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <button type="submit" id="fill_umts" name="fill_umts" class="btn btn-dark btn-sm mt-3">Buscar</button>
                            <table id="tablagestoresumts" class="display compact" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>RNC NAME</th>
                                        <th>WBTS NAME</th>
                                        <th>WCELL NAME</th>
                                        <th>WBTS NO</th>
                                        <th>WCELL NO</th>
                                        <th>GID WBTS</th>
                                        <th>CL</th>
                                        <th>IP ADDRESS</th>
                                        <th>ESTADO</th>
                                        <th>FECHA ESTADO</th>
                                        <th>DEPARTAMENTO</th>
                                        <th>COD MUNICIPIO</th>
                                        <th>MUNICIPIO</th>
                                        <th>LOCALIDAD</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($tablaumts)) {
                                        for ($i = 0; $i < sizeof($tablaumts); $i++) {
                                    ?>
                                            <tr>
                                                <td><?php if (isset($tablaumts[$i][1])) {
                                                        echo $tablaumts[$i][1];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][2])) {
                                                        echo $tablaumts[$i][2];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][3])) {
                                                        echo $tablaumts[$i][3];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][4])) {
                                                        echo $tablaumts[$i][4];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][5])) {
                                                        echo $tablaumts[$i][5];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][6])) {
                                                        echo $tablaumts[$i][6];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][7])) {
                                                        echo $tablaumts[$i][7];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][8])) {
                                                        echo $tablaumts[$i][8];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][9])) {
                                                        echo $tablaumts[$i][9];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][10])) {
                                                        echo $tablaumts[$i][10];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][11])) {
                                                        echo $tablaumts[$i][11];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][12])) {
                                                        echo $tablaumts[$i][12];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][13])) {
                                                        echo $tablaumts[$i][13];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                                <td><?php if (isset($tablaumts[$i][14])) {
                                                        echo $tablaumts[$i][14];
                                                    } else {
                                                        echo "-";
                                                    }; ?></td>
                                            </tr>
                                    <?php
                                        }
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <button type="submit" id="fill_lte" name="fill_lte" class="btn btn-dark btn-sm mt-3">Buscar</button>
                            <div id="divtablalte" name="divtablalte">
                                <table id="tablagestoreslte" class="display compact" style="width: 100%;" >
                                    <thead>
                                        <tr>
                                            <th>LNBTS NAME</th>
                                            <th>LNCEL NAME</th>
                                            <th>LNCEL NO</th>
                                            <th>LNBTS NO</th>
                                            <th>CL</th>
                                            <th>DN</th>
                                            <th>IP CONTROL PLANE</th>
                                            <th>IP USER PLANE</th>
                                            <th>IP SINCRONISMO</th>
                                            <th>ESTADO</th>
                                            <th>FECHA ESTADO</th>
                                            <th>DEPARTAMENTO</th>
                                            <th>COD MUNICIPIO</th>
                                            <th>MUNICIPIO</th>
                                            <th>LOCALIDAD</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (isset($tablalte)) {
                                            for ($i = 0; $i < sizeof($tablalte); $i++) {
                                        ?>
                                                <tr>
                                                    <td><?php if (isset($tablalte[$i][1])) {
                                                            echo $tablalte[$i][1];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][2])) {
                                                            echo $tablalte[$i][2];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][3])) {
                                                            echo $tablalte[$i][3];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][4])) {
                                                            echo $tablalte[$i][4];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][5])) {
                                                            echo $tablalte[$i][5];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][6])) {
                                                            echo $tablalte[$i][6];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][7])) {
                                                            echo $tablalte[$i][7];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][8])) {
                                                            echo $tablalte[$i][8];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][9])) {
                                                            echo $tablalte[$i][9];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][10])) {
                                                            echo $tablalte[$i][10];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][11])) {
                                                            echo $tablalte[$i][11];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][12])) {
                                                            echo $tablalte[$i][12];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][13])) {
                                                            echo $tablalte[$i][13];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][14])) {
                                                            echo $tablalte[$i][14];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                    <td><?php if (isset($tablalte[$i][15])) {
                                                            echo $tablalte[$i][15];
                                                        } else {
                                                            echo "-";
                                                        }; ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function() {
                            $('#tablagestoresgsm').DataTable();
                            $('#tablagestoresumts').DataTable();
                            $('#tablagestoreslte').DataTable();
                        });
                    </script>

                    <script>
                        $('#tablagestoresgsm').DataTable({
                            language: {
                                emptyTable: "Por favor realice su búsqueda"
                            },
                            "searching": false,
                            "ordering": false,
                            "info": false,
                            "paging": false,
                            "scrollX": true,
                            scrollCollapse: true,
                            scrollY: "350px",

                        });
                        $('#tablagestoresumts').DataTable({
                            language: {
                                emptyTable: "Por favor realice su búsqueda"
                            },
                            "searching": false,
                            "ordering": false,
                            "info": false,
                            "paging": false,
                            "scrollX": true,
                            scrollCollapse: true,
                            scrollY: "350px",

                        });
                        $('#tablagestoreslte').DataTable({
                            language: {
                                emptyTable: "Por favor realice su búsqueda"
                            },
                            "searching": false,
                            "ordering": false,
                            "info": false,
                            "paging": false,
                            "scrollX": true,
                            scrollCollapse: true,
                            scrollY: "350px",
                            "autoWidth": true,

                        });
                    </script>
                    <script>
                    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                        var tablelte = $('#tablagestoreslte').DataTable();
                        var tableumts = $('#tablagestoresumts').DataTable();
                        var tablegsm = $('#tablagestoresgsm').DataTable();
                        
                        tablelte.columns.adjust().draw();
                        tableumts.columns.adjust().draw();
                        tablegsm.columns.adjust().draw();
                    });
                        function adjustltetable() {
                            
                        }
                    </script>
                </form>
            <?php }?>
            <script type="text/javascript" src="js/scripts_busqueda.js"></script>
        </div>
    </div>
    <?php include_once('footer.php'); ?>
</div>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#buscartxt").keyup(function() {
            var searchtext = $(this).val();
            if (searchtext != '') {
                $.ajax({
                    url: 'proceso',
                    method: 'post',
                    data: {
                        query: searchtext
                    },
                    success: function(response) {
                        $("#show-list").html(response);
                    }
                });
            } else {
                $("show-list").html('');
            }
        });
        $(document).on('click', 'a', function() {
            $("#buscartxt").val($(this).text());
            $("#show-list").html('');
        });
    });
</script>
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
        // $('#labelplancorp').tooltip('toggle');
        // setTimeout(function () {
        //     $('#labelplancorp').tooltip('hide');
        // }, 1000);
    })
</script>
<script>
    $(function() {
        $('#cnocvip').bootstrapToggle('<?php echo $cnocvip ?>');
        $('#ecp').bootstrapToggle('<?php echo $ecp ?>');
        $('#CRC').bootstrapToggle('<?php echo $CRC ?>');
        $('#bloqueo').bootstrapToggle('<?php echo $bloqueo ?>');
        $('#vip').bootstrapToggle('<?php $vip = trim($vip);
                                    echo $vip ?>');
        $('#usms').bootstrapToggle('<?php echo $usms ?>');
        $('#netnumen').bootstrapToggle('<?php echo $netnumen ?>');
        $('#plancorp').bootstrapToggle('<?php echo $plancorp ?>');
    })
</script>
<script>
    function valoresbotonesdb() {
        $('#cnocvip').bootstrapToggle('<?php echo $cnocvip ?>');
        $('#ecp').bootstrapToggle('<?php echo $ecp ?>');
        $('#CRC').bootstrapToggle('<?php echo $CRC ?>');
        $('#bloqueo').bootstrapToggle('<?php echo $bloqueo ?>');
        $('#vip').bootstrapToggle('<?php $vip = trim($vip);
                                    echo $vip ?>');
        $('#usms').bootstrapToggle('<?php echo $usms ?>');
        $('#netnumen').bootstrapToggle('<?php echo $netnumen ?>');
        $('#plancorp').bootstrapToggle('<?php echo $plancorp ?>');
    }
</script>


<script>
    switch ('<?php echo $criticidad; ?>') {
        case 'CCM':
            document.body.style.background = "#ff4747";
            selectcrit.style.color = "#ffffff"
            document.getElementById('nav-registrar').style.color = "#ffffff";
            document.getElementById('nav-busqueda').style.color = "#ffffff";
            document.getElementById('nav-tareas').style.color = "#ffffff";
            document.getElementById('nav-consulta').style.color = "#ffffff";
            document.getElementById('nav-indicadores').style.color = "#ffffff";
            document.getElementById('nav-plantillas').style.color = "#ffffff";
            document.getElementById('nav-ipran').style.color = "#ffffff";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "#ffffff";
            }
            document.getElementById('nav-alarmas').style.color = "#ffffff";
            document.getElementById('cerrarsesion').style.color = "#ffffff";
            selectcrit.style.background = "#ff4747";
            break;
        case 'CRITICA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.background = "#ff4747";
            selectcrit.style.color = "#ffffff";
            break;
        case 'ALTA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "#ffffff";
            selectcrit.style.background = "#ff6400 ";
            break;
        case 'MEDIA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "#ffc300";
            break;
        case 'BAJA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "";
            break;
        case 'N/A':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "";
            break;
    }
</script>


<?php
date_default_timezone_set('America/Bogota');
$date = date('H:i:s', time());
if (strtotime($date) < strtotime('14:00:00') && strtotime($date) >= strtotime('06:00:00')) {
    $turno = "T1";
} elseif (strtotime($date) < strtotime('22:00:00') && strtotime($date) >= strtotime('14:00:00')) {
    $turno = "T2";
} elseif (strtotime($date) >= strtotime('22:00:00') || (strtotime($date) >= strtotime('00:00:00') && strtotime($date) < strtotime('06:00:00'))) {
    $turno = "T3";
}
?>

<script>
    function textoplano() {
        var GSM = "";
        var UTMS = "";
        var LTE = "";

        var id = document.getElementById('id').value;
        var nombre = document.getElementById('nombre').value;
        var areaselect = document.getElementById("area").value;

        if (document.getElementById('GSM').checked == true) {
            GSM = "SI";
        } else {
            GSM = "NO";
        }
        if (document.getElementById('UMTS').checked == true) {
            UMTS = "SI";
        } else {
            UMTS = "NO";
        }
        if (document.getElementById('LTE').checked == true) {
            LTE = "SI";
        } else {
            LTE = "NO";
        }

        var diagnostico = document.getElementById('diagnostico').value;
        var lugar = document.getElementById('lugar').value;
        var planta = document.getElementById('planta').value;
        var impacto = document.getElementById('impacto').value;
        if (impacto == "") {
            var impactoinfo = "";
        } else {
            var impactoinfo = " (" + impacto + ")";
        }

        var satpost = document.getElementById('sat-poste').value;
        if (satpost == "NO") {
            satpost = "";
        } else if (satpost == "SATELITAL") {
            satpost = " (SATELITAL)";
        } else if (satpost == "POSTE") {
            satpost = " (POSTE)";
        } else if (satpost == "TMX") {
            satpost = " (TMX)"
        }

        if (planta == "SIN PLANTA") {
            var plantainfo = "SIN PE.";
        } else if (planta == "SI") {
            var plantainfo = "CON PE";
        } else {
            var plantainfo = "CON PE " + planta;
        }

        var casosdeuso = document.getElementById('casosdeuso').value;

        var sintrafico = document.getElementById('sintrafico').value;

        var fueradeserv = "";

        var gm = "";

        if (GSM == "NO" && UMTS == "NO" && LTE == "NO") {
            var fallas = "";
        } else {
            if (GSM == "SI") {
                var fallas = " GSM";
                if (UMTS == "SI") {
                    var fallas = " GSM/UMTS";
                    if (LTE == "SI") {
                        var fallas = " GSM/UMTS/LTE";
                    }
                }
                if (LTE == "SI" && UMTS == "NO") {
                    var fallas = " GSM/LTE";
                }
            }
            if (UMTS == "SI" && GSM == "NO") {
                var fallas = " UMTS";
                if (LTE == "SI") {
                    var fallas = " UMTS/LTE";
                }
            }
            if (LTE == "SI" && GSM == "NO" && UMTS == "NO") {
                var fallas = " LTE";
            }
        }

        if (sintrafico != "" && areaselect == "FAOB" || areaselect == "FAOC") {
            if (sintrafico == "VOZ" || sintrafico == "DATOS" || sintrafico == "VOZ-DATOS") {
                if (document.getElementById("sectores").value == "") {
                    var fallasinfo = "Sin tráfico de " + sintrafico;
                } else {
                    var fallasinfo = "Sin tráfico de " + sintrafico + " Sectores " + document.getElementById("sectores").value;;
                }
            } else if (sintrafico == "PERDGESTION") {
                if (document.getElementById("sectores").value == "") {
                    var fallasinfo = "Pérdida de gestión en" + fallas;
                } else {
                    var fallasinfo = "Pérdida de gestión en" + fallas + " Sectores " + document.getElementById("sectores").value;;
                }
            } else if (sintrafico == "FUERADES") {
                if (document.getElementById("sectores").value == "") {
                    if (areaselect == "FEE") {
                        var fallasinfo = "Fuera de servicio";
                        var fueradeserv = " con alarmas de energía";
                    } else {
                        var fallasinfo = "Fuera de servicio";
                        var fueradeserv = "";
                    }
                } else {
                    if (areaselect == "FEE") {
                        var fallasinfo = "Fuera de servicio Sectores " + document.getElementById("sectores").value;
                        var fueradeserv = " con alarmas de energía";
                    } else {
                        var fallasinfo = "Fuera de servicio Sectores " + document.getElementById("sectores").value;
                        var fueradeserv = "";
                    }
                }
            }
        } else if (GSM == "NO" && UMTS == "NO" && LTE == "NO" && casosdeuso == "NO ESPECIFICADO") {
            var fallasinfo = "Alarmas de energía";
        } else if (GSM == "NO" && UMTS == "NO" && LTE == "NO" && casosdeuso != "NO ESPECIFICADO") {
            if (casosdeuso == "ALTATEMP") {
                var fallasinfo = "Alarma de alta temperatura";
            } else if (casosdeuso == "PLANTAEN") {
                var fallasinfo = "Alarma de planta encendida";
            } else if (casosdeuso == "NO ESPECIFICADO") {
                var fallasinfo = "Alarmas de energía";
            } else if (casosdeuso == "FUERADES") {
                if (document.getElementById("sectores").value == "") {
                    if (areaselect == "FEE") {
                        var fallasinfo = "Fuera de servicio";
                        var fueradeserv = " con alarmas de energía";
                    } else {
                        var fallasinfo = "Fuera de servicio";
                        var fueradeserv = "";
                    }
                } else {
                    if (areaselect == "FEE") {
                        var fallasinfo = "Fuera de servicio Sectores " + document.getElementById("sectores").value;
                        var fueradeserv = " con alarmas de energía";
                    } else {
                        var fallasinfo = "Fuera de servicio Sectores " + document.getElementById("sectores").value;
                        var fueradeserv = "";
                    }
                }
            } else if (casosdeuso == "FALLAALCATEL") {
                var fallasinfo = "Alarma de " + document.getElementById('alarmacaso').value + " en equipo ALCATEL " + document.getElementById('equipocaso').value;
            } else if (casosdeuso == "FALLASDH") {
                var fallasinfo = "Alarma de " + document.getElementById('alarmacaso').value + " en equipo SDH " + document.getElementById('equipocaso').value;
            } else if (casosdeuso == "POWERBATERIA") {
                if (document.getElementById('checkbv').checked == true) {
                    var fallasinfo = "Alarma de power en baterias y bajo voltaje de baterías";
                } else {
                    var fallasinfo = "Alarma de power en baterias";
                }
            } else if (casosdeuso == "BAJOVOLTAJEB") {
                var fallasinfo = "Alarma de bajo voltaje de baterías";
            } else if (casosdeuso == "GM") {
                var fallasinfo = "Fuera de servicio GM";
                var gm = " GM";
            } else if (casosdeuso = "BLPORENERGIA") {
                if ($('#automanual').val() == "auto") {
                    var fallasinfo = "Bloqueo automático por alarmas de energía";
                } else if ($('#automanual').val() == "manual") {
                    var fallasinfo = "Bloqueo manual por alarmas de energía";
                }
            }
        } else {
            if (document.getElementById("sectores").value == "") {
                if (areaselect == "FEE") {
                    var fallasinfo = "Fuera de servicio";
                    var fueradeserv = " con alarmas de energía";
                } else {
                    var fallasinfo = "Fuera de servicio";
                    var fueradeserv = "";
                }
            } else {
                if (areaselect == "FEE") {
                    var fallasinfo = "Fuera de servicio Sectores " + document.getElementById("sectores").value;
                    var fueradeserv = " con alarmas de energía";
                } else {
                    var fallasinfo = "Fuera de servicio Sectores " + document.getElementById("sectores").value;
                    var fueradeserv = "";
                }
            }
        }


        if (document.getElementById('CRC').checked == true) {
            var CRC = " (CRC)";
        } else {
            var CRC = "";
        }
        if (document.getElementById('cnocvip').checked == true) {
            var CNOCVIP = " (CNOCVIP)";
        } else {
            var CNOCVIP = "";
        }

        if (document.getElementById('ecp').checked == true) {
            var ECP = " (ECP)";
        } else {
            var ECP = "";
        }

        if (document.getElementById('tx').checked == true) {
            var tx = " (TX)";
        } else {
            var tx = "";
        }

        if (document.getElementById('reinicio').checked == true) {
            var reinicio = " (reinicio)";
        } else {
            var reinicio = "";
        }

        if (document.getElementById('intermitencia').checked == true) {
            var intermitencia = " (intermitencia)";
        } else {
            var intermitencia = "";
        }

        if (document.getElementById('cod').checked == true) {
            var cod = " (COD)";
        } else {
            var cod = "";
        }

        if (document.getElementById('rst_recu').checked == true) {
            var rst_recu = " (reinicio recurrente)";
        } else {
            var rst_recu = "";
        }

        if (document.getElementById('100municipio').checked == true) {
            if (GSM == "SI") {
                var municipio100 = " (M100% V)";
                if (UMTS == "SI") {
                    var municipio100 = " (M100% VD)";
                    if (LTE == "SI") {
                        var municipio100 = " (M100% VD)";
                    }
                }
                if (LTE == "SI" && UMTS == "NO") {
                    var municipio100 = " (M100% VD)";
                }
            }
            if (UMTS == "SI" && GSM == "NO") {
                var municipio100 = " (M100% D)";
                if (LTE == "SI") {
                    var municipio100 = " (M100% D)";
                }
            }
            if (LTE == "SI" && GSM == "NO" && UMTS == "NO") {
                var municipio100 = " (M100% D)";
            }

        } else {
            var municipio100 = "";
        }

        if (document.getElementById('pnmsj').checked == true) {
            var pnmsj = " (PNMSJ)";
        } else {
            var pnmsj = "";
        }

        var ticketFSP = document.getElementById('ticketFSP').value;
        if (ticketFSP == "") {
            var ticketFSPinfo = "";
            var ticketFSPres = "";
        } else {
            var ticketFSPinfo = " (FSP " + ticketFSP + ") ";
            var ticketFSPres = "FSP: " + ticketFSP;
        }

        var idtrabajo = document.getElementById('idtrabajo').value;
        if (idtrabajo == "") {
            var idtrabajoinfo = "";
        } else {
            var idtrabajoinfo = "Trabajo no exitoso ID:" + idtrabajo;
        }

        if (document.getElementById('vip').checked == true) {
            var vip = " (VIP)";
        } else {
            var vip = "";
        }

        if (document.getElementById('validacionderadios').checked == true) {
            var validacionderadios = " EX";
        } else {
            var validacionderadios = "";
        }


        if (document.getElementById('turnot11').checked == true) {
            turno = "T11";
        } else {
            var turno = "<?php echo $turno; ?>"
        }


        var user = document.getElementById('user').value
        var typeu = document.getElementById('typeu').value
        var informaciondelsitio = document.getElementById('informaciondelsitio').value;
        var alarmas = document.getElementById('alarmas').value;
        var observacion = document.getElementById('observacion').value;
        var nie = document.getElementById('nie').value;
        var numero = document.getElementById('numero').value;

        if (document.getElementById('vng').checked == true) {
            var vng = "(vng)\n\n";
        } else {
            var vng = "";
        }



        var textoplano = areaselect + ":MPACC_" + turno + ": " + fallasinfo + " EB " + nombre + satpost + ((sintrafico == "PERDGESTION") ? "" : fallas) + " en " + lugar + gm + fueradeserv + " EB " + plantainfo + reinicio + intermitencia + CRC +
            CNOCVIP + ECP + tx + pnmsj + cod + rst_recu + municipio100 + impactoinfo + ticketFSPinfo + idtrabajoinfo + vip + validacionderadios;

        var textoplano1 = user + typeu + ":" + areaselect + ":MPACC_" + turno + ": " + fallasinfo + " EB " + nombre + satpost + ((sintrafico == "PERDGESTION") ? "" : fallas) + " en " + lugar + gm + fueradeserv + " EB " + plantainfo + reinicio + CRC +
            tx + pnmsj + cod + rst_recu + municipio100 + impactoinfo + ticketFSPinfo + idtrabajoinfo + vip + validacionderadios + "\n";

        textoplano1 = textoplano1 + "Diagnóstico:" + "\n" + diagnostico + "\n" + vng + "Afectación GSM: " + GSM + "\nAfectación UMTS " + UMTS + "\nAfectación LTE: " + LTE + "\n";

        textoplano1 = textoplano1 + idtrabajoinfo + "\n" + ticketFSPres + "\n" + "NIE: " + nie + "\n" + "Numero: " + numero +
            "\n\nInformación del sitio: \n" + informaciondelsitio + "\n\nAlarmas:\n" + alarmas;

        textoplano1 = textoplano1 + "\n\n" + "SIGLA SIGNIFICADO\nFSP Falla parcialmente superada\nFRP Falla regulatoria progresiva\nAID Alto impacto de disponibilidad\nMID Alto impacto de disponibilidad\nCRC Comision regulatoria de comunicaciones\nPPC Posible punto comun\nEE  Empresa de energía\nBOE Back Office Energia\nBOTX Back Office Transmision\nFHG  Falla herramienta de gestion"

        document.getElementById("textoplano").value = textoplano;
        document.getElementById("textoplano1").value = textoplano1;
        document.getElementById("textoplanohidden").value = textoplano;
        document.getElementById("textoplano2hidden").value = textoplano1;
    }
</script>

<script>
    <?php
    if (isset($mensaje_res)) {
    ?>
        var mensaje_p = "<?php echo $mensaje_res['MENSAJE']; ?>";
        var mensaje = "<p class='text-justify'>" + mensaje_p + "</p>"
        document.getElementById("aviso-body").innerHTML = mensaje;
        $('#aviso').modal('show');
    <?php
    }
    ?>
</script>