<?php include('index.php') ?>


<div class="card text-center border-light mb-3">

    <input type="hidden" id="areaselect" name="areaselect">
    <div class="card-body">
        <div class="container">

            <br>
            <form action="proceso.php" method="post">

                <div class="row justify-content-center">
                    <div class="form-group col-md-4">
                        <label for="inputnombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputlugar">Lugar</label>
                        <input type="text" class="form-control" id="lugar" name="lugar">
                    </div>
                    <div class="form-group col-md-1" style="margin-left: 30px;">
                        <label for="inputcnocvip">CNOCVIP</label>
                        <br>
                        <input type="checkbox" id="cnocvip" name="cnocvip" data-toggle="toggle" data-onstyle="info" data-on="On" data-off="Off" data-style="fast">
                    </div>
                    <div class="form-group col-md-1" style="margin-left: -10px;">
                        <label for="inputecp">ECP</label>
                        <br>
                        <input type="checkbox" id="ecp" name="ecp" data-toggle="toggle" data-onstyle="info" data-on="On" data-off="Off" data-style="fast">
                    </div>
                    <div class="form-group" style="margin-left: 30px;">
                        <label for="inputnombre">CRC</label>
                        <br>
                        <input type="checkbox" id="CRC" name="CRC" data-toggle="toggle" data-onstyle="warning" data-on="On" data-off="Off" data-style="fast">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="form-group col-md-2">
                        <label for="inputplanta">Planta</label>
                        <select class="form-control" id="planta" name="planta">
                            <option value="" selected disabled hidden>Seleccione</option>
                            <option value="SIN PLANTA">SIN PLANTA</option>
                            <option value="SI">SI</option>
                            <option value="7X24">7X24</option>
                            <option value="7X12">7X12</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputcriticidad">Criticidad</label>
                        <select class="form-control" id="criticidad" name="criticidad">
                            <option value="" selected disabled hidden>Seleccione</option>
                            <option value="CCM">CCM</option>
                            <option value="CRITICA">CRITICA</option>
                            <option value="ALTA">ALTA</option>
                            <option value="MEDIA">MEDIA</option>
                            <option value="BAJA">BAJA</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputimpacto">Impacto</label>
                        <select class="form-control" id="impacto" name="impacto">
                            <option value="" selected disabled hidden>Seleccione</option>
                            <option value="NO">NO</option>
                            <option value="MID">MID</option>
                            <option value="AID">AID</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputregional">Regional</label>
                        <input type="text" class="form-control" id="regional" name="regional">
                    </div>
                    <div class="form-group col-md-1">
                        <label for="inputbloqueo">Bloqueo</label>
                        <br>
                        <input type="checkbox" id="bloqueo" name="bloqueo" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                    </div>
                </div>
                <div class="row justify-content-center" id="divelecnum">
                    <div class="form-group col-md-3">
                        <label for="inputelectri">Electrificadora</label>
                        <input type="text" class="form-control" id="electrificadora" name="electrificadora">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputNIE">NIE</label>
                        <input type="text" class="form-control" id="nie" name="nie">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputnumero">Número</label>
                        <input type="text" class="form-control" id="numero" name="numero">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="form-group col-md-auto" style="margin-left: 30px; width:auto ">
                        <label for="inputsatelital">SAT/POSTE</label>
                        <select class="form-control" id="sat-poste" name="sat-poste">
                            <option value="NO" selected>NO</option>
                            <option value="SATELITAL">SATELITAL</option>
                            <option value="POSTE">POSTE</option>
                            <option value="RPT">RPT</option>
                        </select>
                    </div>
                    <div class="form-group col-auto offset-md-1">
                        <label for="inputplancorp">PLANCORP</label>
                        <br>
                        <input type="checkbox" id="plancorp" name="plancorp" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                    </div>
                    <div class="form-group col-auto">
                        <label for="inputvip">VIP</label>
                        <br>
                        <input type="checkbox" id="vip" name="vip" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                    </div>
                    <div class="form-group col-auto">
                        <label for="inputUSMS">USMS</label>
                        <br>
                        <input type="checkbox" id="usms" name="usms" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                    </div>
                    <div class="form-group col-auto">
                        <label for="inputNETNUMEN">NETNUMEN</label>
                        <br>
                        <input type="checkbox" id="netnumen" name="netnumen" data-toggle="toggle" data-onstyle="danger" data-on="Si" data-off="No" data-style="fast">
                    </div>
                </div>
                <br>
                <div class="row justify-content-center">
                    <label>Información del sitio</label>
                    <textarea class="form-control" id="informaciondelsitio" name="informaciondelsitio" rows="2"></textarea>
                </div>
                <div class="row justify-content-center">
                    <label>Observación</label>
                    <textarea class="form-control" id="observacion" name="observacion" rows="2"></textarea>
                </div>
                <br>
                <button type="submit" id="save" name="save" class="btn btn-primary">Registrar</button>
                <button type="submit" id="limpiar_registro" name="limpiar_registro" class="btn btn-light">Limpiar</button>
            </form>
        </div>
    </div>
    <?php include_once('footer.php');?>
</div>
<script>
    var selectcrit = document.getElementById('criticidad');
function logValue() {
    var seleccioncrit = selectcrit.value;
    switch (this.value) {
        case 'CCM':
            document.body.style.background = "#ff4747";
            selectcrit.style.color = "#ffffff"
            document.getElementById('nav-registrar').style.color = "#ffffff";
            document.getElementById('nav-busqueda').style.color = "#ffffff";
            document.getElementById('nav-tareas').style.color = "#ffffff";
            document.getElementById('nav-indicadores').style.color = "#ffffff";
            document.getElementById('nav-consulta').style.color = "#ffffff";
            document.getElementById('nav-plantillas').style.color = "#ffffff";
            document.getElementById('nav-ipran').style.color = "#ffffff";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "#ffffff";
            }
            document.getElementById('nav-alarmas').style.color = "#ffffff";
            document.getElementById('cerrarsesion').style.color = "#ffffff";
            selectcrit.style.background = "#ff4747";
            break;
        case 'CRITICA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.background = "#ff4747";
            selectcrit.style.color = "#ffffff";
            break;
        case 'ALTA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "#ffffff";
            selectcrit.style.background = "#ff6400 ";
            break;
        case 'MEDIA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "#ffc300";
            break;
        case 'BAJA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "";
            break;
        case 'N/A':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "";
            break;
    }
}

selectcrit.addEventListener('change', logValue, false);
</script>
<script>
    var nombre = document.getElementById("nombre");
    var lugar = document.getElementById("lugar");

    function iniciales() {
        inicialesnom = nombre.value;
        inic = inicialesnom.slice(0, 3).toUpperCase();
        console.log(inic);
        switch (inic) {
            case 'AMZ':
                lugar.value = "Amazonas";
                break;
            case 'ANT':
                lugar.value = "Antioquia";
                break;
            case 'ARA':
                lugar.value = "Arauca";
                break;
            case 'ARM':
                lugar.value = "Armenia";
                break;
            case 'ATL':
                lugar.value = "Atlantico";
                break;
            case 'BAR':
                lugar.value = "Barranquilla";
                break;
            case 'BCA':
                lugar.value = "Barrancabermeja";
                break;
            case 'BGA':
                lugar.value = "Buga";
                break;
            case 'BNV':
                lugar.value = "Buenaventura";
                break;
            case 'BOG':
                lugar.value = "Bogota";
                break;
            case 'BOL':
                lugar.value = "Bolivar";
                break;
            case 'BOY':
                lugar.value = "Boyaca";
                break;
            case 'BUC':
                lugar.value = "Bucaramanga";
                break;
            case 'CAD':
                lugar.value = "Caldas";
                break;
            case 'CAL':
                lugar.value = "Cali";
                break;
            case 'CAQ':
                lugar.value = "Caqueta";
                break;
            case 'CAR':
                lugar.value = "Cartagena";
                break;
            case 'CAS':
                lugar.value = "Casanare";
                break;
            case 'CAU':
                lugar.value = "Cauca";
                break;
            case 'CES':
                lugar.value = "CESAR";
                break;
            case 'CHI':
                lugar.value = "Chia";
                break;
            case 'CHO':
                lugar.value = "Choco";
                break;
            case 'COR':
                lugar.value = "Cordoba";
                break;
            case 'CUC':
                lugar.value = "Cucuta";
                break;
            case 'CUN':
                lugar.value = "Cundinamarca";
                break;
            case 'DUI':
                lugar.value = "Duitama";
                break;
            case 'FAC':
                lugar.value = "Facatativa";
                break;
            case 'FLO':
                lugar.value = "Florencia";
                break;
            case 'FUS':
                lugar.value = "Fusagasuga";
                break;
            case 'GIR':
                lugar.value = "Girardot";
                break;
            case 'GUA':
                lugar.value = "Guainia";
                break;
            case 'GUJ':
                lugar.value = "Guajira";
                break;
            case 'GUV':
                lugar.value = "Guaviare";
                break;
            case 'HUI':
                lugar.value = "Huila";
                break;
            case 'IBG':
                lugar.value = "Ibague";
                break;
            case 'JAM':
                lugar.value = "Jamundi";
                break;
            case 'LET':
                lugar.value = "Leticia";
                break;
            case 'MAG':
                lugar.value = "Magdalena";
                break;
            case 'MAI':
                lugar.value = "Maicao";
                break;
            case 'MAN':
                lugar.value = "Manizales";
                break;
            case 'MED':
                lugar.value = "Medellin";
                break;
            case 'MET':
                lugar.value = "Meta";
                break;
            case 'MOC':
                lugar.value = "Mocoa";
                break;
            case 'MON':
                lugar.value = "Monteria";
                break;
            case 'NAR':
                lugar.value = "Nariño";
                break;
            case 'NEI':
                lugar.value = "Neiva";
                break;
            case 'NOR':
                lugar.value = "Norte de Santander";
                break;
            case 'PAL':
                lugar.value = "Palmira";
                break;
            case 'PAS':
                lugar.value = "Pasto";
                break;
            case 'PER':
                lugar.value = "Pereira";
                break;
            case 'POP':
                lugar.value = "Popayan";
                break;
            case 'PUT':
                lugar.value = "Putumayo";
                break;
            case 'QUB':
                lugar.value = "Quibdo";
                break;
            case 'QUI':
                lugar.value = "Quindio";
                break;
            case 'RIO':
                lugar.value = "Rioacha";
                break;
            case 'RIS':
                lugar.value = "Risaralda";
                break;
            case 'SAN':
                lugar.value = "San Andres";
                break;
            case 'SIN':
                lugar.value = "Sincelejo";
                break;
            case 'SJG':
                lugar.value = "San Jose del Guaviare";
                break;
            case 'SND':
                lugar.value = "Santander";
                break;
            case 'SOA':
                lugar.value = "Soacha";
                break;
            case 'SOG':
                lugar.value = "Sogamoso";
                break;
            case 'STA':
                lugar.value = "Santa Marta";
                break;
            case 'SUC':
                lugar.value = "Sucre";
                break;
            case 'TOL':
                lugar.value = "Tolima";
                break;
            case 'TUL':
                lugar.value = "Tulua";
                break;
            case 'TUN':
                lugar.value = "Tunja";
                break;
            case 'UBA':
                lugar.value = "Ubate";
                break;
            case 'VAD':
                lugar.value = "Valledupar";
                break;
            case 'VAL':
                lugar.value = "Valle del Cauca";
                break;
            case 'VAU':
                lugar.value = "Vaupes";
                break;
            case 'VCH':
                lugar.value = "Vichada";
                break;
            case 'VCO':
                lugar.value = "Villavicencio";
                break;
            case 'YOP':
                lugar.value = "Yopal";
                break;
            case 'ZIP':
                lugar.value = "Zipaquira";
                break;
        }

    }
    //lugar.addEventListener('change', iniciales, false);
    //$('#nombre').on("input",iniciales,false);
    nombre.addEventListener("input", iniciales, false)
</script>