<?php

if (isset($_POST['recuperar'])) {
    // require_once('PHPMailer/PHPMailerAutoload.php');
    require_once('Mailer/src/Exception.php');
    require_once('Mailer/src/PHPMailer.php');
    require_once('Mailer/src/SMTP.php');

    // $mail = new PHPMailer();
    $mail = new PHPMailer\PHPMailer\PHPMailer(true);
    $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';
    $mail­->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = 'ns18.hostgator.co';
    $mail->Port = '465';
    $mail->isHTML();
    $mail->Username = 'ticketcreador@incoelectronica.com';
    $mail->Password = '*T@URm&uQ9V0';




    $selector = bin2hex(random_bytes(8));
    $token = random_bytes(32);

    $url = "https://incoelectronica.com/ticketcreador/restablecer-contrasenha.php?selector=" . $selector . "&validator=" . bin2hex($token);

    $expires = date("U") + 1800;

    require 'con_db.php';

    $useremail = $_POST["email"];

    $sql = "DELETE FROM recuperarcontra WHERE recuperar_email = ?";
    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        session_start();
        $_SESSION['message'] = "Error en base de datos";
        $_SESSION['msg_type'] = "danger";
        header("location: recuperarcontrasenha.php?error=mysqlerror");
        exit();
    } else {
        mysqli_stmt_bind_param($stmt, "s", $useremail);
        mysqli_stmt_execute($stmt);
    }

    $sql = "SELECT id FROM usuarios WHERE email = ?";
    $stmt = mysqli_stmt_init($con);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        session_start();
        $_SESSION['message'] = "Error en base de datos";
        $_SESSION['msg_type'] = "danger";
        header("location: recuperarcontrasenha.php?error=mysqlerror");
        exit();
    } else {
        mysqli_stmt_bind_param($stmt, "s", $useremail);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $resultcheck = mysqli_stmt_num_rows($stmt);
        if($resultcheck == 0){
            session_start();
            $_SESSION['message'] = "No existe usuario asociado a ese email, intente de nuevo";
            $_SESSION['msg_type'] = "danger";
            header("location: recuperarcontrasenha.php?error=emailsinusuario");
            exit();
        }
    }

    $sql = "INSERT INTO recuperarcontra (recuperar_email, recuperar_selector, recuperar_token, recuperar_expires) VALUES (?,?,?,?);";

    $stmt = mysqli_stmt_init($con);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        session_start();
        $_SESSION['message'] = "Error en base de datos";
        $_SESSION['msg_type'] = "danger";
        header("location: recuperarcontrasenha.php?error=mysqlerror");
        exit();
    } else {
        $hashedtoken = password_hash($token, PASSWORD_DEFAULT);
        mysqli_stmt_bind_param($stmt, "ssss", $useremail, $selector, $hashedtoken, $expires);
        mysqli_stmt_execute($stmt);
    }

    mysqli_stmt_close($stmt);
    mysqli_close($con);


    $mail->setFrom('no-reply@ticketcreador.com');

    // $to = $useremail;

    $mail->Subject = 'Restablecer contraseña para Ticketcreador';

    $mail->Body = '<!doctype html>
    <html lang="en-US">
    
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>Email Templates</title>
        <meta name="description" content="Email Templates.">
        <style type="text/css">
            a:hover {
                text-decoration: underline !important;
            }
        </style>
    </head>
    
    <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
        <!--100% body table-->
        <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: "Open Sans", sans-serif;">
            <tr>
                <td>
                    <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="height:30px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="height:20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                    <tr>
                                        <td style="height:5px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 35px;">
                                            <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:"Rubik",sans-serif;">Solicitud de cambio de contraseña</h1>
                                            <span style="display:inline-block; vertical-align:middle; margin:13px 0 16px; border-bottom:1px solid #cecece; width:100px;"></span>
                                            <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                Solicitud para restablacer su contraseña en Ticketcreador, el link para restablecer su contraseña está al final de este correo, si no ha realizado esta petición por favor ignore este correo.
                                            </p>
                                            <a href="' . $url . '" style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                                Password</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:40px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="height:30px;">&nbsp;</td>
                            </tr>
                    </table>
                </td>
                </tr>
        </table>
        <!--/100% body table-->
    </body>
    
    </html>';


    $mail->addAddress(strtolower(trim($useremail)));

    $mail->Send();
    session_start();
    
    $_SESSION['message'] = "Link enviado!";
    $_SESSION['msg_type'] = "success";
    header("location: recuperarcontrasenha.php?reset=success");
    exit();

    // if (!$mail -> Send()) {
    //     $_SESSION['message'] = "Error" . $mail­->ErrorInfo;
    //     $_SESSION['msg_type'] = "danger";
    //     header("location: recuperarcontrasenha.php?reset=error");
    // } else {
    //     $_SESSION['message'] = "Link enviado!";
    //     $_SESSION['msg_type'] = "success";
    //     header("location: recuperarcontrasenha.php?reset=success");
    //     exit();
    // }

    
    
} else {
    header("iniciarsesion.php");
}
