<?php
include('index.php');
include('includes/alarmasproceso.php');
?>

<?php
$alarmas_sql = $con->query("SELECT * FROM alarmas") or die(mysqli_error($con));
$alarmas_cod = $alarmas_sql->fetch_all();
$alarmasnom_sql = $con->query("SELECT * FROM alarmas") or die(mysqli_error($con));
$alarmas_nom = $alarmasnom_sql->fetch_all();
?>

<div class="card text-center border-light mb-3">
    <div class="card-body">
        <form action="" method="POST">
            <div class="container">
                <div class="row justify-content-center mb-4" style="background: #FFFFFF;">
                    <div class="col-md-4">
                        <div>
                            <label>Código de alarma:</label>
                            <br>
                            <select name="buscod" id="buscod" class="selectpicker" data-live-search="true">
                                <option value="" selected disabled hidden>Seleccione</option>
                                <?php
                                foreach ($alarmas_cod as $cod) { ?>
                                    <?php if ($cod[1] == "") {
                                        continue;
                                    } ?>
                                    <option value="<?php if ($cod[1] != "") {
                                                        echo $cod[0];
                                                    } ?>" data-subtext="<?php if ($cod[5] != "") {
                                                                            echo $cod[5];
                                                                        } ?>"><?php if ($cod[1] != "") {
                                                                                    echo $cod[1];
                                                                                } ?></option>
                                <?php } ?>
                            </select>
                            <button type="submit" id="bcod" name="bcod" class="btn btn btn-info btn-sm">Buscar</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div>
                            <label>Nombre de alarma:</label>
                            <br>
                            <select name="busnom" id="busnom" class="selectpicker" data-live-search="true">
                                <option value="" selected disabled hidden>Seleccione</option>
                                <?php
                                foreach ($alarmas_nom as $nom) { ?>
                                    <?php if ($nom[2] == "") {
                                        continue;
                                    } ?>
                                    <option value="<?php if ($nom[2] != "") {
                                                        echo $nom[0];
                                                    } ?>" data-subtext="<?php if ($nom[5] != "") {
                                                                            echo $nom[5];
                                                                        } ?>"><?php if ($nom[2] != "") {
                                                                                    echo $nom[2];
                                                                                } ?></option>
                                <?php } ?>
                            </select>
                            <button type="submit" id="bnom" name="bnom" class="btn btn btn-info btn-sm">Buscar</button>
                        </div>

                    </div>
                </div>
                <div class="row-12">
                    <div class="row justify-content-center">
                        <div class="col-md-3">
                            <label>Código alarma:</label>
                            <input type="text" class="form-control" id="codalarma" name="codalarma" value="<?php if (isset($alarma)) {
                                                                                                                echo $alarma['codigo'];
                                                                                                            } ?>">
                        </div>
                        <div class="col-md-9">
                            <label>Nombre alarma:</label>
                            <input type="text" class="form-control" id="nombrealarma" name="nombrealarma" value="<?php if (isset($alarma)) {
                                                                                                                        echo $alarma['nombre'];
                                                                                                                    } ?>">

                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <label>Gestor:</label>
                            <input type="text" class="form-control" id="gestoralarma" name="gestoralarma" value="<?php if (isset($alarma)) {
                                                                                                                        echo $alarma['gestor'];
                                                                                                                    } ?>">
                        </div>
                        <div class="col-md-8">
                            <label>Categoría:</label>
                            <input type="text" class="form-control" id="catalarma" name="catalarma" value="<?php if (isset($alarma)) {
                                                                                                                echo $alarma['categoria'];
                                                                                                            } ?>">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <label>Descripción:</label>
                        <textarea class="form-control" id="descalarma" name="descalarma" rows="5"><?php if (isset($alarma)) {
                                                                                                        echo $alarma['descripcion'];
                                                                                                    } ?></textarea>
                    </div>
                    <div class="row justify-content-center">
                        <label>Posible escalamiento:</label>
                        <textarea class="form-control" id="posibalarma" name="posibalarma" rows="8"><?php if (isset($alarma)) {
                                                                                                        echo $alarma['pescalamiento'];
                                                                                                    } ?></textarea>

                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <label>Prioridad:</label>
                            <input type="text" class="form-control" id="prioalarma" name="prioalarma" style="text-align: center;" value="<?php if (isset($alarma)) {
                                                                                                                    echo $alarma['prioridad'];
                                                                                                                } ?>">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<?php include_once('footer.php'); ?>