<?php
include('index.php');
?>
<div class="card text-center border-light mb-3">
    <div class="card-body">
        <div class="container">
            <form action="" method="POST" id="ipran_alcanzabilidad_form">
                <div class="row">
                    <div class="col-4">
                        <div class="row justify-content-center">
                            <label>Equipo A:</label>
                            <input type="text" id="equipoa" name="equipoa" class="form-control">
                        </div>
                        <div class="row justify-content-center">
                            <textarea name="equipoatext" id="equipoatext" rows="16" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row justify-content-center">
                            <label>Equipo B:</label>
                            <input type="text" id="equipob" name="equipob" class="form-control">
                        </div>
                        <div class="row justify-content-center">
                            <textarea name="equipobtext" id="equipobtext" rows="16" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row justify-content-center">
                            <label>Identificador:</label>
                            <input type="text" id="identificador" name="identificador" class="form-control">
                        </div>
                        <div class="row justify-content-center">
                            <label>Tipo de red:</label>
                            <input type="text" id="tipodered" name="tipodered" class="form-control">
                        </div>
                        <div class="row justify-content-center">
                            <label>Ext Contacto:</label>
                            <input type="text" id="extcontacto" name="extcontacto" class="form-control">
                        </div>
                        <div class="row justify-content-center">
                            <label>Observación</label>
                            <textarea name="equipobtext" id="equipobtext" rows="6" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <button type="button" id="nuevo_ipran" name="nuevo_ipran" class="btn btn btn-info">Nuevo</button>
                <button type="button" id="agregar" name="agregar" class="btn btn-primary">Agregar</button>
                <button type="button" id="generar_ipran" name="generar_ipran" class="btn btn-outline-secondary">Generar</button>
            </form>
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>