<?php
include('index.php');
include("con_db.php");
?>

<?php
$userlogged = $_SESSION['logged'];
$consulta = "SELECT * FROM tareas WHERE FIND_IN_SET('$userlogged', usuarios)";
$tareassql = mysqli_query($con, $consulta);

?>
<div class="card text-center border-light mb-3">
    <div class="card-body">
        <div class="container">
            <form action="proceso.php" method="post">
                <div class="form-row justify-content-center">
                    <div class="input-group" style="width:75%;">
                        <input class="form-control" id="nombre_tarea" name="nombre_tarea" type="text" placeholder="Nueva tarea">
                        <div class="input-group-append">
                            <button type="button" onclick="showmodalcreartarea()" class="btn btn-primary">Crear</button>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalcreartarea" name="modalcreartarea" tabindex="-1" role="dialog" aria-labelledby="modalcreartareaLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="titulomodaltarea" name="titulomodaltarea"></h5>
                                <input type="hidden" id="titulotarea" name="titulotarea">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row justify-content-center">
                                    <div class="col-auto">
                                        <label id="user">Usuarios:</label>
                                        <input type="hidden" id="selusers" name="selusers">
                                        <br>
                                        <select class="selectpicker" id="usuarios" name="usuarios" data-live-search="true" multiple>
                                            <?php
                                            $res = $con->query("SELECT usuario FROM usuarios ") or die(mysqli_error($con));
                                            while ($row = $res->fetch_assoc()) :
                                            ?>
                                                <option value="<?php echo $row['usuario']; ?>"><?php echo $row['usuario']; ?></option>
                                            <?php endwhile; ?>
                                        </select>
                                    </div>
                                    <div class="col-auto">
                                        <label for="recordatorio">Recordatorio</label>
                                        <input type="hidden" id="valtiempo" name="valtiempo">
                                        <select class="form-control" id="recordatorio" name="recordatorio" onchange="sqlreminder()">
                                            <option selected disabled hidden>Seleccione</option>
                                            <option> 30 minutos</option>
                                            <option> 1 hora</option>
                                            <option> 6 horas</option>
                                            <option> 8 horas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" id="crear_tarea" name="crear_tarea" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <form action="" method="post">
                <?php while ($rowtareas = $tareassql->fetch_assoc()) : ?>
                    <div class="modal fade" id="modaleditartarea<?php echo $rowtareas['id']; ?>" name="modaleditartarea<?php echo $rowtareas['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modaleditartarea<?php echo $rowtareas['id']; ?>Label" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="titulomodaltareaedit<?php echo $rowtareas['id']; ?>" name="titulomodaltareaedit" contentEditable="true"><?php echo $rowtareas['tarea']; ?></h5>
                                    <input type="hidden" id="titulotareaedit<?php echo $rowtareas['id']; ?>" name="titulotareaedit<?php echo $rowtareas['id']; ?>" value="<?php echo $rowtareas['tarea']; ?>">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <script>
                                    var tituloch = document.getElementById("titulomodaltareaedit<?php echo $rowtareas['id']; ?>");

                                    function cambiartitulo() {
                                        var valuetitulo = document.getElementById("titulomodaltareaedit<?php echo $rowtareas['id']; ?>").innerHTML;
                                        $("#titulotareaedit<?php echo $rowtareas['id']; ?>").val(valuetitulo);
                                    }
                                    tituloch.addEventListener('input', cambiartitulo, false);
                                </script>
                                <div class="modal-body">
                                    <div class="row justify-content-center">
                                        <div class="col-auto text-center">
                                            <label for="recordatorio">Recordatorio:</label>
                                            <input type="hidden" id="tiempoedit<?php echo $rowtareas['id']; ?>" name="tiempoedit<?php echo $rowtareas['id']; ?>">
                                            <select class="form-control" id="recordatorio<?php echo $rowtareas['id']; ?>" name="recordatorio<?php echo $rowtareas['id']; ?>" onchange="sqlreminderedit(<?php echo $rowtareas['id']; ?>)">
                                                <option selected disabled hidden>Seleccione</option>
                                                <option> 30 minutos</option>
                                                <option> 1 hora</option>
                                                <option> 6 horas</option>
                                                <option> 8 horas</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    function sqlreminderedit(id) {
                                        var today = new Date();

                                        var index = document.getElementById("recordatorio" + id).selectedIndex;

                                        switch (index) {
                                            case 1:
                                                var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
                                                today.setMinutes(today.getMinutes() + 1);
                                                var mediah = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
                                                document.getElementById('tiempoedit' + id).value = (date + " " + mediah);

                                                break;

                                            case 2:
                                                var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
                                                today.setHours(today.getHours() + 1);
                                                var unahora = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
                                                document.getElementById('tiempoedit' + id).value = (date + " " + unahora);
                                                break;

                                            case 3:
                                                var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
                                                today.setHours(today.getHours() + 6);
                                                var ochohoras = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
                                                document.getElementById('tiempoedit' + id).value = (date + " " + ochohoras);
                                                break;

                                            case 4:
                                                var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
                                                today.setHours(today.getHours() + 8);
                                                var ochohoras = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
                                                document.getElementById('tiempoedit' + id).value = (date + " " + ochohoras);
                                                break;
                                        }
                                    }
                                </script>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger" style="margin-right: 210px;" id="delete<?php echo $rowtareas['id']; ?>" name="delete<?php echo $rowtareas['id']; ?>">Eliminar</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" id="update<?php echo $rowtareas['id']; ?>" name="update<?php echo $rowtareas['id']; ?>" class="btn btn-primary">Actualizar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center mt-4">
                        <button type="button" class="btn btn-outline-primary btn-lg btn-block w-75" data-toggle="modal" data-target="#modaleditartarea<?php echo $rowtareas['id']; ?>"><?php echo $rowtareas['tarea']; ?></button>
                    </div>

                    <?php
                    if (isset($_POST['update' . $rowtareas['id']])) {
                        if (isset($_POST['titulotareaedit' . $rowtareas['id']])) {
                            $nombre_tarea = $_POST['titulotareaedit' . $rowtareas['id']];
                        }


                        if (isset($_POST['tiempoedit' . $rowtareas['id']])) {
                            $recordatorio = $_POST['tiempoedit' . $rowtareas['id']];
                        }


                        $con->query("UPDATE tareas SET tarea='$nombre_tarea',tiempo='$recordatorio' WHERE id=$rowtareas[id] ") or die($con->error);
                        header("location: to-do");
                        //prevención...
                        ob_end_flush();
                    }

                    if (isset($_POST['delete' . $rowtareas['id']])) {
                        $con->query("DELETE FROM tareas WHERE id=$rowtareas[id] ") or die or die($con->error);
                        $_SESSION['message'] = "Registro eliminado";
                        $_SESSION['msg_type'] = "danger";
                        header("location: to-do");
                        //prevención...
                        ob_end_flush();
                    }
                    ?>
                <?php endwhile; ?>
            </form>

            <div class="row justify-content-center" style="background: #FFFFFF;">
                <button class="btn btn-primary" id="enable" name="enable">Activar notificaciones</button>
            </div>
            <script type="text/javascript" src="js/scripts_to-do.js"></script>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
            <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>

<script>
    window.onload = function() {

        function checkDeadlines() {
            var today = new Date();
            var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
            var time = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
            var fecha = (date + " " + time);

            <?php
            $tareas = "SELECT * FROM tareas WHERE FIND_IN_SET('$userlogged', usuarios)";
            $recordatoriosql = mysqli_query($con, $tareas);
            ?>
            <?php while ($rowrecordatorio = $recordatoriosql->fetch_assoc()) : ?>
                if (fecha == ('<?php echo $rowrecordatorio['tiempo']; ?>')) {
                    createNotification('<?php echo $rowrecordatorio['tarea']; ?>');

                }

            <?php endwhile; ?>
        }


        function createNotification(title) {

            // Create and show the notification
            // let img = '/to-do-notifications/img/icon-128.png';
            let text = 'Recordatorio de "' + title + '".';
            let notification = new Notification('Ticketcreador', {
                body: text,
                //icon: img
            });

            // we need to update the value of notified to "yes" in this particular data object, so the
            // notification won't be set off on it again



        }

        setInterval(checkDeadlines, 1000);
    }
</script>