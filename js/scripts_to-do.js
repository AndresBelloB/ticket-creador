function showmodalcreartarea() {
    if ($('#nombre_tarea').val() != "") {
        var nombretarea = $('#nombre_tarea').val();
        $("#titulomodaltarea").text(nombretarea);
        $("#titulotarea").val(nombretarea);
        $('#modalcreartarea').modal('show');
    } else if ($('#nombre_tarea').val() == "") {
        alert("Campo nombre de la tarea vacío");
    }
}

var seluser = document.getElementById("usuarios")

function selectusers() {
    document.getElementById("selusers").value = $('#usuarios').val();
}
seluser.addEventListener('change', selectusers, false);

function sqlreminder() {

    var today = new Date();

    var index = document.getElementById('recordatorio').selectedIndex;

    switch (index) {
        case 1:
            var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
            today.setMinutes(today.getMinutes() + 30);
            var mediah = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
            document.getElementById('valtiempo').value = (date + " " + mediah);

            break;

        case 2:
            var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
            today.setHours(today.getHours() + 1);
            var unahora = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
            document.getElementById('valtiempo').value = (date + " " + unahora);

            break;

        case 3:
            var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
            today.setHours(today.getHours() + 6);
            var ochohoras = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
            document.getElementById('valtiempo').value = (date + " " + ochohoras);
            break;

        case 4:
            var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
            today.setHours(today.getHours() + 8);
            var ochohoras = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + ((today.getSeconds() < 10 ? '0' : '') + today.getSeconds());
            document.getElementById('valtiempo').value = (date + " " + ochohoras);
            break;
    }

}
const notificationBtn = document.getElementById('enable');

if (Notification.permission === 'denied' || Notification.permission === 'default') {
    notificationBtn.style.display = 'block';
} else {
    notificationBtn.style.display = 'none';
}

function askNotificationPermission() {
    // function to actually ask the permissions
    function handlePermission(permission) {
        // Whatever the user answers, we make sure Chrome stores the information
        if (!('permission' in Notification)) {
            Notification.permission = permission;
        }

        // set the button to shown or hidden, depending on what the user answers
        if (Notification.permission === 'denied' || Notification.permission === 'default') {
            notificationBtn.style.display = 'block';
        } else {
            notificationBtn.style.display = 'none';
        }
    }

    // Let's check if the browser supports notifications
    if (!"Notification" in window) {
        console.log("This browser does not support notifications.");
    } else {
        if (checkNotificationPromise()) {
            Notification.requestPermission()
                .then((permission) => {
                    handlePermission(permission);
                })
        } else {
            Notification.requestPermission(function(permission) {
                handlePermission(permission);
            });
        }
    }
}

// Function to check whether browser supports the promise version of requestPermission()
// Safari only supports the old callback-based version
function checkNotificationPromise() {
    try {
        Notification.requestPermission().then();
    } catch (e) {
        return false;
    }

    return true;
}

// wire up notification permission functionality to "Enable notifications" button

notificationBtn.addEventListener('click', askNotificationPermission);