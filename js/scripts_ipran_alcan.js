//IPRAN

$("#date").datepicker({
    format: 'yyyy/mm/dd',
    language: "es",
    orientation: "bottom auto"
});

var today = new Date();
var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
var time = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + today.getSeconds();
var timedate = date + " " + time;
document.getElementById("date").value = timedate;

$('#div-ventana').hide();
$('#div-tcarrier').hide();
$('#div-tremedy').hide();
$(function () {
    $('#ventanademantenimiento').change(function () {
        if ($(this).prop('checked') == true) {
            $('#div-ventana').slideDown('fast');
        } else {
            $('#div-ventana').slideUp('fast');
        }
    })
    $('#ticketcarrier').change(function () {
        if ($(this).prop('checked') == true) {
            $('#div-tcarrier').slideDown('fast');
        } else {
            $('#div-tcarrier').slideUp('fast');
        }
    })
    $('#ticketremedy').change(function () {
        if ($(this).prop('checked') == true) {
            $('#div-tremedy').slideDown('fast');
        } else {
            $('#div-tremedy').slideUp('fast');
        }
    })
})



function showmodal() {
    let equipo = document.getElementById("equipo").value;
    let fecha = document.getElementById("date").value;
    let descripcionfalla = document.getElementById("descfalla").value;
    if (document.getElementById('intermitencia_ipran').checked == true) {
        let intermitencia = "SI";
    } else {
        let intermitencia = "NO";
    }

    if (document.getElementById("troncal-lineal").checked == true) {
        var troncallineal = "SI";
    } else {
        var troncallineal = "NO";
    }

    if (document.getElementById("troncal-respaldo").checked == true) {
        var troncalrespaldo = "SI";
    } else {
        var troncalrespaldo = "NO";
    }

    if (document.getElementById("compartenhrdw").checked == true) {
        var compartenhrdw = "SI";
    } else {
        var compartenhrdw = "NO";
    }

    let fallaenergia = document.getElementById("fallaenergia").value;
    let proveedor = document.getElementById("proveedor").value;

    if (document.getElementById("ventanademantenimiento").checked == true) {
        var ventanademantenimiento = "SI " + "(" + document.getElementById("textventana1").value + ") \n" + document.getElementById("textventana2").value;
    } else {
        var ventanademantenimiento = "NO";
    }

    if (document.getElementById("escalamientopop").checked == true) {
        var escalamientopop = "SI";
    } else {
        var escalamientopop = "NO";
    }

    if (document.getElementById("ticketcarrier").checked == true) {
        var ticketcarrier = "SI " + "(" + document.getElementById("texttcarrier1").value + ") \n" + document.getElementById("texttcarrier2").value;
    } else {
        var ticketcarrier = "NO";
    }

    if (document.getElementById("ticketremedy").checked == true) {
        var ticketremedy = "SI " + "(" + document.getElementById("texttremedy1").value + ") \n" + document.getElementById("texttremedy2").value;
    } else {
        var ticketremedy = "NO";
    }

    var equiposafectados = document.getElementById("equiposafectados").value;
    var troncalesafectadas = document.getElementById("troncalesafectadas").value;
    var logdealarmas = document.getElementById("logdealarmas").value;

    $('#ModalTxt').modal('show');


    let textoplano = "Equipo pérdida de alcanzabilidad:" + equipo + "\n\n" + "Fecha y hora de inicio: " + fecha + "\n\n" + "Descripción de la falla reportada o detectada: "
        + descripcionfalla + "\n\n" + "Troncal lineal: " + troncallineal + "\n\n" + "Equipo con troncal respaldo: " + troncalrespaldo + "\n\n" + "Troncales comparten Hardware: " + compartenhrdw
        + "\n\n" + "Falla de energía: " + fallaenergia + "\n\n" + "Ventanas de mantenimiento en curso: " + ventanademantenimiento + "\n\n" + "Escalamiento con Planta Externa o Proveedor de Fibra: "
        + escalamientopop + "\n\n" + "Proveedor: " + proveedor + "\n\n" + "Ticket Carrier: " + ticketcarrier + "\n\n" + "Ticket Remedy: " + ticketremedy
        + "\n\nEquipos afectados: " + equiposafectados + "\n\nTroncales afectadas: \n" + troncalesafectadas + "\nLog de alarmas: \n" + logdealarmas;

    document.getElementById("textoplano").value = textoplano;
}

function addsplitter() {
    let text = document.getElementById("equipo").value;
    let split = " // ";
    let res = text.concat(split);
    document.getElementById("equipo").value = res;

}

function mostrarequipos() {

    text = document.getElementById("equipo").value;

    let textsplit = text.split(" // ");

    var equipos = document.getElementById("equiposafectados").value;

    document.getElementById("equiposafectados").value = "";
    textsplit.forEach(mostrartexto);

    function mostrartexto(item, index) {
        document.getElementById("equiposafectados").value += (index + 1) + ". " + item + "\n" + "Servicios afectados:" + "\n\n";
    }

    if (document.getElementById('intermitencia_ipran').checked == true) {
        document.getElementById("descfalla").value = "Se presenta intermitencia del equipo: " + text;
    } else {
        document.getElementById("descfalla").value = "Se presenta perdida de alcanzabilidad del equipo: " + text;
    }

}



$(function () {
    $('#intermitencia_ipran').change(function () {
        if (document.getElementById('intermitencia_ipran').checked == true) {
            document.getElementById("descfalla").value = "Se presenta intermitencia del equipo: " + text;
        } else {
            document.getElementById("descfalla").value = "Se presenta perdida de alcanzabilidad del equipo: " + text;
        }
    });
})