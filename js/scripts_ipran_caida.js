$("#date").datepicker({
    format: 'yyyy/mm/dd',
    language: "es",
    orientation: "bottom auto"
});

var today = new Date();
var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
var time = today.getHours() + ":" + ((today.getMinutes() < 10 ? '0' : '') + today.getMinutes()) + ":" + today.getSeconds();
var timedate = date + " " + time;
document.getElementById("date").value = timedate;

$('#div-ventana').hide();
$('#div-tcarrier').hide();
$('#div-tremedy').hide();

$(function () {
    $('#ventanademantenimiento').change(function () {
        if ($(this).prop('checked') == true) {
            $('#div-ventana').slideDown('fast');
        } else {
            $('#div-ventana').slideUp('fast');
        }
    })
    $('#ticketcarrier').change(function () {
        if ($(this).prop('checked') == true) {
            $('#div-tcarrier').slideDown('fast');
        } else {
            $('#div-tcarrier').slideUp('fast');
        }
    })
    $('#ticketremedy').change(function () {
        if ($(this).prop('checked') == true) {
            $('#div-tremedy').slideDown('fast');
        } else {
            $('#div-tremedy').slideUp('fast');
        }
    })
})

function addsplitter() {
    let text = document.getElementById("troncal").value;
    let split = " // ";
    let res = text.concat(split);
    document.getElementById("troncal").value = res;

}

function mostrarequipos() {
    text = document.getElementById("troncal").value;

    if (document.getElementById('intermitencia_ipran').checked == true) {
        document.getElementById("descfalla").value = "Se presenta intermitencia en la troncal: " + text;
    } else {
        document.getElementById("descfalla").value = "Se presenta caida en la troncal: " + text;
    }

}

$(function () {
    $('#intermitencia_ipran').change(function () {
        if (document.getElementById('intermitencia_ipran').checked == true) {
            document.getElementById("descfalla").value = "Se presenta intermitencia en la troncal: " + text;
        } else {
            document.getElementById("descfalla").value = "Se presenta caida en la troncal: " + text;
        }
    });
})


function showmodal() {
    let troncal = document.getElementById("troncal").value;
    let fecha = document.getElementById("date").value;
    let descripcionfalla = document.getElementById("descfalla").value;

    if (document.getElementById('intermitencia_ipran').checked == true) {
        let intermitencia = "SI";
    } else {
        let intermitencia = "NO";
    }

    if (document.getElementById('fallaenergia').checked == true) {
        var fallaenergia = "SI";
    } else {
        var fallaenergia = "NO";
    }

    let proveedor = document.getElementById("proveedor").value;

    if (document.getElementById("ticketcarrier").checked == true) {
        var ticketcarrier = "SI " + "(" + document.getElementById("texttcarrier1").value + ") \n" + document.getElementById("texttcarrier2").value;
    } else {
        var ticketcarrier = "NO";
    }

    if (document.getElementById("ticketremedy").checked == true) {
        var ticketremedy = "SI " + "(" + document.getElementById("texttremedy1").value + ") \n" + document.getElementById("texttremedy2").value;
    } else {
        var ticketremedy = "NO";
    }

    if (document.getElementById("ventanademantenimiento").checked == true) {
        var ventanademantenimiento = "SI " + "(" + document.getElementById("textventana1").value + ") \n" + document.getElementById("textventana2").value;
    } else {
        var ventanademantenimiento = "NO";
    }

    var troncalesafectadas = document.getElementById("troncalesafectadas").value;
    var logdealarmas = document.getElementById("logdealarmas").value;

    $('#ModalTxt').modal('show');


    let textoplano = "Troncal: " + troncal + "\n\n" + "Fecha y hora de inicio: " + fecha + "\n\n" + "Descripción de la falla reportada o detectada: "
        + descripcionfalla + "\n\n" + "Proveedor: " + proveedor + "\n\n" + "Ticket Carrier: " + ticketcarrier + "\n\n" + "Ticket Remedy: " + ticketremedy
        + "\n\n" + "Ventanas de mantenimiento en curso: " + ventanademantenimiento + "\n\n" + "Falla de energía: " + fallaenergia + "\n\n" + "Troncales afectadas:\n"
        + troncalesafectadas + "\nLog de alarmas: \n" + logdealarmas;

    document.getElementById("textoplano").value = textoplano;

}

