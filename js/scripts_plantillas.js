//EN PLANTILLAS

function showmodalplantillas() {
    var selplantilla = document.getElementById("selplantilla").value;
    if (selplantilla == "reincidencia_sdh") {
        if (reincidenciaequipos() != false) {
            $('#modalplantillas').modal('show');
        }
    } else if (selplantilla == "reporte_sinfalla") {
        if (reportesinfalla() != false) {
            $('#modalplantillas').modal('show');
        }
    } else if (selplantilla == "reporte_confalla") {
        if (reporteconfalla() != false) {
            $('#modalplantillas').modal('show');
        }
    } else if (selplantilla == "manten_elec") {
        if (mantenelec() != false) {
            $('#modalplantillas').modal('show');
        }
    }
}

function reincidenciaequipos() {
    var textoplano = "";
    document.getElementById('textoplano_plantillas').value = "";
    if ($('#revisioninicial').val() != "" && $('#redafectada').val() != "" && $('#equipotramo').val() != "" && $('#proveedor').val() != "") {
        textoplano = "FO SDH REVISIÓN INICIAL: " + $('#revisioninicial').val() + "\n\n" + "Red afectada: " +
            $('#redafectada').val() + "\nEquipo/tramo: " + $('#equipotramo').val() + "\nProveedor: " + $('#proveedor').val();
        document.getElementById('textoplano_plantillas').value = textoplano;
    } else {
        alert("Por favor llene todos los campos");
        return false;
    }

}

function reportesinfalla() {
    var textoplano = "";
    document.getElementById('textoplano_plantillas').value = "";
    if ($('#siglas_rsinfalla').val() != "" && $('#nreporte_rsinfalla').val() != "" && $('#relec_rsinfalla').val() != "" && $('#nasesor_rsinfalla').val() != "") {

        textoplano = "FEE SEGUIMIENTO: " + "(" + $('#siglas_rsinfalla').val() + ")S: Se establece comunicación con la empresa de energía " + $('#nreporte_rsinfalla').val() + ", indican sin aparente fallas o trabajos de mantenimiento de energía en la zona, se genera reporte N° " +
            $('#relec_rsinfalla').val() + " para revisión. Atiende " + $('#nasesor_rsinfalla').val() + ".";
        document.getElementById('textoplano_plantillas').value = textoplano;
    } else {
        alert("Por favor llene todos los campos");
        return false;
    }
}

function reporteconfalla() {
    var textoplano = "";
    document.getElementById('textoplano_plantillas').value = "";
    if ($('#siglas_rconfalla').val() != "" && $('#nreporte_rconfalla').val() != "" && $('#relec_rconfalla').val() != "" && $('#nasesor_rconfalla').val() != "") {

        textoplano = "FEE SEGUIMIENTO: " + "(" + $('#siglas_rconfalla').val() + ")S: Se establece comunicación con la empresa de energía " + $('#nreporte_rconfalla').val() + ", indican que si presentan falla de energía en la zona, se genera reporte N° " +
            $('#relec_rconfalla').val() + " para revisión. Atiende " + $('#nasesor_rconfalla').val() + ".";
        document.getElementById('textoplano_plantillas').value = textoplano;
    } else {
        alert("Por favor llene todos los campos");
        return false;
    }
}

function mantenelec() {
    var textoplano = "";
    document.getElementById('textoplano_plantillas').value = "";
    if ($('#siglas_mantenelec').val() != "" && $('#nreporte_mantenelec').val() != "" && $('#hinicio_mantenelec').val() != "" && $('#hfinal_mantenelec').val() != "" && $('#reporte_mantenelec').val() != "" && $('#nasesor_mantenelec').val() != "") {

        textoplano = "FEE SEGUIMIENTO: " + "(" + $('#siglas_mantenelec').val() + ")S: Se establece comunicación con la empresa de energía " + $('#nreporte_mantenelec').val() + " indican que tienen trabajo programado desde las " + $('#hinicio_mantenelec').val() + " hasta las " + $('#hfinal_mantenelec').val() +
            " se genera reporte N° " + $('#reporte_mantenelec').val() + ". Atiende " + $('#nasesor_mantenelec').val() + ".";
        document.getElementById('textoplano_plantillas').value = textoplano;
    } else {
        alert("Por favor llene todos los campos");
        return false;
    }
}

var selplantilla = document.getElementById("selplantilla");

function selplantillas() {
    var selplantillaval = selplantilla.value;
    switch (selplantillaval) {
        case 'reincidencia_sdh':
            $('#divreincidencia_sdh').slideDown('fast');
            $('#divreporte_sinfalla').slideUp('fast');
            $('#divreporte_confalla').slideUp('fast');
            $('#divmanten_elec').slideUp('fast');
            break;
        case 'reporte_sinfalla':
            $('#divreincidencia_sdh').slideUp('fast');
            $('#divreporte_sinfalla').slideDown('fast');
            $('#divreporte_confalla').slideUp('fast');
            $('#divmanten_elec').slideUp('fast');
            break;
        case 'reporte_confalla':
            $('#divreincidencia_sdh').slideUp('fast');
            $('#divreporte_sinfalla').slideUp('fast');
            $('#divreporte_confalla').slideDown('fast');
            $('#divmanten_elec').slideUp('fast');
            break;
        case 'manten_elec':
            $('#divreincidencia_sdh').slideUp('fast');
            $('#divreporte_sinfalla').slideUp('fast');
            $('#divreporte_confalla').slideUp('fast');
            $('#divmanten_elec').slideDown('fast');
            break;
    }

}

selplantilla.addEventListener('change', selplantillas, false);


//SCRIPTS PLANTILLAS NUEVO