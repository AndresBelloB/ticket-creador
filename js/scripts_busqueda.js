$('#nombre').prop("disabled", true);

//COLORES CRITICIDAD
var selectcrit = document.getElementById('criticidad');

function logValue() {
    var seleccioncrit = selectcrit.value;
    switch (this.value) {
        case 'CCM':
            document.body.style.background = "#ff4747";
            selectcrit.style.color = "#ffffff"
            document.getElementById('nav-registrar').style.color = "#ffffff";
            document.getElementById('nav-busqueda').style.color = "#ffffff";
            document.getElementById('nav-tareas').style.color = "#ffffff";
            document.getElementById('nav-indicadores').style.color = "#ffffff";
            document.getElementById('nav-consulta').style.color = "#ffffff";
            document.getElementById('nav-plantillas').style.color = "#ffffff";
            document.getElementById('nav-ipran').style.color = "#ffffff";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "#ffffff";
            }
            document.getElementById('nav-alarmas').style.color = "#ffffff";
            document.getElementById('cerrarsesion').style.color = "#ffffff";
            selectcrit.style.background = "#ff4747";
            break;
        case 'CRITICA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.background = "#ff4747";
            selectcrit.style.color = "#ffffff";
            break;
        case 'ALTA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "#ffffff";
            selectcrit.style.background = "#ff6400 ";
            break;
        case 'MEDIA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "#ffc300";
            break;
        case 'BAJA':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "";
            break;
        case 'N/A':
            document.body.style.background = "";
            document.getElementById('nav-registrar').style.color = "";
            document.getElementById('nav-busqueda').style.color = "";
            document.getElementById('nav-tareas').style.color = "";
            document.getElementById('nav-indicadores').style.color = "";
            document.getElementById('nav-consulta').style.color = "";
            document.getElementById('nav-plantillas').style.color = "";
            document.getElementById('nav-ipran').style.color = "";
            if (document.getElementById('nav-admin')) {
                document.getElementById('nav-admin').style.color = "";
            }
            document.getElementById('nav-alarmas').style.color = "";
            document.getElementById('cerrarsesion').style.color = "";
            selectcrit.style.color = "";
            selectcrit.style.background = "";
            break;

    }
}

selectcrit.addEventListener('change', logValue, false);
btnbuscar = document.getElementById('buscar');
buscar.addEventListener('Click', logValue, false);

//ANIMACIÓN 100%

$valgsm = $("#labelgsm").text();
$valumts = $("#labelumts").text();
$vallte = $("#labellte").text();

if ($valgsm == "100%") {
    document.getElementById('labelgsm1').style.color = "#ff4747";
    // document.getElementById('labelgsm1').style.color="#ff4747";
    document.getElementById('labelgsm').className = "box bounce";
} else {
    document.getElementById('labelgsm').className = "";
    document.getElementById('labelgsm1').style.color = "";
}

if ($valumts == "100%") {
    document.getElementById('labelumts').className = "box bounce";;
    document.getElementById('labelumts1').style.color = "#ff4747";
} else {
    document.getElementById('labelumts').className = "";
    document.getElementById('labelumts1').style.color = "";
}

if ($vallte == "100%") {
    document.getElementById('labellte').className = "box bounce";;
    document.getElementById('labellte1').style.color = "#ff4747";
} else {
    document.getElementById('labellte').className = "";
    document.getElementById('labellte1').style.color = "";
}

//FUNCIONES PARA MODALS TEXTO PLANO

function showmodal() {
    if ($('#user').val() != "" && $('#diagnostico').val() != "" && $('#nombre').val() != "") {
        if ($('#sintrafico').val() == "PERDGESTION" && document.getElementById('GSM').checked == false && document.getElementById('UMTS').checked == false && document.getElementById('LTE').checked == false) {
            document.getElementById("aviso-body").innerHTML = "<p class='text-justify'>" + "Por favor seleccione una tecnología al generar un caso de uso de pérdida de gestión." + "</p>";
            $('#aviso').modal('show');
        } else {
            textoplano();
            $('#ModalTxt').modal('show');
        }
    } else if ($('#user').val() == "" || $('#diagnostico').val() == "") {
        alert("Campo siglas o diagnóstico vacíos");
    } else if ($('#nombre').val() == "") {
        alert("Por favor realice su consulta");
    }
}

function descargartexto() {
    if ($('#user').val() != "" && $('#diagnostico').val() != "") {
        textoplano();
    } else if ($('#user').val() == "" || $('#diagnostico').val() == "") {
        alert("Campo siglas o diagnóstico vacíos");
    }
}



// DESCARGA DE TEXTO PLANO

$(document).ready(function() {

    function saveTextAsFile() {
        var textToWrite = document.getElementById("textoplano").value;
        textToWrite = textToWrite + "\n\n" + document.getElementById("textoplano1").value;
        var textFileAsBlob = new Blob([textToWrite], { type: 'text/plain' });
        var fileNameToSaveAs = "generado.txt";
        var downloadLink = document.createElement("a");
        downloadLink.download = fileNameToSaveAs;

        // Hidden link.
        downloadLink.innerHTML = "My Hidden Link";

        // allow our code to work in webkit & Gecko based browsers
        // without the need for a if / else block.
        window.URL = window.URL || window.webkitURL;

        // Create the link Object.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        // when link is clicked call a function to remove it from
        // the DOM in case user wants to save a second file.
        downloadLink.onclick = destroyClickedElement;
        // make sure the link is hidden.
        downloadLink.style.display = "none";
        // add the link to the DOM
        document.body.appendChild(downloadLink);

        // click the new link
        downloadLink.click();
    }

    function destroyClickedElement(event) {
        // remove the link from the DOM
        document.body.removeChild(event.target);
    }



    $("#download").click(function(e) {

        e.preventDefault();
        saveTextAsFile();
    });
});

$(document).ready(function() {

    function saveTextAsFile() {
        var textToWrite = document.getElementById("textoplanohidden").value;
        textToWrite = textToWrite + "\n\n" + document.getElementById("textoplano2hidden").value;
        var textFileAsBlob = new Blob([textToWrite], { type: 'text/plain' });
        var fileNameToSaveAs = "generado.txt";
        var downloadLink = document.createElement("a");
        downloadLink.download = fileNameToSaveAs;

        // Hidden link.
        downloadLink.innerHTML = "My Hidden Link";

        // allow our code to work in webkit & Gecko based browsers
        // without the need for a if / else block.
        window.URL = window.URL || window.webkitURL;

        // Create the link Object.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        // when link is clicked call a function to remove it from
        // the DOM in case user wants to save a second file.
        downloadLink.onclick = destroyClickedElement;
        // make sure the link is hidden.
        downloadLink.style.display = "none";
        // add the link to the DOM
        document.body.appendChild(downloadLink);

        // click the new link
        downloadLink.click();
    }

    function destroyClickedElement(event) {
        // remove the link from the DOM
        document.body.removeChild(event.target);
    }



    $("#download2").click(function(e) {

        e.preventDefault();
        saveTextAsFile();
    });
});


//SELECTOR DE CASO DE USO

var casodeuso = document.getElementById("casosdeuso");
var planta = document.getElementById("planta");



function diagnosticocaso() {
    var selplanta = planta.value;
    var selcaso = casodeuso.value;
    switch (selcaso) {
        case 'NO ESPECIFICADO':
            document.getElementById("diagnostico").value = "";
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divcheckbv').slideUp('fast');
            if (document.getElementById("area").value != "FAOB") {
                $('#divsectores').slideUp('fast');
            }
            $('#blporenergia').slideUp('fast');


            break;

        case 'ALTATEMP':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideUp('fast');
            $('#blporenergia').slideUp('fast');
            $('#blporenergia').slideUp('fast');
            $('#divcheckbv').slideUp('fast');
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ NOTIFICACION/POSIBLE AFECTACION/SIN AFECTACION \ ALARMA DE ENERGIA";

            if (document.getElementById("area").value == "FEE") {
                document.getElementById("diagnostico").value = "Se presenta alarma de alta temperatura en la EB , se requiere revisar las condiciones ambientales, el sistema de AA o la causa de la alarma, en caso tal que sea una alarma que no se este reportando de manera correcta por favor realizar la respectiva certificación de alarmas.";
            }
            break;

        case 'PLANTAEN':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideUp('fast');
            $('#blporenergia').slideUp('fast');
            $('#divcheckbv').slideUp('fast');

            if (document.getElementById("area").value == "FEE") {
                document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con respaldo de planta eléctrica se realizará verificación con Eletrificadora. Se requiere validación de la PE y revisión de MT/BT, cañuelas y/o condiciones eléctricas en sitio.";
            }

            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ NOTIFICACION/POSIBLE AFECTACION/SIN AFECTACION \ ALARMA DE ENERGIA";

            if (selplanta == "SIN PLANTA") {
                var avisosinplanta = "<p class='text-justify'>Usted está generando un caso de planta encendida de un sitio que registra sin planta, puede ser que:</p>" +
                    "<p class='text-justify'>- Esté generando un caso de certificación de alarmas, por favor dejarlo documentado en el diagnóstico." + "<br>- Está cometiendo un error por favor validar los datos." + "<br>- El sitio ahora cuenta con planta eléctrica por favor actualizar la información.</p>";
                document.getElementById("aviso-body").innerHTML = avisosinplanta;
                $('#aviso').modal('show');
            } else {

            }
            break;
        case 'FUERADES':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideDown('fast');
            $('#blporenergia').slideUp('fast');
            $('#divcheckbv').slideUp('fast');

            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ AFECTACION DE SERVICIO_ACCESO \ CONTINUA";
            if (document.getElementById("area").value == "FEE") {
                if (selplanta == "SIN PLANTA") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio sin respaldo de planta eléctrica se realizará verificación con la electrificadora.";
                } else if (selplanta == "SI") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con respaldo de planta eléctrica se realizará verificación con la electrificadora. Se requiere validación de la PE y revisión de MT/BT, cañuelas y/o condiciones eléctricas en sitio";
                } else if (selplanta == "7X24") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con modalidad de trabajo de planta eléctrica 7x24 se requiere revisión de la PE, transferencia, combustible y condiciones eléctricas en sitio.";
                } else if (selplanta == "7X12") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con modalidad de trabajo de planta eléctrica 7x12 se requiere revisión de la PE, transferencia, combustible, baterías y condiciones eléctricas en sitio.";
                }
            }
            break;

        case 'FALLAALCATEL':
            $('#casoalcatelsdh').slideDown('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideUp('fast');
            $('#blporenergia').slideUp('fast');
            $('#divcheckbv').slideUp('fast');

            if (document.getElementById("area").value == "FEE") {
                document.getElementById("diagnostico").value = "Se requiere revisar en sitio las condiciones de energía del equipo Alcatel " + document.getElementById('equipocaso').value +
                    " de la estación base " + document.getElementById('nombre').value + " dado que está presentando alarmas de energía directamente sobre el equipo"
            }
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ NOTIFICACION/POSIBLE AFECTACION/SIN AFECTACION \ ALARMA DE ENERGIA";

            break;
        case 'FALLASDH':
            $('#casoalcatelsdh').slideDown('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideUp('fast');
            $('#blporenergia').slideUp('fast');
            $('#divcheckbv').slideUp('fast');

            if (document.getElementById("area").value == "FEE") {
                document.getElementById("diagnostico").value = "Se requiere revisar en sitio las condiciones de energía del equipo SDH " + document.getElementById('equipocaso').value +
                    " de la estación base " + document.getElementById('nombre').value + " dado que está presentando alarmas de energía directamente sobre el equipo"
            }
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ NOTIFICACION/POSIBLE AFECTACION/SIN AFECTACION \ ALARMA DE ENERGIA";

            break;

        case 'POWERBATERIA':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideUp('fast');
            $('#blporenergia').slideUp('fast');
            $('#divcheckbv').slideDown('fast');

            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ NOTIFICACION/POSIBLE AFECTACION/SIN AFECTACION \ ALARMA DE ENERGIA";

            if (document.getElementById('bloqueo').checked == true) {
                document.getElementById("aviso-body").innerHTML = "";
                var avisosinplanta = "<p class='text-justify'>Está apunto de generar un ticket por power en baterías de un sitio con bloqueo autómatico recuerde hacer el escalamiento en el menor" +
                    " tiempo posible y notificar tanto al gerente de zona, responsable de ticket, IM y comunicados. En caso de tener dudas verificar el estado de las ODU, NetNumen, USMS y/o con la electrificadora.</p>";
                document.getElementById("aviso-body").innerHTML = avisosinplanta;
                $('#aviso').modal('show');
            }
            if (document.getElementById("area").value == "FEE") {
                if (selplanta == "SIN PLANTA") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio sin respaldo de planta eléctrica se realizará verificación con la electrificadora.";
                } else if (selplanta == "SI") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con respaldo de planta eléctrica se realizará verificación con la electrificadora. Se requiere validación de la PE y revisión de MT/BT, cañuelas y/o condiciones eléctricas en sitio";
                } else if (selplanta == "7X24") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con modalidad de trabajo de planta eléctrica 7x24 se requiere revisión de la PE, transferencia, combustible y condiciones eléctricas en sitio.";
                } else if (selplanta == "7X12") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con modalidad de trabajo de planta eléctrica 7x12 se requiere revisión de la PE, transferencia, combustible, baterías y condiciones eléctricas en sitio.";
                }
            }
            break;

        case 'BAJOVOLTAJEB':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideUp('fast');
            $('#blporenergia').slideUp('fast');
            $('#divcheckbv').slideUp('fast');

            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ NOTIFICACION/POSIBLE AFECTACION/SIN AFECTACION \ ALARMA DE ENERGIA";

            if (document.getElementById('bloqueo').checked == true) {
                document.getElementById("aviso-body").innerHTML = "";
                var avisosinplanta = "<p class='text-justify'>Está apunto de generar un ticket por bajo voltaje en baterías de un sitio con bloqueo autómatico recuerde hacer el escalamiento en el menor" +
                    " tiempo posible y notificar tanto al gerente de zona, responsable de ticket, IM y comunicados. En caso de tener dudas verificar el estado de las ODU, NetNumen, USMS y/o con la electrificadora.</p>";
                document.getElementById("aviso-body").innerHTML = avisosinplanta;
                $('#aviso').modal('show');
            }
            if (document.getElementById("area").value == "FEE") {
                if (selplanta == "SIN PLANTA") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio sin respaldo de planta eléctrica se realizará verificación con la electrificadora.";
                } else if (selplanta == "SI") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con respaldo de planta eléctrica se realizará verificación con la electrificadora. Se requiere validación de la PE y revisión de MT/BT, cañuelas y/o condiciones eléctricas en sitio";
                } else if (selplanta == "7X24") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con modalidad de trabajo de planta eléctrica 7x24 se requiere revisión de la PE, transferencia, combustible y condiciones eléctricas en sitio.";
                } else if (selplanta == "7X12") {
                    document.getElementById("diagnostico").value = "Se presentan alarmas de energía, sitio con modalidad de trabajo de planta eléctrica 7x12 se requiere revisión de la PE, transferencia, combustible, baterías y condiciones eléctricas en sitio.";
                }
            }
            break;
        case 'GM':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideDown('fast');
            $('#divsectores').slideUp('fast');
            $('#divcheckbv').slideUp('fast');

            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ AFECTACION DE SERVICIO_ACCESO \ CONTINUA";
            document.getElementById("diagnostico").value = "";
            break;
        case 'BLPORENERGIA':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideUp('fast');
            $('#blporenergia').slideDown('fast');
            $('#divcheckbv').slideUp('fast');

            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ AFECTACION DE SERVICIO_ACCESO \ CONTINUA";
            if (document.getElementById('bloqueo').checked == false) {
                document.getElementById("aviso-body").innerHTML = "";
                var avisoenergia = "<p class='text-justify'>El sitio no registra con bloqueo por alarmas de energía, validar si la información debe ser actualizada para notificarla a su líder o por el contrario revise si el procedimiento que esta realizando es el correcto.</p>";
                document.getElementById("aviso-body").innerHTML = avisoenergia;
                $('#aviso').modal('show');
            }


            break;
    }
}
casodeuso.addEventListener('change', diagnosticocaso, false);
planta.addEventListener('change', diagnosticocaso, false);
$('#bloqueo').change(function() {
    diagnosticocaso(false);
});


// Selector sintrafico
// $(function() {
//     if (document.getElementById("sintrafico").value == "FUERADES" && document.getElementById("area").value == "FAOB") {
//         // $('#divsectores').slideDown('fast');
//     }
// })

//Configuración inical FAOB
$(function() {
    var sintrafico = document.getElementById("sintrafico").value;
    switch (sintrafico) {
        case 'FUERADES':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideDown('fast');
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ AFECTACION DE SERVICIO_ACCESO \ CONTINUA";
            document.getElementById("diagnostico").value = "";
            break;
    }
})

function sintraficosel() {
    var sintrafico = document.getElementById("sintrafico").value;
    var selplanta = document.getElementById("planta").value;
    switch (sintrafico) {
        case 'FUERADES':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideDown('fast');
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ AFECTACION DE SERVICIO_ACCESO \ CONTINUA";
            document.getElementById("diagnostico").value = "";
            break;
        case 'DATOS':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideDown('fast');
            document.getElementById("diagnostico").value = "";
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ AFECTACION DE SERVICIO_ACCESO \ CONTINUA";
            break;

        case 'VOZ':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideDown('fast');
            document.getElementById("diagnostico").value = "";
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ AFECTACION DE SERVICIO_ACCESO \ CONTINUA";
            break;

        case 'VOZ-DATOS':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideDown('fast');
            document.getElementById("diagnostico").value = "";
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ AFECTACION DE SERVICIO_ACCESO \ CONTINUA";
            break;

        case 'PERDGESTION':
            $('#casoalcatelsdh').slideUp('fast');
            $('#casogm').slideUp('fast');
            $('#divsectores').slideDown('fast');
            document.getElementById("diagnostico").value = "";
            document.getElementById("rutaclasificacion").innerHTML = "SERVICIOS MOVILES \ PERFORMANCE/CALIDAD/DEGRADACION \ CONTINUA";
            break;

    }
}

document.getElementById("sintrafico").addEventListener('change', sintraficosel, false);


// FUERA DE SERVICIO CON BOTONES GSM UMT Y LTE

$(function() {
    $('#GSM').change(function() {
        if (document.getElementById('GSM').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#casosdeuso').val("FUERADES");
                $('#sintrafico').val("PERDGESTION");
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').val("FUERADES");
                $('#sintrafico').val("FUERADES");
                $('#divsectores').slideDown('fast');
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('UMTS').checked == true || document.getElementById('LTE').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('UMTS').checked == true && document.getElementById('LTE').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('UMTS').checked == false && document.getElementById('LTE').checked == false) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {

            } else {
                $('#casosdeuso').val("NO ESPECIFICADO");
                $('#divsectores').slideUp('fast');
                $('#100municipio').bootstrapToggle('off')
                $('#100municipio').bootstrapToggle('disable')
                $('#sintrafico').prop("disabled", false);
                $('#casosdeuso').prop("disabled", false);
            }
        }
    });
})
$(function() {
    $('#UMTS').change(function() {
        if (document.getElementById('UMTS').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#casosdeuso').val("FUERADES");
                $('#sintrafico').val("PERDGESTION");
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').val("FUERADES");
                $('#sintrafico').val("FUERADES");
                $('#divsectores').slideDown('fast');
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('LTE').checked == true || document.getElementById('GSM').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('LTE').checked == true && document.getElementById('GSM').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('LTE').checked == false && document.getElementById('GSM').checked == false) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {

            } else {
                $('#casosdeuso').val("NO ESPECIFICADO");
                $('#divsectores').slideUp('fast');
                $('#100municipio').bootstrapToggle('off')
                $('#100municipio').bootstrapToggle('disable')
                $('#casosdeuso').prop("disabled", false);
                $('#sintrafico').prop("disabled", false);
            }
        }
    });
})
$(function() {
    $('#LTE').change(function() {
        if (document.getElementById('LTE').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#casosdeuso').val("FUERADES");
                $('#sintrafico').val("PERDGESTION");
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').val("FUERADES");
                $('#sintrafico').val("FUERADES");
                $('#divsectores').slideDown('fast');
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('UMTS').checked == true || document.getElementById('GSM').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('UMTS').checked == true && document.getElementById('GSM').checked == true) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {
                $('#100municipio').bootstrapToggle('enable');
            } else {
                $('#casosdeuso').prop("disabled", true);
                $('#sintrafico').prop("disabled", true);
                $('#100municipio').bootstrapToggle('enable');
            }
        } else if (document.getElementById('UMTS').checked == false && document.getElementById('GSM').checked == false) {
            if (document.getElementById('sintrafico').value == "PERDGESTION") {

            } else {
                $('#casosdeuso').val("NO ESPECIFICADO");
                $('#divsectores').slideUp('fast');
                $('#100municipio').bootstrapToggle('off')
                $('#100municipio').bootstrapToggle('disable')
                $('#casosdeuso').prop("disabled", false);
                $('#sintrafico').prop("disabled", false);
            }
        }
    });
})
$('#100municipio').bootstrapToggle('disable')


//MANTENER VALORES SIGLAS


$(function() {
    document.getElementById("user").value = localStorage.getItem("siglas");

    var siglas = document.getElementById("user");
    siglas.addEventListener('change', (event) => {
        localStorage.setItem("siglas", siglas.value);
    });
});


function alarmagm() {
    var selectalarmas = document.getElementById("selectalarmagm").value;
    var resselectalarmas = selectalarmas.split(/&/);

    if (resselectalarmas[0] == "2") {
        document.getElementById("critalarmagm").style.background = "#ff4747";
    } else if (resselectalarmas[0] == "3") {
        document.getElementById("critalarmagm").style.background = "#ff6400";
    } else if (resselectalarmas[0] == "4") {
        document.getElementById("critalarmagm").style.background = "#ffc300";
    } else if (resselectalarmas[0] == "4") {
        document.getElementById("critalarmagm").style.background = "#ffc300";
    }

    document.getElementById("diagnostico").value = resselectalarmas[2];
}

//AVISO TICKET FSP

function avisoticketfsp() {
    document.getElementById("aviso-body").innerHTML = "<p class='text-justify'>" + "Para generar un ticket FSP se debe tener las siguientes consideraciones: \n" + "</p>" +
        "<p class='text-justify'>" + "- Cuando se tenga una falla de una EB y se haya solucionado parcialmente y queden unos sectores por subir o una tecnología, sobre el mismo caso se hace un nuevo diagnóstico y se genera tarea u OT nueva sobre el mismo ticket, modificando el resumen de la tarea o la OT según aplique." +
        "<br>- Cuando se tenga una falla masiva y se haya solucionado de forma parcial se crea un ticket nuevo tipo FSP, sacar las alarmas del ticket anterior y crear uno nuevo garantizando que todas las alarmas queden debidamente relacionadas y en el resumen especificar el número de ticket." +
        "<br>- Cuando se tenga un caso regulatorio y se haya solucionado de forma parcial se crea un ticket nuevo tipo FSP, sacar las alarmas del ticket anterior y crear uno nuevo garantizando que todas las alarmas queden debidamente relacionadas y en el resumen especificar el número de ticket." +
        "<br><br>Ante cualquier duda con respecto al procedimiento a seguir apoyarse con el líder en turno y el IM." + "</p>";
    $('#aviso').modal('show');
}

$('#ticketFSP').one("focus", avisoticketfsp);


//AJAX PARA TABLAS

// function tablagsm() {
//     var nombre = document.getElementById("nombrehidden").value;
//     $.ajax({
//         type: "POST",
//         url: '../proceso.php',
//         data: { action: nombre },
//         success: function(html) {
//             alert(html);
//         }

//     });
// }