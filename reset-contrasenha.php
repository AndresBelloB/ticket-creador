<?php

if(isset($_POST['reset-password'])){
    
    $selector = $_POST["selector"];
    $validator = $_POST["validator"];
    $password = $_POST["password"];
    $passwordvalidate = $_POST["passwordvalidate"];

    if(empty($password || $passwordvalidate)){
        session_start();
        $_SESSION['message'] = "Ingrese al link enviado a su correo nuevamente, campos vacíos";
        $_SESSION['msg_type'] = "danger";
        header("location: recuperarcontrasenha.php");
        exit();
    }elseif($password != $passwordvalidate){
        session_start();
        $_SESSION['message'] = "Ingrese al link enviado a su correo nuevamente, las contraseñas no coinciden";
        $_SESSION['msg_type'] = "danger";
        header("location: recuperarcontrasenha.php");
        exit();
    }

    $currentdate = date("U");

    require 'con_db.php';

    $sql = "SELECT * FROM recuperarcontra WHERE recuperar_selector=? AND recuperar_expires >=?";
    $stmt = mysqli_stmt_init($con);
    if(!mysqli_stmt_prepare($stmt,$sql)){
        session_start();
        $_SESSION['message'] = "Error en base de datos";
        $_SESSION['msg_type'] = "danger";
        header("location: recuperarcontrasenha.php?error=mysqlerror");
        exit();
    }else{
        mysqli_stmt_bind_param($stmt, "ss", $selector, $currentdate);
        mysqli_stmt_execute($stmt);

        $result = mysqli_stmt_get_result($stmt);
        if(!$row = mysqli_fetch_assoc($result)){
            echo "Por favor realice el proceso nuevamente";
            exit();
        }else{
            $tokenbin = hex2bin($validator);
            $tokencheck = password_verify($tokenbin,$row["recuperar_token"]);

            if($tokencheck === false){
                echo "Por favor realice el proceso nuevamente";
                exit();
            }elseif($tokencheck === true){
                $tokenemail = $row['recuperar_email'];

                $sql = "SELECT * FROM recuperarcontra WHERE recuperar_email=?;";
                $stmt = mysqli_stmt_init($con);
                if(!mysqli_stmt_prepare($stmt,$sql)){
                    echo "ERROR!";
                    exit();
                }else{
                    mysqli_stmt_bind_param($stmt, "s", $tokenemail);
                    mysqli_stmt_execute($stmt);
                    $result = mysqli_stmt_get_result($stmt);
                    if(!$row = mysqli_fetch_assoc($result)){
                        echo "ERROR!";
                        exit();
                    }else{
                        
                        $sql = "UPDATE usuarios SET contra=? WHERE email=?";
                        $stmt = mysqli_stmt_init($con);
                        if(!mysqli_stmt_prepare($stmt,$sql)){
                            echo "ERROR!";
                            exit();
                        }else{
                            $newhashpass = password_hash($password, PASSWORD_DEFAULT);
                            mysqli_stmt_bind_param($stmt, "ss", $newhashpass, $tokenemail);
                            mysqli_stmt_execute($stmt);
                            $sql = "DELETE FROM recuperarcontra WHERE recuperar_email=?";
                            $stmt = mysqli_stmt_init($con);
                            if(!mysqli_stmt_prepare($stmt,$sql)){
                            echo "ERROR!";
                            exit();
                            }else{
                                mysqli_stmt_bind_param($stmt, "s", $tokenemail);
                                mysqli_stmt_execute($stmt);
                                session_start();
                                $_SESSION['message'] = "Contraseña actualizada!";
                                $_SESSION['msg_type'] = "success";
                                header("location: iniciarsesion.php?msg=contraseñaactualizada");
                                exit();

                            }

                }


                    }
                }
            }
        }
    }

}else{
    header("iniciarsesion.php");
}