<?php
if (isset($_POST['g-recaptcha-response'])) {
    $secretkey = "6LcPAzIaAAAAAK2V4yOK86vx8ReDqW3m5bt7zxCu";
    $ip = $_SERVER['REMOTE_ADDR'];
    $response = $_POST['g-recaptcha-response'];
    $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretkey&response=$response&remoteip=$ip";
    $fire = file_get_contents($url);
    $data = json_decode($fire);
    if ($data->success == true) {
        if (isset($_POST['crearusuario'])) {
            require 'con_db.php';

            $nombres = $_POST['nombres'];
            $apellidos = $_POST['apellidos'];
            $email = $_POST['email'];
            $user = $_POST['user'];
            $password = $_POST['password'];
            $validatepassword = $_POST['validatepassword'];

            if (empty($nombres) || empty($apellidos) || empty($email) || empty($user) || empty($password) || empty($validatepassword)) {
                header("location: registrousuarios");
                exit();
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                session_start();
                $_SESSION['message'] = "Email inválido";
                $_SESSION['msg_type'] = "danger";
                header("location: registrousuarios?error=invalidemail&nombres=" . $nombres . "&apellidos=" . $apellidos . "&usuario=" . $user);
                exit();
            } elseif ($password !== $validatepassword) {
                session_start();
                $_SESSION['message'] = "Las contraseñas no coinciden";
                $_SESSION['msg_type'] = "danger";
                header("location: registrousuarios?error=passwordcheck&nombres=" . $nombres . "&apellidos=" . $apellidos . "&email=" . $email . "&usuario=" . $user);
                exit();
            } elseif (strlen($password) < 6) {
                session_start();
                $_SESSION['message'] = "La clave debe tener al menos 6 caracteres";
                $_SESSION['msg_type'] = "danger";
                header("location: registrousuarios?error=contrasenainvalida&nombres=" . $nombres . "&apellidos=" . $apellidos . "&usuario=" . $user . "&email=" . $email);
                exit();
            } elseif (strlen($password) > 20) {
                session_start();
                $_SESSION['message'] = "La clave no puede tener más de 16 caracteres";
                $_SESSION['msg_type'] = "danger";
                header("location: registrousuarios?error=contrasenainvalida&nombres=" . $nombres . "&apellidos=" . $apellidos . "&usuario=" . $user . "&email=" . $email);
                exit();
            } elseif (!preg_match('`[a-z]`', $password)) {
                session_start();
                $_SESSION['message'] = "La clave debe tener al menos una letra minúscula";
                $_SESSION['msg_type'] = "danger";
                header("location: registrousuarios?error=contrasenainvalida&nombres=" . $nombres . "&apellidos=" . $apellidos . "&usuario=" . $user . "&email=" . $email);
                exit();
            } elseif (!preg_match('`[A-Z]`', $password)) {
                session_start();
                $_SESSION['message'] = "La clave debe tener al menos una letra mayúscula";
                $_SESSION['msg_type'] = "danger";
                header("location: registrousuarios?error=contrasenainvalida&nombres=" . $nombres . "&apellidos=" . $apellidos . "&usuario=" . $user . "&email=" . $email);
                exit();
            } elseif (!preg_match('`[0-9]`', $password)) {
                session_start();
                $_SESSION['message'] = "La clave debe tener al menos un caracter numérico";
                $_SESSION['msg_type'] = "danger";
                header("location: registrousuarios?error=contrasenainvalida&nombres=" . $nombres . "&apellidos=" . $apellidos . "&usuario=" . $user . "&email=" . $email);
                exit();
            } else {
                $sql = "SELECT usuario FROM usuarios WHERE usuario=?";
                $stmt = mysqli_stmt_init($con);
                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    session_start();
                    $_SESSION['message'] = "Error base de datos";
                    $_SESSION['msg_type'] = "danger";
                    header("location: registrousuarios?error=sqlerror");
                    exit();
                } else {
                    mysqli_stmt_bind_param($stmt, "s", $user);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_store_result($stmt);
                    $resultcheck = mysqli_stmt_num_rows($stmt);
                    if ($resultcheck > 0) {
                        session_start();
                        $_SESSION['message'] = "Usuario existente";
                        $_SESSION['msg_type'] = "danger";
                        header("location: registrousuarios?error=usertaken&nombres=" . $nombres . "&apellidos=" . $apellidos . "&email=" . $email);
                        exit();
                    } else {
                        $sql = "INSERT INTO usuarios (nombres, apellidos, email, usuario, contra) VALUES (?, ?, ?, ?, ?)";
                        $stmt = mysqli_stmt_init($con);
                        if (!mysqli_stmt_prepare($stmt, $sql)) {
                            header("location: registrousuarios?error=usertaken");
                            exit();
                        } else {
                            $hashedpass = password_hash($password, PASSWORD_DEFAULT);
                            mysqli_stmt_bind_param($stmt, "sssss", $nombres, $apellidos, $email, $user, $hashedpass);
                            mysqli_stmt_execute($stmt);
                            header("location: iniciarsesion?registro=exitoso");

                            //Enviar mail

                            // require_once('PHPMailer/PHPMailerAutoload.php');
                            require_once('Mailer/src/Exception.php');
                            require_once('Mailer/src/PHPMailer.php');
                            require_once('Mailer/src/SMTP.php');
                            // $mail = new PHPMailer();
                            $mail = new PHPMailer\PHPMailer\PHPMailer(true);
                            $mail->CharSet = 'UTF-8';
                            $mail->Encoding = 'base64';
                            $mail­->SMTPDebug = 2;
                            $mail->isSMTP();
                            $mail->SMTPAuth = true;
                            $mail->SMTPSecure = 'ssl';
                            $mail->Host = 'ns18.hostgator.co';
                            $mail->Port = '465';
                            $mail->Username = 'ticketcreador@incoelectronica.com';
                            $mail->Password = '*T@URm&uQ9V0';

                            $emails_sql = $con->query("SELECT email FROM usuarios where perfil='admin'") or die($con->error);

                            $mail->setFrom('no-reply@ticketcreador.com');

                            // $to = $useremail;
                            session_start();

                            $mail->Subject = 'Nuevo registro de usuario';
                            $mail->Body = '<!doctype html>
                            <html lang="en-US">
                            
                            <head>
                                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                                <title>Email Templates</title>
                                <meta name="description" content="Email Templates.">
                                <style type="text/css">
                                    a:hover {
                                        text-decoration: underline !important;
                                    }
                                </style>
                            </head>
                            
                            <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                                <!--100% body table-->
                                <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: "Open Sans", sans-serif;">
                                    <tr>
                                        <td>
                                            <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="height:30px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                            <tr>
                                                                <td style="height:5px;">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding:0 35px;">
                                                                    <h1 style="color:#1e1e2d; font-weight:400; margin:0;font-size:28px;font-family:"Rubik",sans-serif;font-weight: bold;">TICKETCREADOR</h1>
                                                                    <span style="display:inline-block; vertical-align:middle; margin:13px 0 16px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                    <p style="color:#455056; font-size:15px;line-height:24px; margin-top: 0px;">
                                                                        Se le informa que se ha registrado el usuario '. $user .' con los siguientes datos:
                                                                    </p>
                                                                    <p style="color:#455056; font-size:15px;line-height:24px; margin:15px 0px 15px 0px;">
                                                                        Nombres: ' . $nombres . '
                                                                        <br>
                                                                        Apellidos: ' . $apellidos . '
                                                                        <br>
                                                                        Email: ' . $email . '
                                                                    </p>
                                                                    <a href="https://incoelectronica.com/ticketcreador/adminusuarios" style="background:#00E1FF;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Activar</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height:40px;">&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <tr>
                                                        <td style="height:20px;">&nbsp;</td>
                                                    </tr>
                            
                                                    <tr>
                                                        <td style="height:30px;">&nbsp;</td>
                                                    </tr>
                                            </table>
                                        </td>
                                        </tr>
                                </table>
                                <!--/100% body table-->
                            </body>
                            
                            </html>';
                            // $mail->addAddress('andresbello98@gmail.com');
                            while ($email = $emails_sql->fetch_assoc()) {
                                $mail->addAddress(strtolower(trim($email['email'])));
                            }

                            $mail->isHTML(true);
                            $mail->Send();

                            $_SESSION['message'] = "Registro exitoso";
                            $_SESSION['msg_type'] = "success";
                            header("location: registrar");
                            exit();
                        }
                    }
                }
            }
            mysqli_stmt_close($stmt);
            mysqli_close($con);
        }
    }else{
        $nombres = $_POST['nombres'];
        $apellidos = $_POST['apellidos'];
        $email = $_POST['email'];
        $user = $_POST['user'];

        session_start();
        $_SESSION['message'] = "Valide el recaptcha";
        $_SESSION['msg_type'] = "danger";
        header("location: registrousuarios?error=nocaptcha&nombres=" . $nombres . "&apellidos=" . $apellidos . "&email=" . $email . "&usuario=" . $user);
        exit();
    }
}

if (isset($_POST['iniciarsesion'])) {
    require 'con_db.php';

    $usuarioemail = $_POST['usuarioemail'];
    $password = $_POST['password'];

    if (empty($usuarioemail) || empty($password)) {
        session_start();
        $_SESSION['message'] = "Campos vacios";
        $_SESSION['msg_type'] = "danger";
        header("location: iniciarsesion?error=emptyfields");
        exit();
    } else {
        $sql = "SELECT * FROM usuarios WHERE usuario=? OR email=?";
        $stmt = mysqli_stmt_init($con);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            session_start();
            $_SESSION['message'] = "Error en base de datos";
            $_SESSION['msg_type'] = "danger";
            header("location: iniciarsesion?error=sqlerror");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "ss", $usuarioemail, $usuarioemail);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            if ($row = mysqli_fetch_assoc($result)) {
                $passcheck = password_verify($password, $row['contra']);
                if ($passcheck == false) {
                    session_start();
                    $_SESSION['message'] = "Contraseña incorrecta";
                    $_SESSION['msg_type'] = "danger";
                    header("location: iniciarsesion");
                    exit();
                } elseif ($passcheck == true) {
                    ini_set('session.gc_maxlifetime', 10800);
                    session_start();
                    $_SESSION['id'] = $row['id'];
                    $_SESSION['logged'] = $row['usuario'];
                    $_SESSION['nombreusuario'] = $row['nombres']." ".$row['apellidos'];
                    $_SESSION['perfil'] = $row['perfil'];
                    if ($_SESSION['perfil'] == "sin activar") {
                        session_unset();
                        session_destroy();
                        session_start();
                        $_SESSION['message'] = "Por favor espere a que su cuenta sea activada";
                        $_SESSION['msg_type'] = "danger";

                        header("location: iniciarsesion");
                    }
                    header("location: busqueda?success");
                    exit();
                } else {
                    session_start();
                    $_SESSION['message'] = "Contraseña incorrecta";
                    $_SESSION['msg_type'] = "danger";
                    header("location: iniciarsesion?error=contraseñaincorrecta");
                    exit();
                }
            } else {
                session_start();
                $_SESSION['message'] = "No existe el usuario";
                $_SESSION['msg_type'] = "danger";
                header("location: iniciarsesion?error=nouser");
                exit();
            }
        }
    }
}

if (isset($_POST['cerrarsesion'])) {
    session_start();
    session_unset();
    session_destroy();
    header("location: iniciarsesion");
}
