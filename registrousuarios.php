<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Crear usuario</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="login.php" class="login100-form validate-form" method="POST">
					<span class="login100-form-title p-b-30">
						REGISTRO USUARIO
					</span>

					<div class="wrap-input100 validate-input" data-validate="Complete este campo">
						<input class="input100" type="text" id="nombres" name="nombres" value="<?php if (isset($_GET['nombres'])) {
																									$nombres = $_GET['nombres'];
																									echo $nombres;
																								} ?>" required>
						<span class="focus-input100" data-placeholder="Nombres"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Complete este campo">
						<input class="input100" type="text" id="apellidos" name="apellidos" value="<?php if (isset($_GET['apellidos'])) {
																										$apellidos = $_GET['apellidos'];
																										echo $apellidos;
																									} ?>" required>
						<span class="focus-input100" data-placeholder="Apellidos"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Complete este campo">
						<input class="input100" type="text" id="user" name="user" value="<?php if (isset($_GET['usuario'])) {
																										$usuario = $_GET['usuario'];
																										echo $usuario;
																									} ?>" required>
						<span class="focus-input100" data-placeholder="Usuario"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Mail inválido">
						<input class="input100" type="text" id="email" name="email" value="<?php if (isset($_GET['email'])) {
																								$email = $_GET['email'];
																								echo $email;
																							} ?>" required>
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Contraseña inválida">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" id="passwordregistro" name="password" data-toggle="popover" data-content="Su contraseña debe tener de 6 a 20 dígitos, al menos un numero, una letra mayúscula y una letra minúscula." data-trigger="focus" data-placement="left" required>
						<span class="focus-input100" data-placeholder="Contraseña"> </span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="validatepassword" required>
						<span class="focus-input100" data-placeholder="Repita su contraseña"></span>
					</div>
					<?php
					if (isset($_SESSION['message'])) : ?>
						<div class="alert alert-<?= $_SESSION['msg_type'] ?> text-center">
							<?php
							echo $_SESSION['message'];
							unset($_SESSION['message']);
							?>
						</div>
					<?php endif ?>
					
					<div class="g-recaptcha" data-sitekey="6LcPAzIaAAAAANNiHv9LKO-PrRsU_g1YcY-ZKE_Q"></div>
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type="submit" id="crearusuario" name="crearusuario">
								Crear usuario
							</button>
						</div>
					</div>
					

					<div class="text-center p-t-50">
						<a class="txt2" href="iniciarsesion.php">
							Iniciar sesión
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<script>
		$(function() {
			$('[data-toggle="popover"]').popover()
		})
	</script>

</body>

</html>