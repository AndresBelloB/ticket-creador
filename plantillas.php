<?php
include('index.php');
?>

<div class="card text-center border-light mb-3">
    <div class="card-body">
        <div class="container">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-sgmto" role="tabpanel" aria-labelledby="nav-sgmto-tab">
                    <div class="row justify-content-center" style="background: #ffffff;">
                        <div class="col-auto">
                            <select name="selplantilla" id="selplantilla" class="form-control">
                                <option value selected disabled hidden>Seleccione</option>
                                <option value="reincidencia_sdh">Reincidencia equipos SDH</option>
                                <option value="reporte_sinfalla">Reporte electrificadora sin falla</option>
                                <option value="reporte_confalla">Reporte electrificadora con falla confirmada</option>
                                <option value="manten_elec">Mantenimiento electrificadora</option>
                            </select>
                        </div>
                    </div>
                    <div class="row justify-content-center mt-4 pt-2" style="margin: 8px 180px 8px 180px;display:none" id="divreincidencia_sdh" name="divreincidencia_sdh">
                        <div class="col-5">
                            <label>FO SDH Revisión inicial:</label>
                            <input type="text" class="form-control" id="revisioninicial" name="revisioninicial">
                            <label>Red afectada:</label>
                            <input type="text" class="form-control" id="redafectada" name="redafectada">
                            <label>Equipo/tramo:</label>
                            <input type="text" class="form-control" id="equipotramo" name="equipotramo">
                            <label>Proveedor:</label>
                            <input type="text" class="form-control" id="proveedor" name="proveedor">
                            <label style="max-width: 80%;margin-top: 12px;">En equipo/tramo debe salir como sale en el gestor.</label>
                        </div>
                        
                        <div class="col-auto my-auto offset-md-1">
                            <button type="button" onclick="showmodalplantillas()" id="plantillas_modal" name="plantillas_modal" class="btn btn-outline-secondary mt-2">Generar</button>
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4 pt-2" style="margin: 8px 180px 8px 180px;display:none" id="divreporte_sinfalla" name="divreporte_sinfalla">
                        <div class="col-5">
                            <label>Siglas:</label>
                            <input type="text" class="form-control w-50 mx-auto" id="siglas_rsinfalla" name="siglas_rsinfalla">
                            <label class="mt-1">Nombre electrificadora:</label>
                            <input type="text" class="form-control" id="nreporte_rsinfalla" name="nreporte_rsinfalla">
                            <label class="mt-1">Reporte electrificadora:</label>
                            <input type="text" class="form-control" id="relec_rsinfalla" name="relec_rsinfalla">
                            <label class="mt-1">Nombre asesor:</label>
                            <input type="text" class="form-control" id="nasesor_rsinfalla" name="nasesor_rsinfalla">
                        </div>
                        <div class="col-auto my-auto offset-md-1">
                            <button type="button" onclick="showmodalplantillas()" id="plantillas_modal" name="plantillas_modal" class="btn btn-outline-secondary mt-2">Generar</button>
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4 pt-2" style="margin: 8px 180px 8px 180px;display:none" id="divreporte_confalla" name="divreporte_confalla">
                        <div class="col-5">
                            <label>Siglas:</label>
                            <input type="text" class="form-control w-50 mx-auto" id="siglas_rconfalla" name="siglas_rconfalla">
                            <label class="mt-1">Nombre electrificadora:</label>
                            <input type="text" class="form-control" id="nreporte_rconfalla" name="nreporte_rconfalla">
                            <label class="mt-1">Reporte electrificadora:</label>
                            <input type="text" class="form-control" id="relec_rconfalla" name="relec_rconfalla">
                            <label class="mt-1">Nombre asesor:</label>
                            <input type="text" class="form-control" id="nasesor_rconfalla" name="nasesor_rconfalla">
                        </div>
                        <div class="col-auto my-auto offset-md-1">
                            <button type="button" onclick="showmodalplantillas()" id="plantillas_modal" name="plantillas_modal" class="btn btn-outline-secondary mt-2">Generar</button>
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4 pt-2" style="margin: 8px 180px 8px 180px;display:none" id="divmanten_elec" name="divmanten_elec">
                        <div class="col-5">
                            <label>Siglas:</label>
                            <input type="text" class="form-control w-50 mx-auto" id="siglas_mantenelec" name="siglas_mantenelec">
                            <label class="mt-1">Nombre electrificadora:</label>
                            <input type="text" class="form-control" id="nreporte_mantenelec" name="nreporte_mantenelec">
                            <label class="mt-1">Hora inicio mantenimiento:</label>
                            <input type="text" class="form-control" id="hinicio_mantenelec" name="hinicio_mantenelec">
                            <label class="mt-1">Hora final mantenimiento:</label>
                            <input type="text" class="form-control" id="hfinal_mantenelec" name="hfinal_mantenelec">
                            <label class="mt-1">Reporte electrificadora:</label>
                            <input type="text" class="form-control" id="reporte_mantenelec" name="reporte_mantenelec">
                            <label class="mt-1">Nombre asesor:</label>
                            <input type="text" class="form-control" id="nasesor_mantenelec" name="nasesor_mantenelec">
                        </div>
                        <div class="col-auto my-auto offset-md-1">
                            <button type="button" onclick="showmodalplantillas()" id="plantillas_modal" name="plantillas_modal" class="btn btn-outline-secondary mt-2">Generar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalplantillas" name="modalplantillas" tabindex="-1" role="dialog" aria-labelledby="modalplantillasLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <label for="textoplano1">Plantilla:</label>
                            <textarea class="form-control" id="textoplano_plantillas" name="textoplano_plantillas" rows="7"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/scripts_plantillas.js"></script>
</div>
<?php include_once('footer.php'); ?>