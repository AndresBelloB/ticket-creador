<?php
include("con_db.php");

if (isset($_POST['save'])) {
    session_start();
    $areaselect = $_POST['areaselect'];
    $nombre = $_POST['nombre'];

    if (isset($_POST['cnocvip'])) {
        $cnocvip = "on";
    } else {
        $cnocvip = "off";
    }
    if (isset($_POST['ecp'])) {
        $ecp = "on";
    } else {
        $ecp = "off";
    }
    if (isset($_POST['CRC'])) {
        $CRC = "on";
    } else {
        $CRC = "off";
    }

    $informaciondelsitio = $_POST['informaciondelsitio'];

    $lugar = $_POST['lugar'];
    $planta = $_POST['planta'];

    if (isset($_POST['bloqueo'])) {
        $bloqueo = "on";
    } else {
        $bloqueo = "off";
    }

    $electrificadora = $_POST['electrificadora'];
    $regional = $_POST['regional'];
    $nie = $_POST['nie'];
    $numero = $_POST['numero'];
    $criticidad = $_POST['criticidad'];
    $impacto = $_POST['impacto'];

    $satpost = $_POST['sat-poste'];

    if (isset($_POST['usms'])) {
        $usms = "on";
    } else {
        $usms = "off";
    }

    if (isset($_POST['netnumen'])) {
        $netnumen = "on";
    } else {
        $netnumen = "off";
    }

    if (isset($_POST['plancorp'])) {
        $plancorp = "on";
    } else {
        $plancorp = "off";
    }

    date_default_timezone_set('America/Bogota');
    $fecha = date('Y-m-d H:i:s', time());

    $observacion = $_POST['observacion'];

    if (isset($_POST['vip'])) {
        $vip = "on";
    } else {
        $vip = "off";
    }

    $con->query("INSERT INTO datos(T_EB,T_LUGAR ,T_CNOCVIP, T_ECP, T_CRC, T_INFSITIO, T_PLANTA, T_BLOQUEO, T_ELECTRIFICADORA, T_REGIONAL, T_NIE, T_NUMERO, T_CRITICIDAD, 
                            T_IMPACTO, T_SATELITAL, T_USMS, T_NETNUMEN, T_PLANCORP, T_FECHA, T_OBSERVACION, VIP) 
                VALUES ('$nombre','$lugar','$cnocvip','$ecp','$CRC','$informaciondelsitio','$planta','$bloqueo','$electrificadora','$regional','$nie','$numero','$criticidad',
                        '$impacto','$satpost','$usms','$netnumen','$plancorp', '$fecha','$observacion','$vip')") or die($con->error);

    // Enviar email
    // require_once('PHPMailer/PHPMailerAutoload.php');
    require_once('Mailer/src/Exception.php');
    require_once('Mailer/src/PHPMailer.php');
    require_once('Mailer/src/SMTP.php');

    // $mail = new PHPMailer();
    $mail = new PHPMailer\PHPMailer\PHPMailer(true);
    $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';
    $mail­->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = 'ns18.hostgator.co';
    $mail->Port = '465';
    $mail->Username = 'ticketcreador@incoelectronica.com';
    $mail->Password = '*T@URm&uQ9V0';

    $emails_sql = $con->query("SELECT email FROM usuarios where perfil='admin'") or die($con->error);

    $mail->setFrom('no-reply@ticketcreador.com');

    // $to = $useremail;
    session_start();

    $mail->Subject = 'Nuevo sitio creado';
    $mail->Body = '<!doctype html>
    <html lang="en-US">
    
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>Email Templates</title>
        <meta name="description" content="Email Templates.">
        <style type="text/css">
            a:hover {
                text-decoration: underline !important;
            }
        </style>
    </head>
    
    <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
        <!--100% body table-->
        <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: "Open Sans", sans-serif;">
            <tr>
                <td>
                    <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="height:30px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="height:20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                    <tr>
                                        <td style="height:5px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 35px;">
                                            <h1 style="color:#1e1e2d; font-weight:400; margin:0;font-size:28px;font-family:"Rubik",sans-serif;font-weight: bold;">TICKETCREADOR</h1>
                                            <span style="display:inline-block; vertical-align:middle; margin:13px 0 16px; border-bottom:1px solid #cecece; width:100px;"></span>
                                            <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                Se le informa que el usuario ' . $_SESSION['logged'] .' ha creado el sitio ' . $nombre . ' en la base de datos.
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:40px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
    
                            <tr>
                                <td style="height:30px;">&nbsp;</td>
                            </tr>
                    </table>
                </td>
                </tr>
        </table>
        <!--/100% body table-->
    </body>
    
    </html>';
    // $mail->addAddress('andresbello98@gmail.com');
    while($email = $emails_sql->fetch_assoc()){
        $mail->addAddress(strtolower(trim($email['email'])));
    }
    
    $mail->isHTML(true);
    $mail->Send();

    $_SESSION['message'] = "Registro creado";
    $_SESSION['msg_type'] = "success";
    header("location: registrar");
    exit();

    // if (!$mail -> Send()) {
    //     $_SESSION['message'] = "Error" . $mail­->ErrorInfo;
    //     $_SESSION['msg_type'] = "danger";
    //     header("location: registrar?email=error");
    // } else {
    //     $_SESSION['message'] = "Registro creado";
    //     $_SESSION['msg_type'] = "success";
    //     header("location: registrar");
    //     exit();
    // }

}


//BOTÓN LIVESEARCH

if (isset($_POST['query'])) {
    $inpText = trim($_POST['query']);
    $query = "SELECT * FROM datos WHERE T_EB LIKE '%$inpText%' LIMIT 5";
    $result = $con->query($query);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "<a href='#' class='list-group-item list-group-item-action'>" . $row['T_EB'] . "</a>";
        }
    } else {
        echo "<a href='registrar.php' class='list-group-item border-1' >Crear sitio</a>";
    }
}


//BOTÓN ACTUALIZAR

if (isset($_POST['update'])) {
    session_start();
    if ($_SESSION['perfil'] == "normal") {
        $_SESSION['message'] = "Su cuenta no está autorizada para esta acción.";
        $_SESSION['msg_type'] = "danger";
        header("location: busqueda");
        exit();
    }

    $id = $_POST['id'];
    $nombre = $_POST['nombrehidden'];
    $areaselect = $_POST['area'];

    $cambios = "";

    $sql_reg = $con->query("SELECT * FROM datos WHERE id='$id'") or die(mysqli_error($con));
    $r_ant = $sql_reg -> fetch_assoc();

    if (isset($_POST['cnocvip'])) {
        $cnocvip = "on";
    } else {
        $cnocvip = "off";
    }
    ($r_ant['T_CNOCVIP'] == $cnocvip ?  :  $cambios .= "CNOCVIP: " . $cnocvip);

    if (isset($_POST['ecp'])) {
        $ecp = "on";
    } else {
        $ecp = "off";
    }
    ($r_ant['T_ECP'] == $ecp ?  :  $cambios .= "<br>T_ECP: " . $ecp);

    if (isset($_POST['CRC'])) {
        $CRC = "on";
    } else {
        $CRC = "off";
    }
    ($r_ant['T_CRC'] == $CRC ?  :  $cambios .= "<br>T_CRC: " . $CRC);

    $regional = $_POST['regional'];
    ($r_ant['T_REGIONAL'] == $regional ?  :  $cambios .= "<br>T_REGIONAL: " . $regional);

    $informaciondelsitio = $_POST['informaciondelsitio'];
    ($r_ant['T_INFSITIO'] == $informaciondelsitio ?  :  $cambios .= "<br>T_INFSITIO: " . $informaciondelsitio);

    $planta = $_POST['planta'];
    ($r_ant['T_PLANTA'] == $planta ?  :  $cambios .= "<br>T_PLANTA: " . $planta);

    $lugar = $_POST['lugar'];
    ($r_ant['T_LUGAR'] == $lugar ?  :  $cambios .= "<br>Lugar: " . $lugar);
    
    
    if (isset($_POST['bloqueo'])) {
        $bloqueo = "on";
    } else {
        $bloqueo = "off";
    }
    ($r_ant['T_BLOQUEO'] == $bloqueo ?  :  $cambios .= "<br>T_BLOQUEO: " . $bloqueo);

    $electrificadora = $_POST['electrificadora'];
    ($r_ant['T_ELECTRIFICADORA'] == $electrificadora ?  :  $cambios .= "<br>T_ELECTRIFICADORA: " . $electrificadora);

    $nie = $_POST['nie'];
    ($r_ant['T_NIE'] == $nie ?  :  $cambios .= "<br>T_NIE: " . $nie);

    $numero = $_POST['numero'];
    ($r_ant['T_NUMERO'] == $numero ?  :  $cambios .= "<br>T_NUMERO: " . $numero);

    $criticidad = $_POST['criticidad'];
    ($r_ant['T_CRITICIDAD'] == $criticidad ?  :  $cambios .= "<br>T_CRITICIDAD: " . $criticidad);
    
    $impacto = $_POST['impacto'];
    ($r_ant['T_IMPACTO'] == $impacto ?  :  $cambios .= "<br>T_IMPACTO: " . $impacto);

    $satpost = $_POST['sat-poste'];
    ($r_ant['T_SATELITAL'] == $satpost ?  :  $cambios .= "<br>T_SATELITAL: " . $satpost);

    if (isset($_POST['usms'])) {
        $usms = "on";
    } else {
        $usms = "off";
    }
    ($r_ant['T_USMS'] == $usms ?  :  $cambios .= "<br>T_USMS: " . $usms);

    if (isset($_POST['netnumen'])) {
        $netnumen = "on";
    } else {
        $netnumen = "off";
    }
    ($r_ant['T_NETNUMEN'] == $netnumen ?  :  $cambios .= "<br>T_NETNUMEN: " . $netnumen);

    if (isset($_POST['plancorp'])) {
        $plancorp = "on";
    } else {
        $plancorp = "off";
    }
    ($r_ant['T_PLANCORP'] == $plancorp ?  :  $cambios .= "<br>T_PLANCORP: " . $plancorp);

    date_default_timezone_set('America/Bogota');
    $fecha = date('Y-m-d H:i:s', time());

    $observacion = $_POST['observacion'];
    ($r_ant['T_OBSERVACION'] == $observacion ?  :  $cambios .= "<br>T_OBSERVACION: " .$observacion);

    if (isset($_POST['vip'])) {
        $vip = "on";
    } else {
        $vip = "off";
    }
    ($r_ant['VIP'] == $vip ?  :  $cambios .= "<br>VIP: " . $vip);

    ($cambios != "" ? : $cambios = "<br>Sin cambios detectados.");

    $con->query("UPDATE datos SET T_LUGAR='$lugar', T_CNOCVIP='$cnocvip',T_ECP='$ecp',T_CRC='$CRC', T_INFSITIO='$informaciondelsitio', T_PLANTA='$planta',
                 T_BLOQUEO='$bloqueo', T_ELECTRIFICADORA='$electrificadora', T_REGIONAL='$regional' ,T_NIE='$nie', T_NUMERO='$numero', T_CRITICIDAD='$criticidad', T_IMPACTO='$impacto', T_SATELITAL='$satpost', 
                 T_USMS='$usms', T_NETNUMEN='$netnumen', T_PLANCORP='$plancorp', T_OBSERVACION='$observacion', VIP='$vip' WHERE id=$id") or die($con->error);

    // Enviar email
    // require_once('PHPMailer/PHPMailerAutoload.php');
    
    require_once('Mailer/src/Exception.php');
    require_once('Mailer/src/PHPMailer.php');
    require_once('Mailer/src/SMTP.php');

    $mail = new PHPMailer\PHPMailer\PHPMailer(true);
    $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';
    $mail­->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = 'ns18.hostgator.co';
    $mail->Port = '465';
    $mail->Username = 'ticketcreador@incoelectronica.com';
    $mail->Password = '*T@URm&uQ9V0';

    $emails_sql = $con->query("SELECT email FROM usuarios where perfil='admin'") or die($con->error);

    $mail->setFrom('no-reply@ticketcreador.com');

    // $to = $useremail;
    session_start();

    $mail->Subject = 'Sitio modificado';
    $mail->Body = '<!doctype html>
    <html lang="en-US">
    
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>Email Templates</title>
        <meta name="description" content="Email Templates.">
        <style type="text/css">
            a:hover {
                text-decoration: underline !important;
            }
        </style>
    </head>
    
    <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
        <!--100% body table-->
        <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: "Open Sans", sans-serif;">
            <tr>
                <td>
                    <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="height:30px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="height:20px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                    <tr>
                                        <td style="height:5px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 35px;">
                                            <h1 style="color:#1e1e2d; font-weight:400; margin:0;font-size:28px;font-family:"Rubik",sans-serif;font-weight: bold;">TICKETCREADOR</h1>
                                            <span style="display:inline-block; vertical-align:middle; margin:13px 0 16px; border-bottom:1px solid #cecece; width:100px;"></span>
                                            <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                Se le informa que el usuario ' . $_SESSION['logged'] .' ha modificado el sitio ' . $nombre . ' con los siguiente información:
                                            </p>
                                            <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                ' . $cambios . '
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:40px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
    
                            <tr>
                                <td style="height:30px;">&nbsp;</td>
                            </tr>
                    </table>
                </td>
                </tr>
        </table>
        <!--/100% body table-->
    </body>
    
    </html>';
    // $mail->addAddress('andresbello98@gmail.com');
    while($email = $emails_sql->fetch_assoc()){
        $mail->addAddress(strtolower(trim($email['email'])));
    }
    
    $mail->isHTML(true);
    $mail->Send();

    $_SESSION['message'] = "Registro actualizado";
    $_SESSION['msg_type'] = "success";
    header("location: busqueda");
    exit();
}

//LIMPIAR

if (isset($_POST['limpiar_registro'])) {
    header("location: registrar");
}

if (isset($_POST['limpiar_busqueda'])) {
    header("location: busqueda");
}

//BOTON resumen

if (isset($_POST['resumen'])) {
    $id = $_POST['id'];
    $nombre = $_POST['nombrehidden'];
    $areaselect = $_POST['areaselect'];

    if (isset($_POST['ecp'])) {
        $ecp = "on";
    } else {
        $ecp = "off";
    }

    if (isset($_POST['GSM'])) {
        $GSM = "SI";
    } else {
        $GSM = "NO";
    }
    if (isset($_POST['UMTS'])) {
        $UMTS = "SI";
    } else {
        $UMTS = "NO";
    }
    if (isset($_POST['LTE'])) {
        $LTE = "SI";
    } else {
        $LTE = "NO";
    }

    if (isset($_POST['satelital'])) {
        $satelital = "on";
    } else {
        $satelital = "off";
    }
    $diagnostico = $_POST['diagnostico'];
    $lugar = $_POST['lugar'];
    $planta = $_POST['planta'];

    if (isset($_POST['impacto'])) {
        $impacto = $_POST['impacto'];
        $impactoinfo = " (" . $impacto . ")";
    } else {
        $impactoinfo = "";
    }


    $satpost = $_POST['sat-poste'];
    if ($satpost == "NO") {
        $satpost = "";
    } elseif ($satpost == "SATELITAL") {
        $satpost = " (SATELITAL)";
    } elseif ($satpost == "POSTE") {
        $satpost = " (POSTE)";
    } elseif ($satpost == "TMX") {
        $satpost = " (TMX)";
    }

    if ($planta == "SIN PLANTA") {
        $plantainfo = "SIN PE.";
    } elseif ($planta == "SI") {
        $plantainfo = "CON PE";
    } else {
        $plantainfo = "CON PE " . $planta;
    }

    if (isset($_POST['casosdeuso'])) {
        $casosdeuso = $_POST['casosdeuso'];
    }

    if (isset($_POST['sintrafico'])) {
        if ($_POST['sintrafico'] == "VOZ" || $_POST['sintrafico'] == "DATOS" || $_POST['sintrafico'] == "VOZ-DATOS") {
            $fallasinfo = "Sin tráfico de " . $_POST['sintrafico'];
        } elseif ($GSM == "NO" && $UMTS == "NO" && $LTE == "NO") {
            $fallasinfo = "Alarmas de energía";
        }
    } elseif ($GSM == "NO" && $UMTS == "NO" && $LTE == "NO" && $casosdeuso == "NO") {
        $fallasinfo = "Alarmas de energía";
    } elseif ($GSM == "NO" && $UMTS == "NO" && $LTE == "NO" && $casosdeuso != "NO") {
        if ($casosdeuso == "ALTATEMP") {
            $fallasinfo = "Alarma de alta temperatura";
        } elseif ($casosdeuso == "PLANTAEN") {
            $fallasinfo = "Alarma de planta encendida";
        }
    } else {
        $fallasinfo = "Fuera de servicio";
    }



    if ($GSM == "NO" && $UMTS == "NO" && $LTE == "NO") {
        $fallas = "";
    } else {
        if ($GSM == "SI") {
            $fallas = " GSM";
            if ($UMTS == "SI") {
                $fallas = " GSM/UMTS";
                if ($LTE == "SI") {
                    $fallas = " GSM/UMTS/LTE";
                }
            }
            if ($LTE == "SI" && $UMTS == "NO") {
                $fallas = " GSM/LTE";
            }
        }
        if ($UMTS == "SI" && $GSM == "NO") {
            $fallas = " UMTS";
            if ($LTE == "SI") {
                $fallas = " UMTS/LTE";
            }
        }
        if ($LTE == "SI" && $GSM == "NO" && $UMTS == "NO") {
            $fallas = " LTE";
        }
    }

    if (isset($_POST['CRC'])) {
        $CRC = " (CRC)";
    } else {
        $CRC = "";
    }

    if (isset($_POST['cnocvip'])) {
        $CNOCVIP = " (CNOCVIP)";
    } else {
        $CNOCVIP = "";
    }

    if (isset($_POST['ecp'])) {
        $ECP = " (ECP)";
    } else {
        $ECP = "";
    }

    if (isset($_POST['tx'])) {
        $tx = " (TX)";
    } else {
        $tx = "";
    }

    if (isset($_POST['reinicio'])) {
        $reinicio = " (reinicio)";
    } else {
        $reinicio = "";
    }

    if (isset($_POST['intermitencia'])) {
        $intermitencia = " (intermitencia)";
    } else {
        $intermitencia = "";
    }

    if (isset($_POST['100municipio'])) {
        $municipio100 = " (M100%)";
    } else {
        $municipio100 = "";
    }

    if (isset($_POST['pnmsj'])) {
        $pnmsj = " (PNMSJ)";
    } else {
        $pnmsj = "";
    }

    $ticketFSP = $_POST['ticketFSP'];
    if ($ticketFSP == "") {
        $ticketFSPinfo = "";
    } else {
        $ticketFSPinfo = " (FSP " . $ticketFSP . ")";
    }

    $idtrabajo = $_POST['idtrabajo'];
    if ($idtrabajo == "") {
        $idtrabajoinfo = "";
    } else {
        $idtrabajoinfo = "Trabajo no exitoso ID:" . $idtrabajo;
    }

    if (isset($_POST['vip'])) {
        $vip = " VIP";
    } else {
        $vip = "";
    }

    if (isset($_POST['validacionderadios'])) {
        $validacionderadios = " EX";
    } else {
        $validacionderadios = "";
    }

    $turno = "";
    date_default_timezone_set('America/Bogota');
    $date = date('H:i:s', time());
    if (strtotime($date) < strtotime('14:00:00') && strtotime($date) >= strtotime('06:00:00')) {
        $turno = "T1";
    } elseif (strtotime($date) < strtotime('22:00:00') && strtotime($date) >= strtotime('14:00:00')) {
        $turno = "T2";
    } elseif (strtotime($date) >= strtotime('22:00:00') || (strtotime($date) >= strtotime('00:00:00') && strtotime($date) < strtotime('06:00:00'))) {
        $turno = "T3";
    }

    if (isset($_POST['turnot11'])) {
        $turno = "T11";
    }

    $fileurl = 'generado.txt';
    $file = fopen("$fileurl", "w");


    //EXTRAS PARA TRABAJO
    $user = $_POST['user'];
    if (isset($_POST['typeu'])) {
        $typeu = $_POST['typeu'];
    } else {
        $typeu = "";
    }

    $ticketFSP = $_POST['ticketFSP'];
    if ($ticketFSP == "") {
        $ticketFSPinfo = "";
        $ticketFSPres = "";
    } else {
        $ticketFSPinfo = " (FSP " . $ticketFSP . ") ";
        $ticketFSPres = "FSP: " . $ticketFSP;
    }

    $informaciondelsitio = $_POST['informaciondelsitio'];

    if (isset($_POST['alarmas'])) {
        $alarmas = $_POST['alarmas'];
    } else {
        $alarmas = "";
    }

    $observacion = $_POST['observacion'];

    $idtrabajo = $_POST['idtrabajo'];
    if ($idtrabajo == "") {
        $idtrabajoinfo = "";
    } else {
        $idtrabajoinfo = "Trabajo no exitoso ID:" . $idtrabajo;
    }


    $nie = $_POST['nie'];
    $numero = $_POST['numero'];

    if (isset($_POST['vng'])) {
        $vng = "(vng)\n";
    } else {
        $vng = "";
    }

    $fileurl = 'generado.txt';
    $file = fopen("$fileurl", "w");


    fwrite($file, $areaselect . ": MPACC_" . "$turno:" . $fallasinfo . " EB " . $nombre . $satpost . $fallas . " en " . $lugar . " EB " . $plantainfo . $reinicio . $intermitencia . $CRC .
        $CNOCVIP . $ECP . $tx . $pnmsj . $municipio100 . $impactoinfo . $ticketFSPinfo . $idtrabajoinfo . $vip . $validacionderadios . "\n\n\n");


    fwrite($file, $user . $typeu . ":" . $areaselect . ": MPACC_" . "$turno:" . $fallasinfo . " EB " . $nombre . $satpost . $fallas . " en " . $lugar . " EB " . $plantainfo . $CRC .
        $tx . $pnmsj . $impactoinfo . $ticketFSPinfo . $idtrabajoinfo . $vip . $validacionderadios . "\n\n");
    fwrite($file, "Diagnóstico:" . "\n" . $diagnostico . "\n" . $vng . "Afectación GSM: " . $GSM . "\nAfectación UMTS " . $UMTS . "\nAfectación LTE: " . $LTE . "\n\n");
    fwrite($file,  $idtrabajoinfo . "\n" . $ticketFSPres . "\n" . "NIE: " . $nie . "\n" . "Numero: " . $numero
        . "\n\nInformación del sitio: \n" . $informaciondelsitio . "\n\nAlarmas:\n" . $alarmas);
    fwrite($file, "\n" . "SIGLA SIGNIFICADO\nFSP Falla parcialmente superada\nFRP Falla regulatoria progresiva\nAID Alto impacto de disponibilidad\nMID Alto impacto de disponibilidad\nCRC Comision regulatoria de comunicaciones\nPPC Posible punto comun\nEE  Empresa de energía\nBOE Back Office Energia\nBOTX Back Office Transmision\nFHG  Falla herramienta de gestion");

    readfile($fileurl);
    header("Content-type:text-plain");
    header('Content-Disposition: attachment; filename=' . $fileurl);
}

if (isset($_POST['limpiar_busqueda'])) {
    header("location: busqueda");
}

//CREAR TAREA TO-DO


if (isset($_POST['crear_tarea'])) {

    if (isset($_POST['titulotarea'])) {
        $nombre_tarea = $_POST['titulotarea'];
    }

    if (isset($_POST['selusers'])) {
        $usuarios = $_POST['selusers'];
    }

    if (isset($_POST['valtiempo'])) {
        $recordatorio = $_POST['valtiempo'];
    }


    $con->query("INSERT INTO tareas(tarea, usuarios, tiempo) VALUES ('$nombre_tarea','$usuarios','$recordatorio')") or die($con->error);

    header("location: to-do");
}


//TABLAS GSM, UMTS Y LTE

// if(isset($_POST['action'])) {
//     $nombre = $_POST['action'];

//     $res = $con->query("SELECT * FROM consulta_sectores_lte WHERE T_EB='$data'") or die(mysqli_error($con));
// }

if(isset($_POST['fill_gsm'])){
    $nombre = $_POST['nombrehidden'];
    $tablagsm_sql = $con->query("SELECT * FROM consulta_sectores_gsm WHERE BCF_NAME='$nombre'") or die(mysqli_error($con));

    $tablagsm = $tablagsm_sql->fetch_all();
    // print_r($tablagsm)[0];
    header("location: busqueda");
    
}