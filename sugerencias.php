<?php
include('index.php');
?>
<div class="card text-center border-light mb-3">
    <div class="card-body">
        <form action="includes/buzondesugerencias.php" method="POST">
            <div class="container">
                <div class="row justify-content-center" style="background: #ffffff;">
                    <div class="col-7">
                        <h4 class="mb-3" style="color:#212529e8;">Buzón de sugerencias:</h4>
                        <textarea class="form-control" name="sugerencia" id="sugerencia" cols="30" rows="10"></textarea>
                        <br>
                        <div class="g-recaptcha" data-sitekey="6LcPAzIaAAAAANNiHv9LKO-PrRsU_g1YcY-ZKE_Q"></div>
                        <button type="submit" id="envsugerencias" name="envsugerencia" class="btn btn-dark mt-2 btn-sm">Enviar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>