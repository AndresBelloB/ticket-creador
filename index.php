<?php
ob_start();
session_start();
if (!isset($_SESSION['logged'])) {
    header("location: iniciarsesion");
    exit();
}

// $inactividad = 600;

// if (isset($_SESSION["timeout"])) {
//     // Calcular el tiempo de vida de la sesión
//     $sessionTTL = time() - $_SESSION["timeout"];
//     if ($sessionTTL > $inactividad) {
//         session_destroy();
//         header("location: iniciarsesion");
//     }
// }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TICKETCREADOR</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/style.css">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    
    <script src="https://kit.fontawesome.com/ddc9a417d1.js" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    
    <script src="js/bootstrap-datepicker.es.min.js" charset="UTF-8"></script>
    <script lang="javascript" src="js/xlsx.full.min.js"></script>
    <script lang="javascript" src="js/FileSaver.min.js"></script>

</head>

<body>
    <div class="card-header" id="header">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="registrar" id="nav-registrar" name="nav-registrar">Registrar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="busqueda" id="nav-busqueda" name="nav-busqueda">Buscar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="plantillas" id="nav-plantillas" name="nav-plantillas">Plantillas</a>
            </li>
            <li class="nav-item dropdown">
                <a id="nav-consulta" name="nav-consulta" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Consulta</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="consultadesitios" id="nav-consultadesitios" name="nav-consultadesitios">Sitios con y sin PE</a>
                    <a class="dropdown-item" href="consultaafectacion" id="nav-afectacion" name="nav-afectacion">Afectación</a>
                    <a class="dropdown-item" href="consultaafectacion_mun" id="nav-afectacion_mun" name="nav-afectacion_mun">Afectación por municipio</a>
                    <a class="dropdown-item" href="consultaproveedores">Proveedores</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a id="nav-ipran" name="nav-ipran" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">IPRAN</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="ipran_alcanzabilidad">Alcanzabilidad</a>
                    <a class="dropdown-item" href="anillos">Anillos</a>
                    <a class="dropdown-item" href="ipran_troncalcaida">Troncal caída</a>
                    <a class="dropdown-item" href="ipran_tareaplanta">Tarea a planta</a>
                </div>
            </li>
            <?php
            if ($_SESSION['perfil'] == "admin") :
                echo "<li class=nav-item><a class='nav-link' href='adminusuarios' id='nav-admin' name='nav-admin'>Admin</a></li>";
            endif;
            ?>
            <li class="nav-item">
                <a class="nav-link" href="to-do" id="nav-tareas" name="nav-tareas">To-Do</a>
            </li>
            <li>
                <a class="nav-link" href="indicadores" id="nav-indicadores" name="nav-indicadores">Indicadores</a>
            </li>
            <li>
                <a class="nav-link" href="alarmas" id="nav-alarmas" name="nav-alarmas">Alarmas</a>
            </li>
            <div class="col-auto ml-auto">
                <div class="col-auto" style="display: inline-flex;">
                    <label class="my-auto mr-3" style="color:#6f6f6f99;font-weight:bold;" for="user_logged"><?php echo($_SESSION['nombreusuario']); ?></label>
                    <select class="form-control col align-self-end" id="area" name="area">
                        <option value="FEE">FE</option>
                        <option value="FAOB">FAOB</option>
                        <option value="FAOC">FAOC</option>
                        <option value="FAPP2">PLATAFORMA 1</option>
                        <option value="FAPP">PLATAFORMA 2</option>
                        <option value="FC">CORE</option>
                        <option value="FI">FI</option>
                    </select>
                    <div>
                        <form action="login" method="POST">
                            <button type="submit" class="btn btn-link" id="cerrarsesion" name="cerrarsesion">Cerrar Sesión</button>
                        </form>
                    </div>
                </div>
            </div>
        </ul>
    </div>
    <?php
    if (isset($_SESSION['message'])) : ?>
        <div class="alert alert-<?= $_SESSION['msg_type'] ?> alert-dismissible fade show" role="alert">
            <?php
            echo $_SESSION['message'];
            unset($_SESSION['message']);
            ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif ?>
</body>

<script>
    var refreshSn = function ()
{
    var time = 600000; // 10 mins
    setTimeout(
        function ()
        {
        $.ajax({
           url: 'refrescar.php',
           cache: false,
           complete: function () {refreshSn();}
        });
    },
    time
);
};

// Call in page
refreshSn()
</script>
<script>
    var select = document.getElementById('area');
    var casosdeuso = {
        "ALTATEMP": "Alta temperatura",
        "PLANTAEN": "Planta encendida",
        "FUERADES": "Fuera de servicio",
        "FALLAALCATEL": "Falla Alcatel",
        "FALLASDH": "Falla SDH",
        "POWERBATERIA": "Power en batería",
        "BAJOVOLTAJEB": "Bajo voltaje en baterías"
    };

    function logValue() {


        var seleccion = select.value;
        switch (this.value) {
            case 'FEE':
                if (document.getElementById("formproceso") != null) {
                    //window.location = "busqueda.php?area=FE";
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();
                    document.getElementById("area").value = seleccion;

                    document.getElementById("formproceso").reset();
                    resetbuttons();

                    //Opciones casos de uso
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $.each(casosdeuso, function(key, value) {
                        $('#casosdeuso').append($("<option></option>")
                            .attr("value", key)
                            .text(value));
                    });
                    $("#casosdeuso").append('<option value="BLPORENERGIA">Bloqueo x energía</option>');
                    //Recupera siglas:
                    document.getElementById("user").value = localStorage.getItem("siglas");

                    
                }

                break;
            case 'FAOB':
                if (document.getElementById("formproceso") != null) {
                    $('#divelecnum').hide();
                    $('#divcasosdeuso').hide();
                    $('#divsintrafico').show();
                    document.getElementById("area").value = seleccion;

                    document.getElementById("formproceso").reset();
                    resetbuttons();
                    //Recupera siglas:
                    document.getElementById("user").value = localStorage.getItem("siglas");
                    $('#divsectores').slideDown('fast');
                }

                break;
            case 'FAOC':
                if (document.getElementById("formproceso") != null) {
                    $('#divelecnum').hide();
                    $('#divcasosdeuso').hide();
                    $('#divsintrafico').show();
                    document.getElementById("area").value = seleccion;

                    document.getElementById("formproceso").reset();
                    resetbuttons();
                    //Recupera siglas:
                    document.getElementById("user").value = localStorage.getItem("siglas");
                    $('#divsectores').slideDown('fast');
                }


                break;
            case 'FAPP':
                if (document.getElementById("formproceso") != null) {
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();
                    document.getElementById("area").value = seleccion;

                    document.getElementById("formproceso").reset();
                    resetbuttons();

                    //Opciones casos de uso:
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $("#casosdeuso").append('<option value="FUERADES">Fuera de servicio</option>');
                    $("#casosdeuso").append('<option value="FALLAALCATEL">Energía Alcatel</option>');
                    $("#casosdeuso").append('<option value="GM">GM</option>');
                    //Recupera siglas:
                    document.getElementById("user").value = localStorage.getItem("siglas");
                }
                break;
            case 'FAPP2':
                if (document.getElementById("formproceso") != null) {
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();
                    document.getElementById("area").value = seleccion;

                    document.getElementById("formproceso").reset();
                    resetbuttons();

                    //Opciones casos de uso:
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $("#casosdeuso").append('<option value="FUERADES">Fuera de servicio</option>');
                    $("#casosdeuso").append('<option value="FALLAALCATEL">Energía Alcatel</option>');
                    $("#casosdeuso").append('<option value="GM">GM</option>');
                    //Recupera siglas:
                    document.getElementById("user").value = localStorage.getItem("siglas");
                }
                break;
            case 'FC':
                if (document.getElementById("formproceso") != null) {
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();
                    document.getElementById("area").value = seleccion;

                    document.getElementById("formproceso").reset();
                    resetbuttons();

                    //Recupera siglas:
                    document.getElementById("user").value = localStorage.getItem("siglas");
                    //Opciones casos de uso
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $.each(casosdeuso, function(key, value) {
                        $('#casosdeuso').append($("<option></option>")
                            .attr("value", key)
                            .text(value));
                    });
                }
                break;
            case 'FI':
                if (document.getElementById("formproceso") != null) {
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();
                    document.getElementById("area").value = seleccion;

                    document.getElementById("formproceso").reset();
                    resetbuttons();

                    //Recupera siglas:
                    document.getElementById("user").value = localStorage.getItem("siglas");
                    //Opciones casos de uso
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $.each(casosdeuso, function(key, value) {
                        $('#casosdeuso').append($("<option></option>")
                            .attr("value", key)
                            .text(value));
                    });
                }
                break;
        }
    }
    select.addEventListener('change', logValue, false);
</script>

<script>
    function resetbuttons() {
        $('#turnot11').bootstrapToggle('off');
        $('#GSM').bootstrapToggle('off');
        $('#UMTS').bootstrapToggle('off');
        $('#LTE').bootstrapToggle('off');
        $('#intermitencia').bootstrapToggle('off');
        $('#reinicio').bootstrapToggle('off');
        $('#100municipio').bootstrapToggle('off');
        $('#vng').bootstrapToggle('off');
        $('#tx').bootstrapToggle('off');
        $('#pnmsj').bootstrapToggle('off');
        $('#validacionderadios').bootstrapToggle('off');
        $('#bloqueo').bootstrapToggle('off');
        $('#cnocvip').bootstrapToggle('off');
        $('#ecp').bootstrapToggle('off');
        $('#CRC').bootstrapToggle('off');
        $('#vip').bootstrapToggle('off');
        $('#usms').bootstrapToggle('off');
        $('#netnumen').bootstrapToggle('off');
        valoresbotonesdb();
    }
</script>

<script>
    $(function() {
        if(localStorage.getItem("area") != null){
            document.getElementById("area").value = localStorage.getItem("area");
        }else if(localStorage.getItem("area") == null){
            document.getElementById("area").value = "FEE";
        }

        var area = document.getElementById("area");
        area.addEventListener('change', (event) => {
            localStorage.setItem("area", area.value);
        });
    });
</script>

<script>
    $(function() {
        if (document.getElementById("formproceso") != null) {
            var areas = localStorage.getItem("area");
            switch (areas) {
                case 'FEE':
                    //window.location = "busqueda.php?area=FE";
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();


                    document.getElementById("formproceso").reset();

                    //Opciones casos de uso
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $.each(casosdeuso, function(key, value) {
                        $('#casosdeuso').append($("<option></option>")
                            .attr("value", key)
                            .text(value));
                    });
                    $("#casosdeuso").append('<option value="BLPORENERGIA">Bloqueo x energía</option>');

                    break;
                case 'FAOB':
                    $('#divelecnum').hide();
                    $('#divcasosdeuso').hide();
                    $('#divsintrafico').show();


                    document.getElementById("formproceso").reset();
                    $('#divsectores').slideDown('fast');


                    break;
                case 'FAOC':
                    $('#divelecnum').hide();
                    $('#divcasosdeuso').hide();
                    $('#divsintrafico').show();


                    document.getElementById("formproceso").reset();
                    $('#divsectores').slideDown('fast');


                    break;
                case 'FAPP':
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();


                    document.getElementById("formproceso").reset();

                    //Opciones casos de uso:
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $("#casosdeuso").append('<option value="FUERADES">Fuera de servicio</option>');
                    $("#casosdeuso").append('<option value="FALLAALCATEL">Energía Alcatel</option>');
                    $("#casosdeuso").append('<option value="GM">GM</option>');

                    break;
                case 'FAPP2':
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();


                    document.getElementById("formproceso").reset();

                    //Opciones casos de uso:
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $("#casosdeuso").append('<option value="FUERADES">Fuera de servicio</option>');
                    $("#casosdeuso").append('<option value="FALLAALCATEL">Energía Alcatel</option>');
                    $("#casosdeuso").append('<option value="GM">GM</option>');

                    break;
                case 'FC':
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();
                    //Opciones casos de uso
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $.each(casosdeuso, function(key, value) {
                        $('#casosdeuso').append($("<option></option>")
                            .attr("value", key)
                            .text(value));
                    });


                    document.getElementById("formproceso").reset();

                    break;
                case 'FI':
                    $('#divelecnum').show();
                    $('#divcasosdeuso').show();
                    $('#divsintrafico').hide();
                    //Opciones casos de uso
                    $('#casosdeuso').children('option:not(:first)').remove();
                    $.each(casosdeuso, function(key, value) {
                        $('#casosdeuso').append($("<option></option>")
                            .attr("value", key)
                            .text(value));
                    });


                    document.getElementById("formproceso").reset();

                    break;
            }
        }
    });
</script>
</html>